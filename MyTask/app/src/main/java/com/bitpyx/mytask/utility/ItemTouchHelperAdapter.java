package com.bitpyx.mytask.utility;

/**
 * @author Amrit Yadav
 * @since 7/26/2017.
 */
public interface ItemTouchHelperAdapter {

    void onItemMove(int fromPosition, int toPosition);

    void onItemDismiss(int position);
}
