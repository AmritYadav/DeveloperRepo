package com.bitpyx.mytask.addTask.presenters;

import com.bitpyx.mytask.addTask.views.IAddEditView;
import com.bitpyx.mytask.data.model.Task;
import com.bitpyx.mytask.utility.IBasePresenter;

/**
 * @author Amrit Yadav
 * @since 6/17/2017.
 */
public interface IAddEditPresenter<V> extends IBasePresenter<V> {

    void saveNewCategory(String categoryName);

    void fetchTaskCategories();

    void fetchTaskPriorities();

    void fetchTaskRepeated();

    void getCategoryTypeById(int categoryId);

    void saveTask(Task task, IAddEditView.onTaskSaved onTaskSaved);

    void updateTask(Task task, IAddEditView.onTaskUpdated onTaskUpdated);

    void checkPriorityColorCode(String priority);

}
