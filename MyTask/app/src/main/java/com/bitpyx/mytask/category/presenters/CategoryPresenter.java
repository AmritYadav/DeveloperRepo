package com.bitpyx.mytask.category.presenters;

import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.bitpyx.mytask.R;
import com.bitpyx.mytask.adapter.TaskAdapter;
import com.bitpyx.mytask.category.interactors.CategoryInteractor;
import com.bitpyx.mytask.category.interactors.ICategoryInteractor;
import com.bitpyx.mytask.category.views.ICategoryView;
import com.bitpyx.mytask.data.model.DropDownContent;
import com.bitpyx.mytask.data.model.Task;

import java.util.List;

/**
 * @author Amrit Yadav
 * @since 6/24/2017
 */
public class CategoryPresenter implements ICategoryPresenter<ICategoryView>{
    private ICategoryView mCategoryView;
    private ICategoryInteractor mCategoryInteractor;
    @Override
    public void attachView(ICategoryView mCategoryView) {
        this.mCategoryView=mCategoryView;
        mCategoryInteractor = new CategoryInteractor(mCategoryView.getContext());
    }

    @Override
    public void fetchTaskListByCategory(int categoryId) {
        List<Task> taskList = mCategoryInteractor.getTaskByCategory(categoryId);
        mCategoryView.setAdapter(taskList);
    }

    @Override
    public void getCategoryList() {
        List<DropDownContent> dropDownContents = mCategoryInteractor.fetchCategoryList();
        mCategoryView.categoryList(dropDownContents);
    }

    @Override
    public void removeTask(int taskId) {
        mCategoryInteractor.deleteTask(taskId);
    }

    @Override
    public void updateListOnTaskSwipeRemove(List<Task> taskList) {
        mCategoryView.setAdapter(taskList);
    }

    @Override
    public void addUndoTask(List<Task> undoTask) {
        mCategoryInteractor.saveUndoTask(undoTask);
    }

    @Override
    public void detachView() {

    }

    @Override
    public void selectListItem(View view, Task task) {
        task.setSelected(true);
        view.setBackgroundColor(ContextCompat.getColor(mCategoryView.getContext()
                , R.color.listSelectedColor));
    }

    @Override
    public void unSelectListItem(View view, Task task) {
        task.setSelected(false);
        view.setBackgroundColor(ContextCompat.getColor(mCategoryView.getContext()
                , R.color.white));
    }

    @Override
    public void onDestroyActionMode(List<Task> mTaskList, List<Integer> mListItemsToUpdate, RecyclerView mRecyclerView) {
        int listSize = mListItemsToUpdate.size();

        for (int i = 0; i < listSize; i++) {
            int position = mListItemsToUpdate.get(i);
            Task task = mTaskList.get(position);
            task.setSelected(false);
        }
        TaskAdapter adapter = (TaskAdapter) mRecyclerView.getAdapter();
        adapter.refreshAdapter(mTaskList);
        mListItemsToUpdate.clear();
    }

    @Override
    public void onActionItemClicked(List<Task> mTaskList, List<Integer> mListItemsToUpdate,
                                    RecyclerView mRecyclerView, int categoryId) {
        mCategoryInteractor.UpdateTasksStatus(mTaskList, mListItemsToUpdate);
        fetchTaskListByCategory(categoryId);
    }
}
