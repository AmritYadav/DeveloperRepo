package com.bitpyx.mytask.adapter;

import android.content.Context;
import android.graphics.PorterDuff;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bitpyx.mytask.R;
import com.bitpyx.mytask.data.model.DropDownContent;
import com.bitpyx.mytask.data.model.Task;
import com.bitpyx.mytask.utility.Common;

import java.util.ArrayList;
import java.util.List;

/**
 * A common adapter to be used to display All, Today
 * , Category wise and Completed Tasks
 *
 * @author Amrit Yadav
 * @since 6/24/2017
 */
public class TaskAdapter extends RecyclerView.Adapter<TaskAdapter.VHItem> {
    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;
    private Context mContext;
    private List<Task> mTaskList;
    private List<DropDownContent> mCategoryList;
    private List<Task> itemsPendingRemoval;
    private int colorAccent, colorPriorityNormal, colorPriorityMedium, colorPriorityHigh;

    public TaskAdapter(Context context, List<Task> mTaskList, List<DropDownContent> categoryList) {
        mContext = context;
        this.mTaskList = mTaskList;
        itemsPendingRemoval = new ArrayList<>();
        mCategoryList = categoryList;

        colorAccent = ContextCompat
                .getColor(mContext, R.color.colorAccent);
        colorPriorityNormal = ContextCompat
                .getColor(mContext, R.color.priorityNormal);
        colorPriorityMedium = ContextCompat
                .getColor(mContext, R.color.priorityMedium);
        colorPriorityHigh = ContextCompat
                .getColor(mContext, R.color.priorityHigh);

    }

    class VHItem extends RecyclerView.ViewHolder {
        private TextView mTextViewTaskTitle, mTextViewHeaderTitle, mTextViewTaskCategory, mTextViewTaskDueDate;
        private ImageView mImageViewTaskPriority, mImageViewTaskRepeating;

        VHItem(View view, int viewType) {
            super(view);

            switch (viewType) {
                case TYPE_HEADER:
                    mTextViewHeaderTitle = (TextView) view.findViewById(R.id.text_header_title);
                    break;
                case TYPE_ITEM:
                    mImageViewTaskPriority = (ImageView) view.findViewById(R.id.iv_task_priority);
                    mImageViewTaskRepeating = (ImageView) view.findViewById(R.id.iv_task_repeat);
                    mTextViewTaskTitle = (TextView) view.findViewById(R.id.tv_task_title);
                    mTextViewTaskDueDate = (TextView) view.findViewById(R.id.tv_task_due_date);
                    mTextViewTaskCategory = (TextView) view.findViewById(R.id.tv_task_category);
                    break;
            }
        }
    }

    @Override
    public TaskAdapter.VHItem onCreateViewHolder(ViewGroup parent, int viewType) {

        View view;

        switch (viewType) {
            case TYPE_HEADER:
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.row_task_today_header, parent, false);
                return new TaskAdapter.VHItem(view, viewType);
            case TYPE_ITEM:
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.row_task_today_layout, parent, false);
                return new TaskAdapter.VHItem(view, viewType);
        }

        return null;
    }

    @Override
    public int getItemViewType(int position) {
        return mTaskList.get(position).getViewType();
    }

    @Override
    public void onBindViewHolder(TaskAdapter.VHItem holder, int position) {

        Task task = mTaskList.get(position);

        switch (task.getViewType()) {
            case TYPE_HEADER:
                holder.mTextViewHeaderTitle.setText(task.getTitle());
                holder.mTextViewHeaderTitle.setTextColor(ContextCompat
                        .getColor(mContext, task.getTaskHeaderTitleColor()));
                break;
            case TYPE_ITEM:

                String category = "";

                int categoryID = task.getCategoryId();
                for (DropDownContent dropDownContent : mCategoryList) {
                    if (categoryID == dropDownContent.getCategoryId()) {
                        category = dropDownContent.getName();
                        break;
                    }
                }

                if (!task.getRepeat().equals("One Time")) {
                    holder.mImageViewTaskRepeating.setVisibility(View.VISIBLE);
                } else {
                    holder.mImageViewTaskRepeating.setVisibility(View.GONE);
                }

                String dueDate = Common.getFormattedDate(task.getDate());
                holder.mTextViewTaskDueDate.setText(dueDate);

                holder.mTextViewTaskTitle.setText(task.getTitle());
                holder.mTextViewTaskCategory.setText(category);

                String priority = task.getPriority();
                switch (priority) {
                    case "Normal":
                        holder.mImageViewTaskPriority
                                .getDrawable()
                                .setColorFilter(colorPriorityNormal, PorterDuff.Mode.SRC_IN);
                        break;
                    case "Medium":
                        holder.mImageViewTaskPriority
                                .getDrawable()
                                .setColorFilter(colorPriorityMedium, PorterDuff.Mode.SRC_IN);
                        break;
                    case "High":
                        holder.mImageViewTaskPriority
                                .getDrawable()
                                .setColorFilter(colorPriorityHigh, PorterDuff.Mode.SRC_IN);
                        break;
                }

                if (task.isSelected()) {
                    holder.itemView.setBackgroundColor(ContextCompat.getColor(mContext
                            , R.color.listSelectedColor));
                } else {
                    holder.itemView.setBackgroundColor(0);
                }
        }
    }

    @Override
    public int getItemCount() {
        return mTaskList.size();
    }


    public void pendingRemoval(int position) {

        final Task task = mTaskList.get(position);
        if (!itemsPendingRemoval.contains(task)) {
            itemsPendingRemoval.add(task);
            notifyItemChanged(position);
            remove(mTaskList.indexOf(task));
        }
    }

    private void remove(int position) {
        Task task = mTaskList.get(position);
        if (itemsPendingRemoval.contains(task)) {
            itemsPendingRemoval.remove(task);
        }
        if (mTaskList.contains(task)) {
            mTaskList.remove(position);
            notifyItemRemoved(position);
        }
    }

    public boolean isPendingRemoval(int position) {
        Task task = mTaskList.get(position);
        return itemsPendingRemoval.contains(task);
    }

    public void refreshAdapter(List<Task> taskList) {
        mTaskList = taskList;
        notifyDataSetChanged();
    }

}
