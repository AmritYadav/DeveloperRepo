package com.bitpyx.mytask.adapter;

import android.content.Context;
import android.graphics.PorterDuff;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bitpyx.mytask.R;
import com.bitpyx.mytask.data.model.NavSubTitle;
import com.bitpyx.mytask.data.model.NavTitle;

import java.util.HashMap;
import java.util.List;

/**
 * @author Amrit Yadav
 * @since 6/22/2017.
 */
public class NavExpandableListAdapter extends BaseExpandableListAdapter {

    private Context context;
    private List<NavTitle> mNavTitleList;
    private HashMap<String, List<NavSubTitle>> mNavMenuMap;

    public NavExpandableListAdapter(Context context, List<NavTitle> mNavTitleList
            , HashMap<String, List<NavSubTitle>> mNavMenuMap) {
        this.context = context;
        this.mNavTitleList = mNavTitleList;
        this.mNavMenuMap = mNavMenuMap;
    }

    @Override
    public int getGroupCount() {
        return mNavTitleList.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return mNavMenuMap.get(mNavTitleList.get(groupPosition).getTitleText())
                .size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return mNavTitleList.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return mNavMenuMap.get(mNavTitleList.get(groupPosition).getTitleText())
                .get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {

        NavTitle navTitle = (NavTitle) getGroup(groupPosition);
        String headerTitle = navTitle.getTitleText();

        VHGroup holder;

        if (convertView == null) {
            holder = new VHGroup();
            LayoutInflater inflateInflater = (LayoutInflater) this.context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflateInflater.inflate(R.layout.nav_list_group, parent, false);
            holder.mTextViewTaskTitle = (TextView) convertView.findViewById(R.id.tv_nav_title);
            holder.mImageViewNavGroup = (ImageView) convertView.findViewById(R.id.img_nav_group);
            holder.mImageViewNavGroupViewMore = (ImageView) convertView.findViewById(R.id.iv_task_category);
            holder.mTextViewTaskCount = (TextView) convertView.findViewById(R.id.tv_task_count);
            convertView.setTag(holder);
        } else {
            holder = (VHGroup) convertView.getTag();
        }

        holder.mTextViewTaskTitle.setText(headerTitle);
        holder.mImageViewNavGroup.setImageResource(navTitle.getDrawableId());

        if (navTitle.getTaskCount() > 0) {
            holder.mTextViewTaskCount.setVisibility(View.VISIBLE);
            holder.mTextViewTaskCount.setText(String.valueOf(navTitle.getTaskCount()));
        } else {
            holder.mTextViewTaskCount.setVisibility(View.GONE);
        }

        if (groupPosition == 3 || groupPosition == 5)
            holder.mImageViewNavGroupViewMore.setVisibility(View.VISIBLE);
        else
            holder.mImageViewNavGroupViewMore.setVisibility(View.GONE);

        if (isExpanded)
            holder.mImageViewNavGroupViewMore.setImageResource(R.drawable.ic_expand_less);
        else
            holder.mImageViewNavGroupViewMore.setImageResource(R.drawable.ic_expand_more);

        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        NavSubTitle navSubTitle = (NavSubTitle) getChild(groupPosition, childPosition);

        if (convertView == null) {
            LayoutInflater inflateInflater = (LayoutInflater) this.context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflateInflater.inflate(R.layout.nav_list_item, parent, false);
        }

        ImageView imageView = (ImageView) convertView.findViewById(R.id.img_nav_subCategory);
        TextView textViewSubCategoryTaskCount = (TextView) convertView.findViewById(R.id.tv_nav_subCategory_task_count);
        TextView txtListChild = (TextView) convertView
                .findViewById(R.id.lblListItem);
        String subMenuTitle = navSubTitle.getSubTitleText();
        txtListChild.setText(subMenuTitle);
        imageView.setImageResource(navSubTitle.getDrawableId());

        switch (subMenuTitle) {
            case "Normal":
                int colorPriorityNormal = ContextCompat.getColor(context, R.color.priorityNormal);
                imageView.getDrawable().setColorFilter(colorPriorityNormal, PorterDuff.Mode.SRC_IN);
                break;
            case "Medium":
                int colorPriorityMedium = ContextCompat.getColor(context, R.color.priorityMedium);
                imageView.getDrawable().setColorFilter(colorPriorityMedium, PorterDuff.Mode.SRC_IN);
                break;
            case "High":
                int colorPriorityHigh = ContextCompat.getColor(context, R.color.priorityHigh);
                imageView.getDrawable().setColorFilter(colorPriorityHigh, PorterDuff.Mode.SRC_IN);
                break;
        }

        if (navSubTitle.getTaskCount() == 0)
            textViewSubCategoryTaskCount.setVisibility(View.GONE);
        else {
            textViewSubCategoryTaskCount.setVisibility(View.VISIBLE);
            textViewSubCategoryTaskCount.setText(String.valueOf(navSubTitle.getTaskCount()));
        }

        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    private class VHGroup {
        TextView mTextViewTaskTitle, mTextViewTaskCount;
        ImageView mImageViewNavGroup, mImageViewNavGroupViewMore;
    }

}
