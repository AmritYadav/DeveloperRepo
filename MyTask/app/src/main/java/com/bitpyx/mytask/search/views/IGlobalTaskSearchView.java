package com.bitpyx.mytask.search.views;

import com.bitpyx.mytask.data.model.DropDownContent;
import com.bitpyx.mytask.data.model.Task;
import com.bitpyx.mytask.utility.IMvpView;

import java.util.List;

/**
 * @author Amrit Yadav
 * @since 6/24/2017
 */
public interface IGlobalTaskSearchView extends IMvpView{

    void filteredTaskList(List<Task> taskList);

    void categoryList(List<DropDownContent> dropDownContents);

}
