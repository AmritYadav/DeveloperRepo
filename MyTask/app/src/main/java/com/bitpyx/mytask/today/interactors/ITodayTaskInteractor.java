package com.bitpyx.mytask.today.interactors;

import com.bitpyx.mytask.data.model.DropDownContent;
import com.bitpyx.mytask.data.model.Task;

import java.util.List;

/**
 * @author Amrit Yadav
 * @since 6/17/2017
 */
public interface ITodayTaskInteractor {

    List<Task> fetchTodayTask();

    List<Task> fetchTodayTask(int contactId);

    List<DropDownContent> fetchCategoryList();

    void deleteTask(int taskId);

    void saveUndoTask(List<Task> undoTaskList);

    void UpdateTasksStatus(List<Task> mTaskList, List<Integer> mListItemsToUpdate);

}
