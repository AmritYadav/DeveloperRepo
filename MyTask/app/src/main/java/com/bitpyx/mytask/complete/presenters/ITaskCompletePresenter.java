package com.bitpyx.mytask.complete.presenters;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.bitpyx.mytask.data.model.Task;
import com.bitpyx.mytask.utility.IBasePresenter;

import java.util.List;

/**
 * @author Amrit Yadav
 * @since 7/29/2017.
 */
public interface ITaskCompletePresenter<V> extends IBasePresenter<V> {
    void requestTaskCompleteList();

    void getCategoryList();

    void removeTask(int taskId);

    void updateListOnTaskSwipeRemove(List<Task> taskList);

    void addUndoTask(List<Task> undoTask);

    void selectListItem(View view, Task task);

    void unSelectListItem(View view, Task task);

    void onDestroyActionMode(List<Task> mTaskList, List<Integer> mListItemsToRestore, RecyclerView mRecyclerView);

    void onActionItemClicked(List<Task> mTaskList, List<Integer> mListItemsToRestore, RecyclerView mRecyclerView);
}
