package com.bitpyx.mytask.utility;

import android.content.Context;

/**
 * Created by Amrit Yadav and Ajay Bind on 21-05-2017.
 */

public interface IMvpView {

    Context getContext();
}

