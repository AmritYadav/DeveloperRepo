package com.bitpyx.mytask.utility;

/**
 * @author Amrit Yadav
 * @since 7/27/2017.
 */

public interface ItemTouchHelperViewHolder {
    void onItemSelected();
    void onItemClear();
}
