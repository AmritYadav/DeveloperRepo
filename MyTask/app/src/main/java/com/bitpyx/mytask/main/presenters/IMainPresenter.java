package com.bitpyx.mytask.main.presenters;

import com.bitpyx.mytask.utility.IBasePresenter;

import java.util.List;

/**
 * @author Amrit Yadav
 * @since 21-05-2017.
 */
public interface IMainPresenter<V> extends IBasePresenter<V> {

    boolean isReadStorageAllowed();

    void requestStoragePermission();

    void showDefaultFragment();

    void getCategoryList();

    void getAssigneeList();

    void loadAppCreatedCategoryList(List<String> defaultCategoryList);

    void resetNavigationMenuItemList();

    void getTodayTaskCount();

    void getTaskCompleteCount();

}
