package com.bitpyx.mytask.filter.interactors;

import android.content.Context;

import com.bitpyx.mytask.data.model.DropDownContent;
import com.bitpyx.mytask.data.model.Task;
import com.bitpyx.mytask.data.source.local.dbconstants.DatabaseHelper;

import org.joda.time.LocalDateTime;

import java.util.List;

import static com.bitpyx.mytask.utility.Common.defaultDateTimeFormatter;

/**
 * @author Aamrit Yadav
 * @since 7/13/2017.
 */
public class TaskFilterInteractor implements ITaskFilterInteractor{
    private DatabaseHelper mDatabaseHelper;

    public TaskFilterInteractor(Context context) {
        mDatabaseHelper = new DatabaseHelper(context);
    }

    @Override
    public List<Task> getTaskByFilter(int filterId) {
        return mDatabaseHelper.getTaskListByFilter(filterId);
    }

    @Override
    public List<DropDownContent> fetchCategoryList() {
        return mDatabaseHelper.getCategoryList();
    }

    @Override
    public void deleteTask(int taskId) {
        mDatabaseHelper.deleteTask(taskId);
    }

    @Override
    public void saveUndoTask(List<Task> undoTaskList) {
        for (Task task : undoTaskList) {
            mDatabaseHelper.addNewTask(task);
        }
    }

    @Override
    public void UpdateTasksStatus(List<Task> mTaskList, List<Integer> mListItemsToUpdate) {
        try {
            int listSize = mListItemsToUpdate.size();
            for (int i = 0; i < listSize; i++) {
                Task task = mTaskList.get(mListItemsToUpdate.get(i));
                LocalDateTime localDateTime = LocalDateTime.parse(task.getDate()
                        , defaultDateTimeFormatter);
                switch (task.getRepeat()) {
                    case "One Time":
                        task.setStatus("Complete");
                        break;
                    case "Weekly":
                        task.setDate(defaultDateTimeFormatter
                                .print(localDateTime.plusWeeks(task.getRepeatFrequency())));
                        break;
                    case "Monthly":
                        task.setDate(defaultDateTimeFormatter
                                .print(localDateTime.plusMonths(task.getRepeatFrequency())));
                        break;
                    case "Yearly":
                        task.setDate(defaultDateTimeFormatter
                                .print(localDateTime.plusYears(task.getRepeatFrequency())));
                        break;
                }
                mDatabaseHelper.updateTask(task);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
