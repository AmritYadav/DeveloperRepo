package com.bitpyx.mytask.main.presenters;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.widget.Toast;

import com.bitpyx.mytask.data.model.Assignee;
import com.bitpyx.mytask.data.model.DropDownContent;
import com.bitpyx.mytask.main.interactors.MainInteractorImpl;
import com.bitpyx.mytask.main.view.IMainView;
import com.bitpyx.mytask.main.interactors.IMainInteractor;

import java.util.Arrays;
import java.util.List;

/**
 * @author Amrit Yadav
 * @since 21-05-2017.
 */
public class MainPresenterImpl implements IMainPresenter<IMainView> {
    private IMainView mMainView;
    private IMainInteractor mMainInteractor;

    @Override
    public void attachView(IMainView view) {
        mMainView = view;
        mMainInteractor = new MainInteractorImpl(mMainView.getContext());
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.LOLLIPOP) {
            if(!mMainInteractor.checkDefaultTaskCategory()){
                List<String> preloadTaskCategoryList = Arrays.asList("Personal", "Society", "Office", "Any other");
                for (String taskCategory : preloadTaskCategoryList) {
                    mMainInteractor.addDefaultTaskCategory(taskCategory);
                }
            }
        }
    }

    @Override
    public void detachView() {

    }

    //We are calling this method to check the permission status
    @Override
    public boolean isReadStorageAllowed() {
        //Getting the permission status
        int result = ContextCompat.checkSelfPermission(mMainView.getContext(), Manifest.permission.READ_EXTERNAL_STORAGE);

        //If permission is granted returning true
        if (result == PackageManager.PERMISSION_GRANTED)
            return true;

        //If permission is not granted returning false
        return false;
    }

    //Requesting permission
    @Override
    public void requestStoragePermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) mMainView.getContext()
                , Manifest.permission.READ_EXTERNAL_STORAGE)){
            //If the user has denied the permission previously your code will come to this block
            //Here you can explain why you need this permission
            //Explain here why you need this permission
            Toast.makeText(mMainView.getContext(), "Request you to provide permission in the settings of the app and then restart the application", Toast.LENGTH_LONG).show();
            mMainView.closeMainActivity();
        }

        //And finally ask for the permission
        int STORAGE_PERMISSION_CODE = 1;
        ActivityCompat.requestPermissions((Activity) mMainView.getContext()
                , new String[]{Manifest.permission.READ_EXTERNAL_STORAGE
                        , Manifest.permission.READ_CONTACTS}
                , STORAGE_PERMISSION_CODE);
    }

    @Override
    public void showDefaultFragment() {
        mMainView.openDefaultTodayFragment();
    }

    @Override
    public void getCategoryList() {
        resetNavigationMenuItemList();
        List<DropDownContent> dropDownContents = mMainInteractor.fetchCategoryList();
        mMainView.categoryList(dropDownContents);
    }

    @Override
    public void getAssigneeList() {
        List<Assignee> assigneeList = mMainInteractor.fetchAssigneeList();
        mMainView.setAssgineeList(assigneeList);
    }

    @Override
    public void loadAppCreatedCategoryList(List<String> appCreatedCategoryList) {
        mMainInteractor.addAppCreatedCategoryList(appCreatedCategoryList);
    }

    @Override
    public void resetNavigationMenuItemList() {
        mMainView.clearNavMenuDataList();
    }

    @Override
    public void getTodayTaskCount() {
        int taskCount = mMainInteractor.fetchTodayTaskCount();
        mMainView.setTodayTaskCount(taskCount);
    }

    @Override
    public void getTaskCompleteCount() {
        int taskCount = mMainInteractor.fetchTaskCompleteCount();
        mMainView.setTaskCompleteCount(taskCount);
    }

}
