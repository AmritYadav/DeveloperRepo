package com.bitpyx.mytask.today;

import android.animation.Animator;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.ActionMode;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.LinearInterpolator;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bitpyx.mytask.R;
import com.bitpyx.mytask.SwipeUtil.SwipeUtil;
import com.bitpyx.mytask.adapter.TaskAdapter;
import com.bitpyx.mytask.addTask.AddEditTaskActivity;
import com.bitpyx.mytask.data.model.DropDownContent;
import com.bitpyx.mytask.data.model.Task;
import com.bitpyx.mytask.main.MainActivity;
import com.bitpyx.mytask.today.presenters.ITodayTaskPresenter;
import com.bitpyx.mytask.today.presenters.TodayTaskPresenter;
import com.bitpyx.mytask.today.views.ITodayView;
import com.bitpyx.mytask.utility.RecyclerTouchListener;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.bitpyx.mytask.utility.Common.createTaskDocumentDirectory;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * Use the {@link TodayTaskFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class TodayTaskFragment extends Fragment implements ITodayView
        , View.OnClickListener {

    private static final String ARG_CONTACT_ID = "contact_id";
    private int statusBarColor;
    private RecyclerView mRecyclerView;
    private RelativeLayout mRelativeLayoutEmptyView;
    private Snackbar snackbar;
    private ActionMode mActionMode;
    private FloatingActionButton mFabAddEditTask;
    private Dialog mBottomSheetDialog;
    private List<Task> mTaskList;
    private List<Task> mRemovedTaskList;
    private List<DropDownContent> mCategoryList;
    private List<Integer> mListItemsToUpdate = new ArrayList<>();
    private int contactId;
    private boolean mIsMultiSelect = false;
    private TaskAdapter mTaskAdapter;
    private ITodayTaskPresenter<ITodayView> mTodayTaskPresenter;

    private ActionMode.Callback mActionModeCallback = new ActionMode.Callback() {

        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                //hold current color of status bar
                statusBarColor = getActivity().getWindow().getStatusBarColor();
                getActivity().getWindow()
                        .setStatusBarColor(ContextCompat.getColor(getActivity()
                                , R.color.actionModePrimaryColorDark));
            }

            CoordinatorLayout.LayoutParams layoutParams =
                    (CoordinatorLayout.LayoutParams) mFabAddEditTask.getLayoutParams();
            int fab_bottomMargin = layoutParams.bottomMargin;

            mFabAddEditTask.animate().translationY(mFabAddEditTask.getHeight() + fab_bottomMargin)
                    .setInterpolator(new LinearInterpolator()).setListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animation) {

                }

                @Override
                public void onAnimationEnd(Animator animation) {
                    if (mIsMultiSelect)
                        mFabAddEditTask.setVisibility(View.GONE);
                }

                @Override
                public void onAnimationCancel(Animator animation) {

                }

                @Override
                public void onAnimationRepeat(Animator animation) {

                }
            }).start();

            // Inflate a menu resource providing context menu items
            MenuInflater inflater = mode.getMenuInflater();
            inflater.inflate(R.menu.menu_multi_select, menu);
            MenuItem item = menu.findItem(R.id.action_send);
            MenuItem itemRestoreTask = menu.findItem(R.id.action_restore);
            itemRestoreTask.setVisible(false);
            if (contactId == 0)
                item.setVisible(false);
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false; // Return false if nothing is done
        }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            int itemId = item.getItemId();
            switch (itemId) {
                case R.id.action_task_done:
                    mTodayTaskPresenter.onActionItemClicked(mTaskList, mListItemsToUpdate
                            , mRecyclerView, contactId);
                    mActionMode = null;
                    mIsMultiSelect = false;
                    break;
                case R.id.action_send:
                    openShareBottomSheet();
                    break;
            }
            return false;
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                //return to "old" color of status bar
                getActivity().getWindow().setStatusBarColor(statusBarColor);
            }
            mFabAddEditTask.setVisibility(View.VISIBLE);
            mFabAddEditTask.animate().translationY(0).setInterpolator(new LinearInterpolator()).start();
            mActionMode = null;
            mIsMultiSelect = false;
            mTodayTaskPresenter.onDestroyActionMode(mTaskList, mListItemsToUpdate, mRecyclerView);
        }
    };

    public TodayTaskFragment() {
        // Required empty public constructor
    }

    public static TodayTaskFragment newInstance(int contactId) {
        TodayTaskFragment fragment = new TodayTaskFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_CONTACT_ID, contactId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            contactId = getArguments().getInt(ARG_CONTACT_ID);
        }

        //Creates directory where all the tasks will be exported
        createTaskDocumentDirectory();

        mRemovedTaskList = new ArrayList<>();

        mTodayTaskPresenter = new TodayTaskPresenter();
        mTodayTaskPresenter.attachView(this);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_today, container, false);

        CoordinatorLayout mCorCoordinatorLayout = (CoordinatorLayout) container.getParent();

        mFabAddEditTask = (FloatingActionButton) mCorCoordinatorLayout
                .findViewById(R.id.fab_add_edit_task);

        snackbar = Snackbar.make(mCorCoordinatorLayout, "", 2000).setAction("UNDO", this)
                .addCallback(new Snackbar.Callback() {
                    @Override
                    public void onDismissed(Snackbar snackbar, int event) {
                        mRemovedTaskList.clear();
                    }

                    @Override
                    public void onShown(Snackbar snackbar) {

                    }
                });

        mRelativeLayoutEmptyView = (RelativeLayout) view.findViewById(R.id.layout_today_empty_view);

        mRecyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        // LinearLayoutManager is used here, this will layout the elements in a similar fashion
        // to the way ListView would layout elements. The RecyclerView.LayoutManager defines how
        // elements are laid out.
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerView.addOnItemTouchListener(new RecyclerTouchListener(getActivity()
                , mRecyclerView, new MainActivity.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                Task task = mTaskList.get(position);

                if (mIsMultiSelect) {
                    if (task.isSelected()) {
                        mTodayTaskPresenter.unSelectListItem(view, task);
                        mListItemsToUpdate.removeAll(Arrays.asList(position));
                    } else {
                        mTodayTaskPresenter.selectListItem(view, task);
                        mListItemsToUpdate.add(position);
                    }

                    mActionMode.setTitle("" + mListItemsToUpdate.size());

                    if (mListItemsToUpdate.isEmpty() && mActionMode != null) {
                        mIsMultiSelect = false;
                        mActionMode.finish();
                        mActionMode = null;
                    }

                } else {
                    Intent intent = new Intent(getActivity(), AddEditTaskActivity.class);
                    intent.putExtra("Fragment", "Today");
                    intent.putExtra("Task", task);
                    intent.putExtra("Type", "edit");
                    startActivity(intent);
                }
            }

            @Override
            public void onLongClick(View view, int position) {
                mIsMultiSelect = true;

                if (!mListItemsToUpdate.contains(position)) {
                    Task task = mTaskList.get(position);
                    mTodayTaskPresenter.selectListItem(view, task);
                    mListItemsToUpdate.add(position);
                }

                if (mActionMode == null) {
                    mActionMode = getActivity().startActionMode(mActionModeCallback);
                }

                mActionMode.setTitle("" + mListItemsToUpdate.size());
            }
        }));

        DividerItemDecoration mDividerItemDecoration =
                new DividerItemDecoration(mRecyclerView.getContext(),
                        DividerItemDecoration.VERTICAL);
        mRecyclerView.addItemDecoration(mDividerItemDecoration);

        setSwipeForRecyclerView();

        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        mTodayTaskPresenter.getCategoryList();
        if (contactId > 0)
            mTodayTaskPresenter.fetchTodayTask(contactId);
        else
            mTodayTaskPresenter.fetchTodayTask();
    }

    @Override
    public void setAdapter(List<Task> taskList) {

        if (mActionMode != null)
            mActionMode.finish();

        mTaskList = taskList;

        if (mTaskList.size() > 0) {
            mRecyclerView.setVisibility(View.VISIBLE);
            mRelativeLayoutEmptyView.setVisibility(View.GONE);
            mTaskAdapter = new TaskAdapter(getActivity(), mTaskList, mCategoryList);
            mRecyclerView.setAdapter(mTaskAdapter);
            mTaskAdapter.notifyDataSetChanged();
        } else {
            mRecyclerView.setVisibility(View.GONE);
            mRelativeLayoutEmptyView.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void categoryList(List<DropDownContent> dropDownContents) {
        mCategoryList = dropDownContents;
    }

    private void setSwipeForRecyclerView() {

        SwipeUtil swipeHelper = new SwipeUtil(0, ItemTouchHelper.LEFT, getActivity()) {

            @Override
            public int getMovementFlags(RecyclerView recyclerView
                    , RecyclerView.ViewHolder viewHolder) {
                return viewHolder.getItemViewType() == 0
                        ? 0 : super.getMovementFlags(recyclerView, viewHolder);
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                int swipedPosition = viewHolder.getAdapterPosition();
                Task task = mTaskList.get(swipedPosition);
                mTodayTaskPresenter.removeTask(task.getTaskId());
                mRemovedTaskList.add(task);
                mTaskAdapter = (TaskAdapter) mRecyclerView.getAdapter();
                mTaskAdapter.pendingRemoval(swipedPosition);
                mTodayTaskPresenter.updateListHeadersOnSwipeRemove(mTaskList);
                String message = "DELETE (" + mRemovedTaskList.size() + ")";
                snackbar.setText(message);
                snackbar.show();
            }

            @Override
            public int getSwipeDirs(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
                int position = viewHolder.getAdapterPosition();
                mTaskAdapter = (TaskAdapter) mRecyclerView.getAdapter();
                if (mTaskAdapter.isPendingRemoval(position)) {
                    return 0;
                }
                return super.getSwipeDirs(recyclerView, viewHolder);
            }
        };

        ItemTouchHelper mItemTouchHelper = new ItemTouchHelper(swipeHelper);
        mItemTouchHelper.attachToRecyclerView(mRecyclerView);
    }

    @Override
    public void onClick(View v) {
        int viewId = v.getId();
        switch (viewId) {
            case R.id.action_share_via_sms:
                String contactNumber1 = getContactNumber(contactId);
                String message1 = prepareMessageToShare(0);
                Intent smsIntent = new Intent(Intent.ACTION_SENDTO);
                smsIntent.putExtra("sms_body", message1);
                smsIntent.addCategory(Intent.CATEGORY_DEFAULT);
                smsIntent.setType("vnd.android-dir/mms-sms");
                smsIntent.setData(Uri.parse("sms:" + contactNumber1));
                startActivity(smsIntent);
                break;
            case R.id.action_share_via_whatsapp:
                String contactNumber2 = getContactNumber(contactId);
                String message2 = prepareMessageToShare(0);
                Intent sendIntent = new Intent("android.intent.action.MAIN");
                sendIntent.putExtra("jid", contactNumber2 + "@s.whatsapp.net");
                sendIntent.putExtra(Intent.EXTRA_TEXT, message2);
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.setPackage("com.whatsapp");
                sendIntent.setType("text/plain");
                startActivity(sendIntent);
                break;
            case R.id.action_share_via_mail:
                String assigneeEmailId = getAssigneeEmailId(contactId);
                String message3 = prepareMessageToShare(1);
                Intent emailIntent = new Intent(Intent.ACTION_SEND, Uri.parse("mailto:"));
                emailIntent.setType("message/rfc822");
                String[] recipients = new String[]{assigneeEmailId};
                emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, recipients);
                emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Assigned Task");
                emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, message3);
                startActivity(Intent.createChooser(emailIntent, "Send E-mail via"));
                break;
            default:
                mTodayTaskPresenter.addUndoTask(mRemovedTaskList);
                mTodayTaskPresenter.fetchTodayTask();
                mRemovedTaskList.clear();
        }
        if (mBottomSheetDialog != null && mBottomSheetDialog.isShowing())
            mBottomSheetDialog.dismiss();
    }

    public List<Task> getTaskList() {
        return mTaskList;
    }

    public void openShareBottomSheet() {
        View view = getActivity().getLayoutInflater().inflate(R.layout.content_bottom_sheet, null);

        TextView textViewSms = (TextView) view.findViewById(R.id.action_share_via_sms);
        TextView textViewWhatsApp = (TextView) view.findViewById(R.id.action_share_via_whatsapp);
        TextView textViewMail = (TextView) view.findViewById(R.id.action_share_via_mail);

        textViewSms.setOnClickListener(this);
        textViewWhatsApp.setOnClickListener(this);
        textViewMail.setOnClickListener(this);

        mBottomSheetDialog = new Dialog(getActivity(), R.style.MaterialDialogSheet);
        mBottomSheetDialog.setContentView(view);
        mBottomSheetDialog.setCancelable(true);
        mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        mBottomSheetDialog.getWindow().setGravity(Gravity.BOTTOM);
        mBottomSheetDialog.show();
    }

    public String getContactNumber(int rawContactId) {
        Cursor c = getActivity().getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                new String[]{ContactsContract.CommonDataKinds.Phone.CONTACT_ID
                        , ContactsContract.CommonDataKinds.Phone.NUMBER}
                , ContactsContract.CommonDataKinds.Phone.CONTACT_ID + "=?",
                new String[]{String.valueOf(rawContactId)}, null);
        if (c != null) {
            c.moveToFirst();
            return c.getString(1).replaceAll("[()\\s-]+", "").substring(1, 13);
        }
        return "";
    }

    public String getAssigneeEmailId(int contactId) {
        Cursor cursor = getActivity().getContentResolver().query(
                ContactsContract.CommonDataKinds.Email.CONTENT_URI, null,
                ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = ?",
                new String[]{String.valueOf(contactId)}, null);
        if (cursor != null && cursor.moveToNext()) {
            return cursor.getString(cursor
                    .getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));
        }
        return "";
    }

    private String prepareMessageToShare(int shareCode) {
        String assigneeName = mTaskList.get(mListItemsToUpdate.get(0)).getAssignee().getAssigneeName();
        String message = "";
        switch (shareCode) {
            case 0:
                message = "You have been assigned below task\n\n";
                break;
            case 1:
                message = "Hi " + assigneeName + ",\n\n"
                        + "You have been assigned below task :\n\n";
                break;
        }

        int taskIncrement = 1;
        for (Integer integer : mListItemsToUpdate) {
            Task task = mTaskList.get(integer);
            message = message + "#" + taskIncrement + " Task\n"
                    + "Title : " + task.getTitle() + "\n";
            taskIncrement++;
        }
        return message;
    }

}
