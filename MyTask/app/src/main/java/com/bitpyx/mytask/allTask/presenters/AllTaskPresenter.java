package com.bitpyx.mytask.allTask.presenters;

import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.bitpyx.mytask.R;
import com.bitpyx.mytask.adapter.TaskSearchViewAdapter;
import com.bitpyx.mytask.allTask.interactors.AllTaskInteractor;
import com.bitpyx.mytask.allTask.interactors.IAllTaskInteractor;
import com.bitpyx.mytask.allTask.views.IAllTaskView;
import com.bitpyx.mytask.data.model.DropDownContent;
import com.bitpyx.mytask.data.model.Task;

import java.util.List;

/**
 * @author Amrit Yadav
 * @since 25-06-2017.
 */
public class AllTaskPresenter implements IAllTaskPresenter<IAllTaskView> {
    private IAllTaskView mAllTaskView;
    private IAllTaskInteractor mAllTaskInteractor;

    @Override
    public void attachView(IAllTaskView mAllTaskView) {
        this.mAllTaskView = mAllTaskView;
        mAllTaskInteractor = new AllTaskInteractor(mAllTaskView.getContext());
    }

    @Override
    public void fetchAllTaskList() {
        List<Task> taskList = mAllTaskInteractor.fetchAllTaskList();
        mAllTaskView.setAdapter(taskList);
    }

    @Override
    public void getCategoryList() {
        List<DropDownContent> dropDownContents = mAllTaskInteractor.fetchCategoryList();
        mAllTaskView.categoryList(dropDownContents);
    }

    @Override
    public void removeTask(int taskId) {
        mAllTaskInteractor.deleteTask(taskId);
    }

    @Override
    public void addUndoTask(List<Task> undoTask) {
        mAllTaskInteractor.saveUndoTask(undoTask);
    }

    @Override
    public void selectListItem(View view, Task task) {
        task.setSelected(true);
        view.setBackgroundColor(ContextCompat.getColor(mAllTaskView.getContext()
                , R.color.listSelectedColor));
    }

    @Override
    public void unSelectListItem(View view, Task task) {
        task.setSelected(false);
        view.setBackgroundColor(ContextCompat.getColor(mAllTaskView.getContext()
                , R.color.white));
    }

    @Override
    public void onDestroyActionMode(List<Task> mTaskList, List<Integer> mListItemsToRemove, RecyclerView mRecyclerView) {
        int listSize = mListItemsToRemove.size();
        for (int i = 0; i < listSize; i++) {
            int position = mListItemsToRemove.get(i);
            Task task = mTaskList.get(position);
            task.setSelected(false);
        }
        TaskSearchViewAdapter adapter = (TaskSearchViewAdapter) mRecyclerView.getAdapter();
        adapter.refreshAdapter(mTaskList, false);
        mListItemsToRemove.clear();
    }

    @Override
    public void onActionItemClicked(List<Task> mTaskList, List<Integer> mListItemsToRemove, RecyclerView mRecyclerView) {
        mAllTaskInteractor.updateTasksStatus(mTaskList, mListItemsToRemove);
        fetchAllTaskList();
    }

    @Override
    public void changeTaskCategory(Task task) {
        mAllTaskInteractor.updateTaskCategory(task);
    }

    @Override
    public void detachView() {

    }
}
