package com.bitpyx.mytask.complete;


import android.animation.Animator;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.LinearInterpolator;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.bitpyx.mytask.R;
import com.bitpyx.mytask.SwipeUtil.SwipeUtil;
import com.bitpyx.mytask.adapter.TaskAdapter;
import com.bitpyx.mytask.complete.presenters.ITaskCompletePresenter;
import com.bitpyx.mytask.complete.presenters.TaskCompletePresenter;
import com.bitpyx.mytask.complete.view.ITaskCompleteView;
import com.bitpyx.mytask.data.model.DropDownContent;
import com.bitpyx.mytask.data.model.Task;
import com.bitpyx.mytask.main.MainActivity;
import com.bitpyx.mytask.utility.RecyclerTouchListener;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link TaskCompleteFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class TaskCompleteFragment extends Fragment implements ITaskCompleteView
        , View.OnClickListener, ActionMode.Callback {
    private static final String ARG_PARAM1 = "param1";

    private String mParam1;
    private int statusBarColor;
    protected RecyclerView mRecyclerView;
    private Snackbar snackbar;
    private List<Task> mTaskList;
    private List<DropDownContent> mCategoryList;
    private List<Task> mRemovedTaskList;
    private ITaskCompletePresenter<ITaskCompleteView> mTaskCompletePresenter;
    private ActionMode mActionMode;
    private boolean mIsMultiSelect = false;
    private List<Integer> mListItemsToRestore = new ArrayList<>();
    private FloatingActionButton mFabAddEditTask;
    private RelativeLayout mRelativeLayoutEmptyView;
    private TaskAdapter mTaskAdapter;

    public TaskCompleteFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @return A new instance of fragment TaskCompleteFragment.
     */
    public static TaskCompleteFragment newInstance(String param1) {
        TaskCompleteFragment fragment = new TaskCompleteFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mTaskCompletePresenter = new TaskCompletePresenter();
        mTaskCompletePresenter.attachView(this);
        mRemovedTaskList = new ArrayList<>();
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_completed, container, false);

        CoordinatorLayout mCorCoordinatorLayout = (CoordinatorLayout) container.getParent();

        mFabAddEditTask = (FloatingActionButton) mCorCoordinatorLayout
                .findViewById(R.id.fab_add_edit_task);

        snackbar = Snackbar.make(mCorCoordinatorLayout, "", 2000).setAction("UNDO", this)
                .addCallback(new Snackbar.Callback() {
                    @Override
                    public void onDismissed(Snackbar snackbar, int event) {
                        mRemovedTaskList.clear();
                    }

                    @Override
                    public void onShown(Snackbar snackbar) {

                    }
                });
        mRelativeLayoutEmptyView = (RelativeLayout) view.findViewById(R.id.layout_today_empty_view);

        mRecyclerView = (RecyclerView) view.findViewById(R.id.recycler_view_task_complete);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerView.addOnItemTouchListener(new RecyclerTouchListener(getActivity()
                , mRecyclerView, new MainActivity.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                Task task = mTaskList.get(position);
                if (mIsMultiSelect) {
                    if (task.isSelected()) {
                        mTaskCompletePresenter.unSelectListItem(view, task);
                        mListItemsToRestore.removeAll(Arrays.asList(position));
                    } else {
                        mTaskCompletePresenter.selectListItem(view, task);
                        mListItemsToRestore.add(position);
                    }

                    mActionMode.setTitle("" + mListItemsToRestore.size());

                    if (mListItemsToRestore.isEmpty() && mActionMode != null) {
                        mIsMultiSelect = false;
                        mActionMode.finish();
                        mActionMode = null;
                    }
                }
            }

            @Override
            public void onLongClick(View view, int position) {
                mIsMultiSelect = true;
                if (!mListItemsToRestore.contains(position)) {
                    Task task = mTaskList.get(position);
                    mTaskCompletePresenter.selectListItem(view, task);
                    mListItemsToRestore.add(position);
                }
                if (mActionMode == null) {
                    mActionMode = getActivity().startActionMode(TaskCompleteFragment.this);
                    mActionMode.setTitle("" + mListItemsToRestore.size());
                }
            }
        }));

        DividerItemDecoration mDividerItemDecoration =
                new DividerItemDecoration(mRecyclerView.getContext(),
                        DividerItemDecoration.VERTICAL);
        mRecyclerView.addItemDecoration(mDividerItemDecoration);

        setSwipeForRecyclerView();

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        mTaskCompletePresenter.getCategoryList();
        mTaskCompletePresenter.requestTaskCompleteList();
    }

    @Override
    public void showTaskCompleteList(List<Task> taskList) {
        if (mActionMode != null)
            mActionMode.finish();
        mTaskList = taskList;
        if (mTaskList.size() > 0) {
            mRecyclerView.setVisibility(View.VISIBLE);
            mRelativeLayoutEmptyView.setVisibility(View.GONE);
            mTaskAdapter = new TaskAdapter(getActivity(), mTaskList, mCategoryList);
            mRecyclerView.setAdapter(mTaskAdapter);
            mTaskAdapter.notifyDataSetChanged();
        } else {
            mRecyclerView.setVisibility(View.GONE);
            mRelativeLayoutEmptyView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void showToast(String message, int duration) {
        Toast.makeText(getActivity(), message, duration).show();
    }

    @Override
    public void categoryList(List<DropDownContent> dropDownContents) {
        mCategoryList = dropDownContents;
    }

    private void setSwipeForRecyclerView() {

        SwipeUtil swipeHelper = new SwipeUtil(0, ItemTouchHelper.LEFT, getActivity()) {

            @Override
            public int getMovementFlags(RecyclerView recyclerView
                    , RecyclerView.ViewHolder viewHolder) {
                return viewHolder.getItemViewType() == 0
                        ? 0 : super.getMovementFlags(recyclerView, viewHolder);
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                int swipedPosition = viewHolder.getAdapterPosition();
                Task task = mTaskList.get(swipedPosition);
                mTaskCompletePresenter.removeTask(task.getTaskId());
                mRemovedTaskList.add(task);
                mTaskAdapter = (TaskAdapter) mRecyclerView.getAdapter();
                mTaskAdapter.pendingRemoval(swipedPosition);
                mTaskCompletePresenter.updateListOnTaskSwipeRemove(mTaskList);
                String message = "DELETE (" + mRemovedTaskList.size() + ")";
                snackbar.setText(message);
                snackbar.show();
            }

            @Override
            public int getSwipeDirs(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
                int position = viewHolder.getAdapterPosition();
                mTaskAdapter = (TaskAdapter) mRecyclerView.getAdapter();
                if (mTaskAdapter.isPendingRemoval(position)) {
                    return 0;
                }
                return super.getSwipeDirs(recyclerView, viewHolder);
            }
        };

        ItemTouchHelper mItemTouchHelper = new ItemTouchHelper(swipeHelper);
        mItemTouchHelper.attachToRecyclerView(mRecyclerView);
    }

    @Override
    public void onClick(View v) {
        mTaskCompletePresenter.addUndoTask(mRemovedTaskList);
        mTaskCompletePresenter.requestTaskCompleteList();
        mRemovedTaskList.clear();
    }

    @Override
    public boolean onCreateActionMode(ActionMode mode, Menu menu) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            //hold current color of status bar
            statusBarColor = getActivity().getWindow().getStatusBarColor();
            getActivity().getWindow()
                    .setStatusBarColor(ContextCompat.getColor(getActivity()
                            , R.color.actionModePrimaryColorDark));
        }

        CoordinatorLayout.LayoutParams layoutParams =
                (CoordinatorLayout.LayoutParams) mFabAddEditTask.getLayoutParams();
        int fab_bottomMargin = layoutParams.bottomMargin;

        mFabAddEditTask.animate().translationY(mFabAddEditTask.getHeight() + fab_bottomMargin)
                .setInterpolator(new LinearInterpolator()).setListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                if(mIsMultiSelect)
                    mFabAddEditTask.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        }).start();

        // Inflate a menu resource providing context menu items
        MenuInflater inflater = mode.getMenuInflater();
        inflater.inflate(R.menu.menu_multi_select, menu);
        MenuItem itemSend = menu.findItem(R.id.action_send);
        itemSend.setVisible(false);
        MenuItem itemTaskDone = menu.findItem(R.id.action_task_done);
        itemTaskDone.setVisible(false);
        return true;
    }

    @Override
    public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
        return false;
    }

    @Override
    public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
        mTaskCompletePresenter.onActionItemClicked(mTaskList, mListItemsToRestore, mRecyclerView);
        mActionMode = null;
        mIsMultiSelect = false;
        return false;
    }

    @Override
    public void onDestroyActionMode(ActionMode mode) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            //return to "old" color of status bar
            getActivity().getWindow().setStatusBarColor(statusBarColor);
        }
        mFabAddEditTask.setVisibility(View.VISIBLE);
        mFabAddEditTask.animate().translationY(0).setInterpolator(new LinearInterpolator()).start();
        mActionMode = null;
        mIsMultiSelect = false;
        mTaskCompletePresenter.onDestroyActionMode(mTaskList, mListItemsToRestore, mRecyclerView);
    }
}
