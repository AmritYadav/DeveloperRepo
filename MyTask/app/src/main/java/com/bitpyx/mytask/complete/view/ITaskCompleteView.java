package com.bitpyx.mytask.complete.view;

import com.bitpyx.mytask.data.model.DropDownContent;
import com.bitpyx.mytask.data.model.Task;
import com.bitpyx.mytask.utility.IMvpView;

import java.util.List;

/**
 * @author Amrit Yadav
 * @since 7/29/2017.
 */
public interface ITaskCompleteView extends IMvpView {
    void showTaskCompleteList(List<Task> taskList);

    void showToast(String message, int duration);

    void categoryList(List<DropDownContent> dropDownContents);

}
