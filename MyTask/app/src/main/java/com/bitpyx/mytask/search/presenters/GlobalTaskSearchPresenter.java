package com.bitpyx.mytask.search.presenters;

import com.bitpyx.mytask.data.model.DropDownContent;
import com.bitpyx.mytask.data.model.Task;
import com.bitpyx.mytask.search.interactors.GlobalTaskSearchInteractor;
import com.bitpyx.mytask.search.interactors.IGlobalTaskSearchInteractor;
import com.bitpyx.mytask.search.views.IGlobalTaskSearchView;

import java.util.List;

/**
 * @author Amrit Yadav
 * @since 6/24/2017
 */
public class GlobalTaskSearchPresenter implements IGlobalTaskSearchPresenter<IGlobalTaskSearchView>{
    private IGlobalTaskSearchView mGlobalTaskSearchView;
    private IGlobalTaskSearchInteractor mGlobalTaskSearchInteractor;
    @Override
    public void attachView(IGlobalTaskSearchView mGlobalTaskSearchView) {
        this.mGlobalTaskSearchView=mGlobalTaskSearchView;
        mGlobalTaskSearchInteractor=new GlobalTaskSearchInteractor(mGlobalTaskSearchView.getContext());
    }

    @Override
    public void searchTask(String searchQuery) {
        List<Task> tasks = mGlobalTaskSearchInteractor.filterTask(searchQuery);
        mGlobalTaskSearchView.filteredTaskList(tasks);
    }

    @Override
    public void getCategoryList() {
        List<DropDownContent> dropDownContents = mGlobalTaskSearchInteractor.fetchCategoryList();
        mGlobalTaskSearchView.categoryList(dropDownContents);
    }

    @Override
    public void detachView() {

    }
}
