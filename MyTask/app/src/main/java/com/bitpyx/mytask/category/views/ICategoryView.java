package com.bitpyx.mytask.category.views;

import com.bitpyx.mytask.data.model.DropDownContent;
import com.bitpyx.mytask.data.model.Task;
import com.bitpyx.mytask.utility.IMvpView;

import java.util.List;

/**
 * @author Amrit Yadav
 * @since 6/24/2017
 */
public interface ICategoryView extends IMvpView {
    void setTaskList(List<Task> taskList);

    void categoryList(List<DropDownContent> dropDownContents);

    void setAdapter(List<Task> taskList);
}
