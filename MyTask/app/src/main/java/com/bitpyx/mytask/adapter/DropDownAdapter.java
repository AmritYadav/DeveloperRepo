package com.bitpyx.mytask.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bitpyx.mytask.R;
import com.bitpyx.mytask.data.model.DropDownContent;

import java.util.List;

/**
 * @author Amrit Yadav
 * @since  6/10/2017
 */
public class DropDownAdapter extends BaseAdapter {

    private Context mContext;
    private List<DropDownContent> mDropDownContents;
    public DropDownAdapter(Context context, List<DropDownContent> mDropDownContents) {
        this.mContext=context;
        this.mDropDownContents=mDropDownContents;
    }

    @Override
    public int getCount() {
        return mDropDownContents.size();
    }

    @Override
    public Object getItem(int position) {
        return mDropDownContents.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        final DropDownContent dropDownContents = mDropDownContents.get(position);

        Holder holder;
        View rowView = convertView;

        if(rowView==null){
            holder = new Holder();
            rowView = LayoutInflater.from(mContext).inflate(R.layout.row_layout_list, parent, false);
            holder.textName=(TextView) rowView.findViewById(R.id.text_priority);
            holder.img=(ImageView) rowView.findViewById(R.id.img_dropdown_item);
            holder.view = rowView.findViewById(R.id.view_priority_color_code);
            rowView.setTag(holder);
        } else {
            holder = (Holder) rowView.getTag();
        }

        holder.textName.setText(dropDownContents.getName());

        if(dropDownContents.getColorCode()!=null){
            if(dropDownContents.getColorCode().trim().isEmpty())
                holder.view.setVisibility(View.GONE);
            else
                holder.view.setBackgroundColor(Color.parseColor(dropDownContents.getColorCode()));

            if(dropDownContents.getDrawableResourceId()==0)
                holder.img.setVisibility(View.GONE);
            else
                holder.img.setImageResource(dropDownContents.getDrawableResourceId());
        } else {
            holder.view.setVisibility(View.GONE);
            holder.img.setVisibility(View.GONE);
        }


        return rowView;
    }

    private class Holder {
        TextView textName;
        ImageView img;
        View view;
    }

}
