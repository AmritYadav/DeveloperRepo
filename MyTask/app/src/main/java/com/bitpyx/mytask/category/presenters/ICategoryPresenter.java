package com.bitpyx.mytask.category.presenters;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.bitpyx.mytask.data.model.Task;
import com.bitpyx.mytask.utility.IBasePresenter;

import java.util.List;

/**
 * @author Amrit Yadav
 * @since 6/24/2017
 */
public interface ICategoryPresenter<V> extends IBasePresenter<V> {
    void fetchTaskListByCategory(int categoryId);

    void getCategoryList();

    void removeTask(int taskId);

    void updateListOnTaskSwipeRemove(List<Task> taskList);

    void addUndoTask(List<Task> undoTask);

    void selectListItem(View view, Task task);

    void unSelectListItem(View view, Task task);

    void onDestroyActionMode(List<Task> mTaskList, List<Integer> mListItemsToUpdate, RecyclerView mRecyclerView);

    void onActionItemClicked(List<Task> mTaskList, List<Integer> mListItemsToUpdate, RecyclerView mRecyclerView, int categoryId);
}
