package com.bitpyx.mytask.complete.presenters;

import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.bitpyx.mytask.R;
import com.bitpyx.mytask.adapter.TaskAdapter;
import com.bitpyx.mytask.complete.interactors.ITaskCompleteInteractor;
import com.bitpyx.mytask.complete.interactors.TaskCompleteInteractor;
import com.bitpyx.mytask.complete.view.ITaskCompleteView;
import com.bitpyx.mytask.data.model.DropDownContent;
import com.bitpyx.mytask.data.model.Task;

import java.util.List;

/**
 * @author Amrit Yadav
 * @since 7/29/2017.
 */
public class TaskCompletePresenter implements ITaskCompletePresenter<ITaskCompleteView> {
    private ITaskCompleteInteractor mTaskCompleteInteractor;
    private ITaskCompleteView mTaskCompleteView;

    @Override
    public void attachView(ITaskCompleteView view) {
        mTaskCompleteView = view;
        mTaskCompleteInteractor = new TaskCompleteInteractor(mTaskCompleteView.getContext());
    }

    @Override
    public void requestTaskCompleteList() {
        List<Task> taskCompleteList = mTaskCompleteInteractor.fetchTaskCompleteList();
        mTaskCompleteView.showTaskCompleteList(taskCompleteList);
    }

    @Override
    public void getCategoryList() {
        List<DropDownContent> dropDownContents = mTaskCompleteInteractor.fetchCategoryList();
        mTaskCompleteView.categoryList(dropDownContents);
    }

    @Override
    public void removeTask(int taskId) {
        mTaskCompleteInteractor.deleteTask(taskId);
    }

    @Override
    public void updateListOnTaskSwipeRemove(List<Task> taskList) {
       mTaskCompleteView.showTaskCompleteList(taskList);
    }

    @Override
    public void addUndoTask(List<Task> undoTask) {
        mTaskCompleteInteractor.saveUndoTask(undoTask);
    }

    @Override
    public void selectListItem(View view, Task task) {
        task.setSelected(true);
        view.setBackgroundColor(ContextCompat.getColor(mTaskCompleteView.getContext()
                , R.color.listSelectedColor));
    }

    @Override
    public void unSelectListItem(View view, Task task) {
        task.setSelected(false);
        view.setBackgroundColor(ContextCompat.getColor(mTaskCompleteView.getContext()
                , R.color.white));
    }

    @Override
    public void onDestroyActionMode(List<Task> mTaskList, List<Integer> mListItemsToRestore
            , RecyclerView mRecyclerView) {

        int listSize = mListItemsToRestore.size();

        for (int i = 0; i < listSize; i++) {
            int position = mListItemsToRestore.get(i);
            Task task = mTaskList.get(position);
            task.setSelected(false);
        }
        TaskAdapter adapter = (TaskAdapter) mRecyclerView.getAdapter();
        adapter.refreshAdapter(mTaskList);
        mListItemsToRestore.clear();
    }

    @Override
    public void onActionItemClicked(List<Task> mTaskList, List<Integer> mListItemsToRestore
            , RecyclerView mRecyclerView) {
        mTaskCompleteInteractor.updateTasksStatus(mTaskList, mListItemsToRestore);
        requestTaskCompleteList();
        mTaskCompleteView.showToast("Task restored", Toast.LENGTH_SHORT);
    }

    @Override
    public void detachView() {

    }
}
