package com.bitpyx.mytask.today.views;

import com.bitpyx.mytask.data.model.DropDownContent;
import com.bitpyx.mytask.data.model.Task;
import com.bitpyx.mytask.utility.IMvpView;

import java.util.List;

/**
 * @author Amrit Yadav
 * @since 6/17/2017
 */
public interface ITodayView extends IMvpView {

    void setAdapter(List<Task> taskList);

    void categoryList(List<DropDownContent> dropDownContents);
}
