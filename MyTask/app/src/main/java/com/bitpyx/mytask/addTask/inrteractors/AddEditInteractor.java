package com.bitpyx.mytask.addTask.inrteractors;

import android.content.Context;

import com.bitpyx.mytask.data.model.DropDownContent;
import com.bitpyx.mytask.data.model.Task;
import com.bitpyx.mytask.data.source.local.dbconstants.DatabaseHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Amrit Yadav
 * @since 6/17/2017
 */

public class AddEditInteractor implements IAddEditInteractor{

    private DatabaseHelper mDatabaseHelper;
    public AddEditInteractor(Context context) {
        mDatabaseHelper = new DatabaseHelper(context);
    }

    @Override
    public long insertNewCategory(String categoryName) {
        return mDatabaseHelper.addTaskCategory(categoryName);
    }

    @Override
    public boolean categoryExists(String categoryName) {
        return mDatabaseHelper.isCategoryExists(categoryName);
    }

    @Override
    public List<DropDownContent> fetchCategoryList() {
        return mDatabaseHelper.getCategoryList();
    }

    @Override
    public List<DropDownContent> fetchPriorityList() {
        return getPriorityList();
    }

    @Override
    public List<DropDownContent> fetchRepeatedList() {
        return getRepeatedList();
    }

    @Override
    public String fetchCategoryName(int categoryId) {
        return mDatabaseHelper.getCategoryName(categoryId);
    }

    @Override
    public long saveNewTask(Task task) {
        return mDatabaseHelper.addNewTask(task);
    }

    @Override
    public long updateTask(Task task) {
        return mDatabaseHelper.updateTask(task);
    }

    public List<DropDownContent> getPriorityList(){

        List<DropDownContent> dropDownContents = new ArrayList<>();

        DropDownContent dropDownContentL = new DropDownContent();
        dropDownContentL.setName("Low");
        dropDownContentL.setColorCode("#FFC107");
        dropDownContentL.setType("P");

        DropDownContent dropDownContentM = new DropDownContent();
        dropDownContentM.setName("Medium");
        dropDownContentM.setColorCode("#1565C0");
        dropDownContentM.setType("P");

        DropDownContent dropDownContentH = new DropDownContent();
        dropDownContentH.setName("High");
        dropDownContentH.setColorCode("#b71c1c");
        dropDownContentH.setType("P");

        dropDownContents.add(dropDownContentL);
        dropDownContents.add(dropDownContentM);
        dropDownContents.add(dropDownContentH);

        return dropDownContents;

    }

    public List<DropDownContent> getRepeatedList(){

        List<DropDownContent> dropDownContents = new ArrayList<>();

        DropDownContent dropDownContentW = new DropDownContent();
        dropDownContentW.setName("Weekly");
        dropDownContentW.setType("R");

        DropDownContent dropDownContentM = new DropDownContent();
        dropDownContentM.setName("Monthly");
        dropDownContentM.setType("R");

        DropDownContent dropDownContentY = new DropDownContent();
        dropDownContentY.setName("Yearly");
        dropDownContentY.setType("R");

        DropDownContent dropDownContentO = new DropDownContent();
        dropDownContentO.setName("One Time");
        dropDownContentO.setType("R");

        dropDownContents.add(dropDownContentO);
        dropDownContents.add(dropDownContentW);
        dropDownContents.add(dropDownContentM);
        dropDownContents.add(dropDownContentY);

        return dropDownContents;

    }

}
