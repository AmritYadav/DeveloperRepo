package com.bitpyx.mytask.complete.interactors;

import android.content.Context;

import com.bitpyx.mytask.data.model.DropDownContent;
import com.bitpyx.mytask.data.model.Task;
import com.bitpyx.mytask.data.source.local.dbconstants.DatabaseHelper;

import java.util.List;

/**
 * @author Amrit Yadav
 * @since 7/29/2017.
 */
public class TaskCompleteInteractor implements ITaskCompleteInteractor {
    private DatabaseHelper mDatabaseHelper;

    public TaskCompleteInteractor(Context context) {
        this.mDatabaseHelper = new DatabaseHelper(context);
    }

    @Override
    public List<Task> fetchTaskCompleteList() {
        return mDatabaseHelper.getTaskCompleteList();
    }

    @Override
    public List<DropDownContent> fetchCategoryList() {
        return mDatabaseHelper.getCategoryList();
    }

    @Override
    public void deleteTask(int taskId) {
        mDatabaseHelper.deleteTask(taskId);
    }

    @Override
    public void saveUndoTask(List<Task> undoTaskList) {
        for (Task task : undoTaskList) {
            mDatabaseHelper.addNewTask(task);
        }
    }

    @Override
    public void updateTasksStatus(List<Task> mTaskList, List<Integer> mListItemsToUpdate) {
        int listSize = mListItemsToUpdate.size();
        for (int i = 0; i < listSize; i++) {
            Task task = mTaskList.get(mListItemsToUpdate.get(i));
            task.setStatus("Pending");
            mDatabaseHelper.updateTask(task);
        }
    }
}
