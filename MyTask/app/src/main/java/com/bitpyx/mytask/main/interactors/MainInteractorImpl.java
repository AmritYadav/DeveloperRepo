package com.bitpyx.mytask.main.interactors;

import android.content.Context;

import com.bitpyx.mytask.data.model.Assignee;
import com.bitpyx.mytask.data.model.DropDownContent;
import com.bitpyx.mytask.data.source.local.dbconstants.DatabaseHelper;

import java.util.List;

/**
 * @author Amrit Yadav
 * @since 21-05-2017
 */
public class MainInteractorImpl implements IMainInteractor {

    private DatabaseHelper mDatabaseHelper;

    public MainInteractorImpl(Context context) {
        this.mDatabaseHelper=new DatabaseHelper(context);
    }

    @Override
    public void addDefaultTaskCategory(String newTaskCategory) {
        mDatabaseHelper.addTaskCategory(newTaskCategory);
    }

    @Override
    public boolean checkDefaultTaskCategory() {
        return mDatabaseHelper.isDefaultTaskCategoryAvailable();
    }

    @Override
    public List<DropDownContent> fetchCategoryList() {
        return mDatabaseHelper.getCategoryList();
    }

    @Override
    public int fetchTodayTaskCount() {
        return mDatabaseHelper.getTodayTaskCount();
    }

    @Override
    public int fetchTaskCompleteCount() {
        return mDatabaseHelper.getTaskCompleteList().size();
    }

    @Override
    public List<Assignee> fetchAssigneeList() {
        return mDatabaseHelper.getAssigneeList();
    }

    @Override
    public void addAppCreatedCategoryList(List<String> categoryList) {
        for (String taskCategory : categoryList) {
            mDatabaseHelper.addTaskCategory(taskCategory);
        }
    }

}
