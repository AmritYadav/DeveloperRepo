package com.bitpyx.mytask.category.manage.interactors;

import android.content.Context;

import com.bitpyx.mytask.data.model.DropDownContent;
import com.bitpyx.mytask.data.source.local.dbconstants.DatabaseHelper;

import java.util.List;

/**
 * @author Amrit Yadav
 * @since 7/9/2017.
 */
public class MCategoryInteractor implements IMCategoryInteractor{
    private DatabaseHelper mDatabaseHelper;
    public MCategoryInteractor(Context context) {
        mDatabaseHelper = new DatabaseHelper(context);
    }

    @Override
    public List<DropDownContent> fetchCategoryList() {
        return mDatabaseHelper.getCategoryList();
    }

    @Override
    public boolean categoryExists(String categoryName) {
        return mDatabaseHelper.isCategoryExists(categoryName);
    }

    @Override
    public int fetchTaskCountCountForCategory(int categoryId) {
        return mDatabaseHelper.getTaskCount(categoryId);
    }

    @Override
    public long insertNewCategory(String categoryName) {
        return mDatabaseHelper.addTaskCategory(categoryName);
    }

    @Override
    public long updateCategory(String category, int categoryId) {
        return mDatabaseHelper.updateCategory(category, categoryId);
    }

    @Override
    public void deleteCategory(DropDownContent categoryObj) {
        mDatabaseHelper.deleteCategory(categoryObj);
    }
}
