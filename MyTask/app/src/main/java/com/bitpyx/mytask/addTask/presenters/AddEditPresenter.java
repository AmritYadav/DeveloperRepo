package com.bitpyx.mytask.addTask.presenters;

import android.support.v4.content.ContextCompat;
import android.widget.Toast;

import com.bitpyx.mytask.R;
import com.bitpyx.mytask.addTask.inrteractors.AddEditInteractor;
import com.bitpyx.mytask.addTask.inrteractors.IAddEditInteractor;
import com.bitpyx.mytask.addTask.views.IAddEditView;
import com.bitpyx.mytask.data.model.DropDownContent;
import com.bitpyx.mytask.data.model.Task;

import java.util.List;

/**
 * @author Amrit Yadav
 * @since 6/17/2017
 */
public class AddEditPresenter implements IAddEditPresenter<IAddEditView>{

    private IAddEditView mAddEditView;
    private IAddEditView.onTaskSaved mTaskSaved;
    private IAddEditView.onTaskUpdated mTaskUpdated;
    private IAddEditInteractor mAddEditInteractor;
    @Override
    public void attachView(IAddEditView mAddEditView) {
        this.mAddEditView=mAddEditView;
        mAddEditInteractor = new AddEditInteractor(mAddEditView.getContext());
    }

    @Override
    public void saveNewCategory(String categoryName) {
        boolean categoryExists = mAddEditInteractor.categoryExists(categoryName);
        if(categoryExists){
            mAddEditView.showToast("Category Already exists", Toast.LENGTH_SHORT);
        } else {
            long rowId = mAddEditInteractor.insertNewCategory(categoryName);
            String message = rowId > 0 ? "New category added" : "Failed to add new category";
            mAddEditView.showToast(message, Toast.LENGTH_SHORT);
        }
    }

    @Override
    public void fetchTaskCategories() {
        List<DropDownContent> categoryList = mAddEditInteractor.fetchCategoryList();
        mAddEditView.showCategoryList(categoryList);
    }

    @Override
    public void fetchTaskPriorities() {
        List<DropDownContent> priorityList = mAddEditInteractor.fetchPriorityList();
        mAddEditView.showPriorityList(priorityList);
    }

    @Override
    public void fetchTaskRepeated() {
        List<DropDownContent> repeatedList = mAddEditInteractor.fetchRepeatedList();
        mAddEditView.showRepeatedList(repeatedList);
    }

    @Override
    public void getCategoryTypeById(int categoryId) {
        String categoryName = mAddEditInteractor.fetchCategoryName(categoryId);
        mAddEditView.categoryName(categoryName);
    }

    @Override
    public void saveTask(Task task, IAddEditView.onTaskSaved mTaskSaved) {
        this.mTaskSaved = mTaskSaved;
        if(task.getTitle()!=null) {
            if (task.getTitle().trim().isEmpty()) {
                mTaskSaved.onFail("Enter title of your task");
            } else {
                long rowId = mAddEditInteractor.saveNewTask(task);
                if (rowId > 0)
                    mTaskSaved.onSuccess("Task Added");
                else
                    mTaskSaved.onFail("Failed to add task");
            }
        } else {
            mTaskSaved.onFail("Enter title of your task");
        }
    }

    @Override
    public void updateTask(Task task, IAddEditView.onTaskUpdated onTaskUpdated) {
        this.mTaskUpdated = onTaskUpdated;
        if(task.getTitle()!=null) {
            if (task.getTitle().trim().isEmpty()) {
                mTaskUpdated.onUpdateFail("Enter title of your task");
            } else {
                long rowId = mAddEditInteractor.updateTask(task);
                if (rowId > 0)
                    mTaskUpdated.onUpdateSuccess("Task Updated");
                else
                    mTaskUpdated.onUpdateFail("Failed to update task");
            }
        } else {
            mTaskUpdated.onUpdateFail("Enter title of your task");
        }
    }

    @Override
    public void checkPriorityColorCode(String priority) {
        int priorityColor = 0;
        switch (priority){
            case "Normal":
                priorityColor = ContextCompat
                        .getColor(mAddEditView.getContext(), R.color.priorityNormal);
                break;
            case "Medium":
                priorityColor = ContextCompat
                        .getColor(mAddEditView.getContext(), R.color.priorityMedium);
                break;
            case "High":
                priorityColor = ContextCompat
                        .getColor(mAddEditView.getContext(), R.color.priorityHigh);
                break;
        }
        mAddEditView.setPriorityTextColor(priorityColor);
    }

    @Override
    public void detachView() {

    }
}
