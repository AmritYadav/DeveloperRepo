package com.bitpyx.mytask.receiver;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;

import com.bitpyx.mytask.R;
import com.bitpyx.mytask.data.model.Task;
import com.bitpyx.mytask.data.source.local.dbconstants.DatabaseHelper;
import com.bitpyx.mytask.main.MainActivity;

import java.util.List;

/**
 * @author Amrit Yadav
 * @since 01-07-2017
 */
public class AlarmReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        System.out.println("ALARM SET");
        DatabaseHelper mDatabaseHelper = new DatabaseHelper(context);
        List<Task> todayTaskList = mDatabaseHelper.getTodayTaskList();
        showNotification(context, todayTaskList);
    }

    public void showNotification(Context context, List<Task> todayTaskList) {

        int notificationId = 1;

        Intent resultIntent = new Intent(context, MainActivity.class);
        resultIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent piResult = PendingIntent.getActivity(context, 0, resultIntent, 0);

//        Bitmap largeIcon = BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_launcher);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(context)
                .setSmallIcon(R.drawable.ic_logo)
                .setContentTitle("Today's Task")
                .setTicker("Today's Task")
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                .setColor(ContextCompat.getColor(context, R.color.colorPrimary))
                .setVibrate(new long[]{1000, 1000})
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setContentIntent(piResult)
                .setAutoCancel(true);
        NotificationCompat.InboxStyle inboxStyle =
                new NotificationCompat.InboxStyle();

        // Sets a title for the Inbox in expanded layout
        inboxStyle.setBigContentTitle("Today's Task");
        System.out.println("TodayTaskList " + todayTaskList.size());
        // Moves events into the expanded layout
        for (Task task : todayTaskList) {
            if (task.getViewType() == 0) {
                inboxStyle.setSummaryText("Tap to view your task");
            } else {
                inboxStyle.addLine(task.getTitle());
            }
        }

        if (todayTaskList.isEmpty()) {
            builder.setContentTitle("No Task Today");
            builder.setContentText("Create a new task to get started");
        } else {
            builder.setContentText("You have " + (todayTaskList.size() - 1) + " due task today");
        }
        // Moves the expanded layout object into the notification object.
        builder.setStyle(inboxStyle);

        NotificationManager notificationManager = (NotificationManager) context
                .getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(notificationId, builder.build());

    }

}
