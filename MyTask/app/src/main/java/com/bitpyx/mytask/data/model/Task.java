package com.bitpyx.mytask.data.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * @author Amrit Yadav
 * @since 6/10/2017
 */
public class Task implements Parcelable {

    private String title, reminder, description, date, priority, repeat, status, taskCreationDate;
    private int categoryId, taskId, viewType, repeatFrequency;
    private int taskHeaderTitleColor;
    private boolean isSelected;

    private Assignee assignee;

    //Empty constructor to prevent initialization of the private fields
    public Task() {
    }

    public static final Creator<Task> CREATOR = new Creator<Task>() {
        @Override
        public Task createFromParcel(Parcel in) {
            return new Task(in);
        }

        @Override
        public Task[] newArray(int size) {
            return new Task[size];
        }
    };

    public int getTaskId() {
        return taskId;
    }

    public void setTaskId(int taskId) {
        this.taskId = taskId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public String getReminder() {
        return reminder;
    }

    public void setReminder(String reminder) {
        this.reminder = reminder;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public String getRepeat() {
        return repeat;
    }

    public void setRepeat(String repeat) {
        this.repeat = repeat;
    }

    public int getRepeatFrequency() {
        return repeatFrequency;
    }

    public void setRepeatFrequency(int repeatFrquency) {
        this.repeatFrequency = repeatFrquency;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTaskCreationDate() {
        return taskCreationDate;
    }

    public void setTaskCreationDate(String taskCreationDate) {
        this.taskCreationDate = taskCreationDate;
    }

    public int getViewType() {
        return viewType;
    }

    public void setViewType(int viewType) {
        this.viewType = viewType;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public Assignee getAssignee() {
        return assignee;
    }

    public void setAssignee(Assignee assignee) {
        this.assignee = assignee;
    }

    public int getTaskHeaderTitleColor() {
        return taskHeaderTitleColor;
    }

    public void setTaskHeaderTitleColor(int taskHeaderTitleColor) {
        this.taskHeaderTitleColor = taskHeaderTitleColor;
    }

    @Override
    public String toString() {
        return "Task{" +
                "title='" + title + '\'' +
                ", reminder='" + reminder + '\'' +
                ", description='" + description + '\'' +
                ", date='" + date + '\'' +
                ", priority='" + priority + '\'' +
                ", repeat='" + repeat + '\'' +
                ", repeatFrequency='" + repeatFrequency + '\'' +
                ", status='" + status + '\'' +
                ", taskCreationDate='" + taskCreationDate + '\'' +
                ", categoryId=" + categoryId +
                ", taskId=" + taskId +
                ", viewType=" + viewType +
                ", isSelected=" + isSelected +
                ", assignee=" + assignee +
                ", taskHeaderTitleColor=" + taskHeaderTitleColor +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeString(reminder);
        dest.writeString(description);
        dest.writeString(date);
        dest.writeString(priority);
        dest.writeString(repeat);
        dest.writeInt(repeatFrequency);
        dest.writeString(status);
        dest.writeString(taskCreationDate);
        dest.writeInt(categoryId);
        dest.writeInt(taskId);
        dest.writeInt(viewType);
        dest.writeInt(taskHeaderTitleColor);
        dest.writeByte((byte) (isSelected ? 1 : 0));
        // Add inner class
        dest.writeParcelable(assignee, flags);
    }

    protected Task(Parcel in) {
        title = in.readString();
        reminder = in.readString();
        description = in.readString();
        date = in.readString();
        priority = in.readString();
        repeat = in.readString();
        repeatFrequency = in.readInt();
        status = in.readString();
        taskCreationDate = in.readString();
        categoryId = in.readInt();
        taskId = in.readInt();
        viewType = in.readInt();
        taskHeaderTitleColor = in.readInt();
        isSelected = in.readByte() != 0;
        assignee = in.readParcelable(Assignee.class.getClassLoader());
    }

}