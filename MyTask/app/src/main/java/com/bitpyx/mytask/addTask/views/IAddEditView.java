package com.bitpyx.mytask.addTask.views;

import com.bitpyx.mytask.data.model.DropDownContent;
import com.bitpyx.mytask.utility.IMvpView;

import java.util.List;

/**
 * @author Amrit Yadav
 * @since 6/17/2017
 */
public interface IAddEditView extends IMvpView{

    void showCategoryList(List<DropDownContent> categoryList);

    void showPriorityList(List<DropDownContent> priorityList);

    void showRepeatedList(List<DropDownContent> repeatedList);

    void showToast(String message, int toastDuration);

    void categoryName(String categoryName);

    interface onTaskSaved {
        void onSuccess(String successMessage);
        void onFail(String errorMessage);
    }

    interface onTaskUpdated{
        void onUpdateSuccess(String successMessage);
        void onUpdateFail(String errorMessage);
    }

    void setPriorityTextColor(int priorityTextColor);

}
