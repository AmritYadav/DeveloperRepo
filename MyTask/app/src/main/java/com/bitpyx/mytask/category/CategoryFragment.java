package com.bitpyx.mytask.category;

import android.animation.Animator;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.LinearInterpolator;
import android.widget.RelativeLayout;

import com.bitpyx.mytask.R;
import com.bitpyx.mytask.SwipeUtil.SwipeUtil;
import com.bitpyx.mytask.adapter.TaskAdapter;
import com.bitpyx.mytask.addTask.AddEditTaskActivity;
import com.bitpyx.mytask.category.presenters.CategoryPresenter;
import com.bitpyx.mytask.category.presenters.ICategoryPresenter;
import com.bitpyx.mytask.category.views.ICategoryView;
import com.bitpyx.mytask.data.model.DropDownContent;
import com.bitpyx.mytask.data.model.Task;
import com.bitpyx.mytask.main.MainActivity;
import com.bitpyx.mytask.utility.Common;
import com.bitpyx.mytask.utility.RecyclerTouchListener;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link CategoryFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CategoryFragment extends Fragment implements ICategoryView
        , View.OnClickListener {
    private static final String ARG_COLUMN_CATEGORY_ID = "categoryId";
    private static final String EXTRA_MAIN_FRAGMENT = "mainFragment";
    private static final String EXTRA_CATEGORY_FRAGMENT_ID = "categoryFragmentId";
    private static final String EXTRA_CATEGORY_FRAGMENT_TAG = "categoryFragmentTitle";
    private int statusBarColor;
    protected RecyclerView mRecyclerView;
    private int categoryId;
    private TaskAdapter mCategoryAdapter;
    private List<Task> mTaskList;
    private List<DropDownContent> mCategoryList;
    private List<Task> mRemovedTaskList;
    private ICategoryPresenter<ICategoryView> mICategoryPresenter;
    private boolean mIsMultiSelect = false;
    private Snackbar mSnackbar;
    private List<Integer> mListItemsToUpdate = new ArrayList<>();
    private ActionMode mActionMode;
    private FloatingActionButton mFabAddEditTask;
    private RelativeLayout mRelativeLayoutEmptyView;

    private ActionMode.Callback mActionModeCallback = new ActionMode.Callback() {

        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                //hold current color of status bar
                statusBarColor = getActivity().getWindow().getStatusBarColor();
                getActivity().getWindow()
                        .setStatusBarColor(ContextCompat.getColor(getActivity()
                                , R.color.actionModePrimaryColorDark));
            }

            CoordinatorLayout.LayoutParams layoutParams =
                    (CoordinatorLayout.LayoutParams) mFabAddEditTask.getLayoutParams();
            int fab_bottomMargin = layoutParams.bottomMargin;

            mFabAddEditTask.animate().translationY(mFabAddEditTask.getHeight() + fab_bottomMargin)
                    .setInterpolator(new LinearInterpolator()).setListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animation) {

                }

                @Override
                public void onAnimationEnd(Animator animation) {
                    if (mIsMultiSelect)
                        mFabAddEditTask.setVisibility(View.GONE);
                }

                @Override
                public void onAnimationCancel(Animator animation) {

                }

                @Override
                public void onAnimationRepeat(Animator animation) {

                }
            }).start();

            // Inflate a menu resource providing context menu items
            MenuInflater inflater = mode.getMenuInflater();
            inflater.inflate(R.menu.menu_multi_select, menu);
            MenuItem item = menu.findItem(R.id.action_send);
            item.setVisible(false);
            MenuItem itemRestoreTask = menu.findItem(R.id.action_restore);
            itemRestoreTask.setVisible(false);
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false;
        }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            mICategoryPresenter.onActionItemClicked(mTaskList, mListItemsToUpdate, mRecyclerView, categoryId);
            mActionMode = null;
            mIsMultiSelect = false;
            return false;
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                //return to "old" color of status bar
                getActivity().getWindow().setStatusBarColor(statusBarColor);
            }
            mFabAddEditTask.setVisibility(View.VISIBLE);
            mFabAddEditTask.animate().translationY(0).setInterpolator(new LinearInterpolator()).start();
            mActionMode = null;
            mIsMultiSelect = false;
            mICategoryPresenter.onDestroyActionMode(mTaskList, mListItemsToUpdate, mRecyclerView);
        }
    };

    public CategoryFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param categoryId navigation category sub menu id
     * @return A new instance of fragment CategoryFragment.
     */
    public static CategoryFragment newInstance(int categoryId) {
        CategoryFragment fragment = new CategoryFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_CATEGORY_ID, categoryId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onResume() {
        super.onResume();
        mICategoryPresenter.getCategoryList();
        mICategoryPresenter.fetchTaskListByCategory(categoryId);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mICategoryPresenter = new CategoryPresenter();
        mICategoryPresenter.attachView(this);
        if (getArguments() != null) {
            categoryId = getArguments().getInt(ARG_COLUMN_CATEGORY_ID);
        }
        mRemovedTaskList = new ArrayList<>();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_category, container, false);

        CoordinatorLayout mCorCoordinatorLayout = (CoordinatorLayout) container.getParent();

        mFabAddEditTask = (FloatingActionButton) mCorCoordinatorLayout
                .findViewById(R.id.fab_add_edit_task);

        mSnackbar = Snackbar.make(mCorCoordinatorLayout, "", 2000).setAction("UNDO", this)
                .addCallback(new Snackbar.Callback() {
                    @Override
                    public void onDismissed(Snackbar snackbar, int event) {
                        mRemovedTaskList.clear();
                    }

                    @Override
                    public void onShown(Snackbar snackbar) {

                    }
                });

        mRelativeLayoutEmptyView = (RelativeLayout) view.findViewById(R.id.layout_today_empty_view);

        mRecyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        // LinearLayoutManager is used here, this will layout the elements in a similar fashion
        // to the way ListView would layout elements. The RecyclerView.LayoutManager defines how
        // elements are laid out.
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        mRecyclerView.addOnItemTouchListener(new RecyclerTouchListener(getActivity()
                , mRecyclerView, new MainActivity.ClickListener() {
            @Override
            public void onClick(View view, int position) {

                Task task = mTaskList.get(position);

                if (mIsMultiSelect) {
                    if (task.isSelected()) {
                        mICategoryPresenter.unSelectListItem(view, task);
                        mListItemsToUpdate.removeAll(Arrays.asList(position));
                    } else {
                        mICategoryPresenter.selectListItem(view, task);
                        mListItemsToUpdate.add(position);
                    }

                    mActionMode.setTitle("" + mListItemsToUpdate.size());

                    if (mListItemsToUpdate.isEmpty() && mActionMode != null) {
                        mIsMultiSelect = false;
                        mActionMode.finish();
                        mActionMode = null;
                    }


                } else {
                    String categoryName = Common.getCategoryName(mCategoryList, categoryId);
                    Intent intent = new Intent(getActivity(), AddEditTaskActivity.class);
                    intent.putExtra(EXTRA_MAIN_FRAGMENT, "Category");
                    intent.putExtra(EXTRA_CATEGORY_FRAGMENT_ID, categoryId);
                    intent.putExtra(EXTRA_CATEGORY_FRAGMENT_TAG, categoryName);
                    intent.putExtra("Task", task);
                    intent.putExtra("Type", "edit");
                    startActivity(intent);
                }

            }

            @Override
            public void onLongClick(View view, int position) {
                mIsMultiSelect = true;

                if (!mListItemsToUpdate.contains(position)) {
                    Task task = mTaskList.get(position);
                    mICategoryPresenter.selectListItem(view, task);
                    mListItemsToUpdate.add(position);
                }

                if (mActionMode == null) {
                    mActionMode = getActivity().startActionMode(mActionModeCallback);
                }

                mActionMode.setTitle("" + mListItemsToUpdate.size());
            }
        }));

        DividerItemDecoration mDividerItemDecoration =
                new DividerItemDecoration(mRecyclerView.getContext(),
                        DividerItemDecoration.VERTICAL);
        mRecyclerView.addItemDecoration(mDividerItemDecoration);

        setSwipeForRecyclerView();

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    public void setCategory(int categoryId) {
        mICategoryPresenter.fetchTaskListByCategory(categoryId);
        mCategoryAdapter = new TaskAdapter(getActivity(), mTaskList, mCategoryList);
        mRecyclerView.setAdapter(mCategoryAdapter);
    }

    @Override
    public void setAdapter(List<Task> taskList) {
        if (mActionMode != null)
            mActionMode.finish();

        mTaskList = taskList;
        if (mTaskList.size() > 0) {
            mRecyclerView.setVisibility(View.VISIBLE);
            mRelativeLayoutEmptyView.setVisibility(View.GONE);
            mCategoryAdapter = new TaskAdapter(getActivity(), mTaskList, mCategoryList);
            mRecyclerView.setAdapter(mCategoryAdapter);
            mCategoryAdapter.notifyDataSetChanged();
        } else {
            mRecyclerView.setVisibility(View.GONE);
            mRelativeLayoutEmptyView.setVisibility(View.VISIBLE);
        }

    }

    private void setSwipeForRecyclerView() {

        SwipeUtil swipeHelper = new SwipeUtil(0, ItemTouchHelper.LEFT, getActivity()) {

            @Override
            public int getMovementFlags(RecyclerView recyclerView
                    , RecyclerView.ViewHolder viewHolder) {
                return viewHolder.getItemViewType() == 0
                        ? 0 : super.getMovementFlags(recyclerView, viewHolder);
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                int swipedPosition = viewHolder.getAdapterPosition();
                Task task = mTaskList.get(swipedPosition);
                mICategoryPresenter.removeTask(task.getTaskId());
                mRemovedTaskList.add(task);
                mCategoryAdapter = (TaskAdapter) mRecyclerView.getAdapter();
                mCategoryAdapter.pendingRemoval(swipedPosition);
                mICategoryPresenter.updateListOnTaskSwipeRemove(mTaskList);
                String message = "DELETE (" + mRemovedTaskList.size() + ")";
                mSnackbar.setText(message);
                mSnackbar.show();
            }

            @Override
            public int getSwipeDirs(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
                int position = viewHolder.getAdapterPosition();
                mCategoryAdapter = (TaskAdapter) mRecyclerView.getAdapter();
                if (mCategoryAdapter.isPendingRemoval(position)) {
                    return 0;
                }
                return super.getSwipeDirs(recyclerView, viewHolder);
            }
        };

        ItemTouchHelper mItemTouchHelper = new ItemTouchHelper(swipeHelper);
        mItemTouchHelper.attachToRecyclerView(mRecyclerView);
    }

    @Override
    public void onClick(View v) {
        mICategoryPresenter.addUndoTask(mRemovedTaskList);
        mICategoryPresenter.fetchTaskListByCategory(categoryId);
        mRemovedTaskList.clear();
    }

    public List<Task> getTaskList() {
        return mTaskList;
    }

    @Override
    public void setTaskList(List<Task> taskList) {
        mTaskList = taskList;
    }

    @Override
    public void categoryList(List<DropDownContent> dropDownContents) {
        mCategoryList = dropDownContents;
    }

}
