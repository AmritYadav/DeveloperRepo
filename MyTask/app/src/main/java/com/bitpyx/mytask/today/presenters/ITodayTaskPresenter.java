package com.bitpyx.mytask.today.presenters;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.bitpyx.mytask.data.model.Task;
import com.bitpyx.mytask.utility.IBasePresenter;

import java.util.List;

/**
 * @author Amrit Yadav
 * @since 6/17/2017.
 */

public interface ITodayTaskPresenter<V> extends IBasePresenter<V> {

    void fetchTodayTask();

    void fetchTodayTask(int contactId);

    void getCategoryList();

    void removeTask(int taskId);

    void updateListHeadersOnSwipeRemove(List<Task> taskList);

    void addUndoTask(List<Task> undoTask);

    void selectListItem(View view, Task task);

    void unSelectListItem(View view, Task task);

    void onDestroyActionMode(List<Task> mTaskList, List<Integer> mListItemsToRemove
            , RecyclerView mRecyclerView);

    void onActionItemClicked(List<Task> mTaskList, List<Integer> mListItemsToRemove
            , RecyclerView mRecyclerView, int contactId);

}
