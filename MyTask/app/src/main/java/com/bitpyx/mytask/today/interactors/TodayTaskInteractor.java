package com.bitpyx.mytask.today.interactors;

import android.content.Context;

import com.bitpyx.mytask.data.model.DropDownContent;
import com.bitpyx.mytask.data.model.Task;
import com.bitpyx.mytask.data.source.local.dbconstants.DatabaseHelper;

import org.joda.time.LocalDateTime;

import java.util.ArrayList;
import java.util.List;

import static com.bitpyx.mytask.utility.Common.defaultDateTimeFormatter;

/**
 * @author Amrit Yadav
 * @since 6/17/2017.
 */
public class TodayTaskInteractor implements ITodayTaskInteractor{

    private DatabaseHelper mDatabaseHelper;

    public TodayTaskInteractor(Context context) {
        mDatabaseHelper = new DatabaseHelper(context);
    }

    @Override
    public List<Task> fetchTodayTask() {
        List<Task> taskList = new ArrayList<>();
        taskList.addAll(mDatabaseHelper.getOverdueTaskList());
        taskList.addAll(mDatabaseHelper.getTodayTaskList());
        return taskList;
    }

    @Override
    public List<Task> fetchTodayTask(int contactId) {
        List<Task> taskList = new ArrayList<>();
        taskList.addAll(mDatabaseHelper.getOverdueTaskList(contactId));
        taskList.addAll(mDatabaseHelper.getTodayTaskList(contactId));
        return taskList;
    }

    @Override
    public List<DropDownContent> fetchCategoryList() {
        return mDatabaseHelper.getCategoryList();
    }

    @Override
    public void deleteTask(int taskId) {
        mDatabaseHelper.deleteTask(taskId);
    }

    @Override
    public void saveUndoTask(List<Task> undoTaskList) {
        for (Task task : undoTaskList) {
            mDatabaseHelper.addNewTask(task);
        }
    }

    @Override
    public void UpdateTasksStatus(List<Task> mTaskList, List<Integer> mListItemsToUpdate) {
        try {
            int listSize = mListItemsToUpdate.size();
            for (int i = 0; i < listSize; i++) {
                Task task = mTaskList.get(mListItemsToUpdate.get(i));
                LocalDateTime localDateTime = LocalDateTime.parse(task.getDate(), defaultDateTimeFormatter);

                switch (task.getRepeat()) {
                    case "One Time":
                        task.setStatus("Complete");
                        break;
                    case "Weekly":
                        task.setDate(defaultDateTimeFormatter
                                .print(localDateTime.plusWeeks(task.getRepeatFrequency())));
                        break;
                    case "Monthly":
                        task.setDate(defaultDateTimeFormatter
                                .print(localDateTime.plusMonths(task.getRepeatFrequency())));
                        break;
                    case "Yearly":
                        task.setDate(defaultDateTimeFormatter
                                .print(localDateTime.plusYears(task.getRepeatFrequency())));
                        break;
                }
                mDatabaseHelper.updateTask(task);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
