package com.bitpyx.mytask.utility;

import android.content.Context;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

import com.bitpyx.mytask.R;
import com.bitpyx.mytask.main.MainActivity;

/**
 * @author Amrit Yadav
 * @since 6/17/2017
 */
public class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

    private GestureDetector gestureDetector;
    private MainActivity.ClickListener clickListener;

    public RecyclerTouchListener(Context context, final RecyclerView recyclerView, final MainActivity.ClickListener clickListener) {
        this.clickListener = clickListener;
        gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
            @Override
            public boolean onSingleTapUp(MotionEvent e) {
                return true;
            }

            @Override
            public void onLongPress(MotionEvent e) {
                View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                if (child != null && clickListener != null) {
                    RecyclerView.Adapter adapter = recyclerView.getAdapter();
                    int viewType = adapter.getItemViewType(recyclerView.getChildLayoutPosition(child));
                    if(viewType!=0){
                        View ib = child.findViewById(R.id.ib_task_drag);
                        if(ib!=null && ib.isShown()){
                            if (e.getX() >= ib.getLeft() + ViewCompat.getTranslationX(ib)) {
                                clickListener.onLongClick(ib, recyclerView.getChildLayoutPosition(child));
                            } else {
                                clickListener.onLongClick(child, recyclerView.getChildLayoutPosition(child));
                            }
                        } else {
                            clickListener.onLongClick(child, recyclerView.getChildLayoutPosition(child));
                        }
                    }
                }
            }
        });
    }

    @Override
    public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
        View child = rv.findChildViewUnder(e.getX(), e.getY());
        if (child != null && clickListener != null && gestureDetector.onTouchEvent(e)) {
            RecyclerView.Adapter adapter = rv.getAdapter();
            int viewType = adapter.getItemViewType(rv.getChildLayoutPosition(child));
            if(viewType!=0){
                clickListener.onClick(child, rv.getChildLayoutPosition(child));
            }
        }
        return false;
    }

    @Override
    public void onTouchEvent(RecyclerView rv, MotionEvent e) {
    }

    @Override
    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

    }

}

