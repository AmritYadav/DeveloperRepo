package com.bitpyx.mytask.filter.interactors;

import com.bitpyx.mytask.data.model.DropDownContent;
import com.bitpyx.mytask.data.model.Task;

import java.util.List;

/**
 * @author Amrit Yadav
 * @since 7/13/2017.
 */
public interface ITaskFilterInteractor {

    List<Task> getTaskByFilter(int filterId);

    List<DropDownContent> fetchCategoryList();

    void deleteTask(int taskId);

    void saveUndoTask(List<Task> undoTaskList);

    void UpdateTasksStatus(List<Task> mTaskList, List<Integer> mListItemsToUpdate);

}
