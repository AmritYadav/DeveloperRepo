package com.bitpyx.mytask.utility;

import android.os.Environment;

import com.bitpyx.mytask.data.model.DropDownContent;
import com.bitpyx.mytask.data.model.Task;
import com.bitpyx.mytask.data.source.local.dbconstants.DatabaseContract;

import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;

import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;

/**
 * @author Amrit Yadav
 * @since 21-05-2017
 */
public class Common {

    public static final int TASK_PRIORITY_NORMAL = 0;
    public static final int TASK_PRIORITY_MEDIUM = 1;
    public static final int TASK_PRIORITY_HIGH = 2;

    public static  DateTimeFormatter defaultDateTimeFormatter = DateTimeFormat
            .forPattern("yyyy-MM-dd hh:mm a");
    private static  DateTimeFormatter dueDateTimeFormatter = DateTimeFormat
            .forPattern("dd MMM");
    private static  DateTimeFormatter dueDateTimeFormatterWithYear = DateTimeFormat
            .forPattern("dd MMM YYYY");
    private static String taskDirectory = Environment
            .getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS)
            + File.separator + "MyTaskExcelData" + File.separator;
    private static WritableWorkbook mWorkbook;

   /* public static boolean hasInternetConnectivity() {
        boolean rc = false;
        ConnectivityManager cm = (ConnectivityManager) AzaanApplication.get()
        .getSystemService(Context.CONNECTIVITY_SERVICE);
        if (cm != null) {
            try {
                NetworkInfo activeNetworkInfo = cm.getActiveNetworkInfo();
                if (activeNetworkInfo != null && activeNetworkInfo.isAvailable()
                && activeNetworkInfo.isConnected()) {
                    rc = true;
                }
            } catch (Exception e) {
                Log.d(TAG, "hasInternetConnectivity: ");
            }
        }
        return rc;
    }*/

    public static boolean isAppDbDirectoryExists(){
        File file = new File(DatabaseContract.DB_LOCATION + File.separator
                + DatabaseContract.DATABASE_NAME);
        return file.exists();
    }

    public static void createTaskDocumentDirectory(){
        File directory = new File(taskDirectory);
        if(!directory.exists()) {
            boolean mkdirs = directory.mkdirs();
            System.out.println("Directory exists " + mkdirs);
        }
    }

    public static WritableSheet createTaskExcelHeader(String fileName){
        String csvTaskFile = fileName + " - "
                + defaultDateTimeFormatter.print(LocalDateTime.now())
                + ".xls";

        try {
            //file path
            File file = new File(taskDirectory, csvTaskFile);
            WorkbookSettings wbSettings = new WorkbookSettings();
            wbSettings.setLocale(new Locale("en", "EN"));
            mWorkbook = Workbook.createWorkbook(file, wbSettings);
            //Excel sheet name. 0 represents first sheet
            WritableSheet sheet = mWorkbook.createSheet("userList", 0);
            // column and row
            sheet.addCell(new Label(0, 0, "Title"));
            sheet.addCell(new Label(1, 0, "Description"));
            sheet.addCell(new Label(2, 0, "Category"));
            sheet.addCell(new Label(3, 0, "Due Date"));
            sheet.addCell(new Label(4, 0, "Priority"));
            sheet.addCell(new Label(5, 0, "Repeat"));
            sheet.addCell(new Label(6, 0, "RepeatFrequency"));
            sheet.addCell(new Label(7, 0, "Assigned To"));
            sheet.addCell(new Label(8, 0, "Status"));
            sheet.addCell(new Label(9, 0, "Creation Date"));
            return sheet;

        } catch (IOException | WriteException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void generatedTaskExcel(WritableSheet writableSheet, Task task, int row
            , String categoryName, boolean isLastTask) throws IOException, WriteException {

        writableSheet.addCell(new Label(0, row, task.getTitle()));
        writableSheet.addCell(new Label(1, row, task.getDescription()));
        writableSheet.addCell(new Label(2, row, categoryName));
        writableSheet.addCell(new Label(3, row, task.getDate()));
        writableSheet.addCell(new Label(4, row, task.getPriority()));
        writableSheet.addCell(new Label(5, row, task.getRepeat()));
        writableSheet.addCell(new Label(6, row, String.valueOf(task.getRepeatFrequency())));
        writableSheet.addCell(new Label(7, row, "None"));
        writableSheet.addCell(new Label(8, row, task.getStatus()));
        writableSheet.addCell(new Label(9, row, task.getTaskCreationDate()));

        if(isLastTask){
            mWorkbook.write();
            mWorkbook.close();
        }
    }

    public static String getPriorityById(int priorityId){
        return priorityId == 0 ? "Normal" : priorityId == 1 ? "Medium" : "High";
    }

    public static String getFormattedDate(String dueDateTime){

        LocalDate localDateDue = LocalDateTime.parse(dueDateTime, defaultDateTimeFormatter).toLocalDate();
        LocalDate localDateNow = LocalDate.now();

        if(localDateDue.getYear() == localDateNow.getYear()){
            if(localDateDue.isBefore(localDateNow)){
                return dueDateTimeFormatter.print(localDateDue);
            } else if(localDateDue.equals(localDateNow)){
                return "Today";
            } else if(localDateDue.equals(localDateNow.plusDays(1))){
                return "Tomorrow";
            } else {
                return dueDateTimeFormatter.print(localDateDue);
            }
        } else {
            return dueDateTimeFormatterWithYear.print(localDateDue);
        }
    }

    public static String getCategoryName(List<DropDownContent> categoryList, int categoryId){
        String category = "";
        for (DropDownContent dropDownContent : categoryList) {
            if (categoryId == dropDownContent.getCategoryId()) {
                category = dropDownContent.getName();
                break;
            }
        }
        return category;
    }

}
