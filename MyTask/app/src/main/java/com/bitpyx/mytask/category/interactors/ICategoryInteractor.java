package com.bitpyx.mytask.category.interactors;

import com.bitpyx.mytask.data.model.DropDownContent;
import com.bitpyx.mytask.data.model.Task;

import java.util.List;

/**
 * @author Amrit Yadav
 * @since 6/24/2017
 */
public interface ICategoryInteractor {

    List<Task> getTaskByCategory(int categoryId);

    List<DropDownContent> fetchCategoryList();

    void deleteTask(int taskId);

    void saveUndoTask(List<Task> undoTaskList);

    void UpdateTasksStatus(List<Task> mTaskList, List<Integer> mListItemsToUpdate);
}
