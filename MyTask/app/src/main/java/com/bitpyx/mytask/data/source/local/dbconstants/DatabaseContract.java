package com.bitpyx.mytask.data.source.local.dbconstants;

import android.os.Environment;

import java.io.File;

/**
 * @author Amrit Yadav
 * @since 21-05-2017
 */
public class DatabaseContract {

    static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "MyTask.db";
    public static final String DB_LOCATION = Environment.getExternalStorageDirectory()
            + File.separator + "MyTaskDatabase";

    private static final String TEXT_TYPE = " TEXT";
    private static final String INTEGER_TYPE = " INTEGER";
    private static final String NOT_NULL = " NOT NULL";
    private static final String PRIMARY_KEY = " PRIMARY KEY";
    private static final String AUTOINCREMENT = "  AUTOINCREMENT";
    private static final String DEFAULT_TYPE = "  DEFAULT";
    private static final String COMMA_SEP = ",";

    public DatabaseContract() {
    }

    static final class TaskTable {
        static final String TABLE_NAME = "Task";
        static final String COLUMN_NAME_ID = "id";
        static final String COLUMN_NAME_TITLE = "title";
        static final String COLUMN_NAME_DESCRIPTION = "description";
        static final String COLUMN_NAME_CATEGORY_ID = "categoryId";
        static final String COLUMN_NAME_DUE_DATE = "dueDate";
        static final String COLUMN_NAME_REMINDER = "reminder";
        static final String COLUMN_NAME_PRIORITY = "priority";
        static final String COLUMN_NAME_REPEAT = "repeat";
        static final String COLUMN_NAME_REPEAT_FREQUENCY = "repeatFrequency";
        static final String COLUMN_NAME_ASSIGNEE_TABLE_ID = "assignedTableId";
        static final String COLUMN_NAME_STATUS = "status";
        static final String COLUMN_NAME_CREATED_ON = "createdOn";
        static final String CREATE_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + "( "
                + COLUMN_NAME_ID + INTEGER_TYPE + PRIMARY_KEY + AUTOINCREMENT + COMMA_SEP
                + COLUMN_NAME_TITLE + TEXT_TYPE + NOT_NULL + COMMA_SEP
                + COLUMN_NAME_DESCRIPTION + TEXT_TYPE + COMMA_SEP
                + COLUMN_NAME_CATEGORY_ID + INTEGER_TYPE + NOT_NULL + COMMA_SEP
                + COLUMN_NAME_DUE_DATE + TEXT_TYPE + NOT_NULL + COMMA_SEP
                + COLUMN_NAME_REMINDER + TEXT_TYPE + COMMA_SEP
                + COLUMN_NAME_PRIORITY + TEXT_TYPE + COMMA_SEP
                + COLUMN_NAME_REPEAT + TEXT_TYPE + COMMA_SEP
                + COLUMN_NAME_REPEAT_FREQUENCY + INTEGER_TYPE + COMMA_SEP
                + COLUMN_NAME_ASSIGNEE_TABLE_ID + INTEGER_TYPE + COMMA_SEP
                + COLUMN_NAME_STATUS + TEXT_TYPE + COMMA_SEP
                + COLUMN_NAME_CREATED_ON + TEXT_TYPE + ")";
    }

    static final class TaskCategoryTable {
        static final String TABLE_NAME = "TaskCategory";
        static final String COLUMN_NAME_ID = "id";
        static final String COLUMN_NAME_TYPE = "taskType";
        static final String CREATE_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + "( "
                + COLUMN_NAME_ID + INTEGER_TYPE + PRIMARY_KEY + AUTOINCREMENT + COMMA_SEP
                + COLUMN_NAME_TYPE + TEXT_TYPE + ")";
    }

    static final class TaskAssigneeTable {
        static final String TABLE_NAME = "TaskAssigneeDetails";
        static final String COLUMN_NAME_ID = "id";
        static final String COLUMN_NAME_PEOPLE_APP_CONTACT_ID = "contactId";
        static final String COLUMN_NAME_ASSINEE_NAME = "assigneeName";
        static final String CREATE_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + "( "
                + COLUMN_NAME_ID + INTEGER_TYPE + PRIMARY_KEY + AUTOINCREMENT + COMMA_SEP
                + COLUMN_NAME_PEOPLE_APP_CONTACT_ID + INTEGER_TYPE + COMMA_SEP
                + COLUMN_NAME_ASSINEE_NAME + TEXT_TYPE + ")";
    }

}
