package com.bitpyx.mytask.utility;

import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;

import com.bitpyx.mytask.adapter.TaskSearchViewAdapter;

/**
 * @author Amrit Yadav
 * @since 7/26/2017.
 */
public class RecyclerItemTouchHelperCallback extends ItemTouchHelper.Callback {

    private final TaskSearchViewAdapter mAdapter;
    private boolean isDragIconPressed;

    public RecyclerItemTouchHelperCallback(TaskSearchViewAdapter mAdapter
            , boolean isDragIconPressed) {
        this.mAdapter = mAdapter;
        this.isDragIconPressed = isDragIconPressed;
    }

    @Override
    public int getMovementFlags(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
        return viewHolder.getItemViewType() == 0 ? 0
                : makeMovementFlags(ItemTouchHelper.UP | ItemTouchHelper.DOWN, 0);
    }

    @Override
    public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder
            , RecyclerView.ViewHolder target) {
        if(target.getAdapterPosition()==0) {
            mAdapter.onItemMove(viewHolder.getAdapterPosition(), target.getAdapterPosition() + 1);
            return true;
        }
        mAdapter.onItemMove(viewHolder.getAdapterPosition(), target.getAdapterPosition());
        return true;
    }

    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {

    }

    @Override
    public boolean isLongPressDragEnabled() {
        return isDragIconPressed;
    }

    @Override
    public void onSelectedChanged(RecyclerView.ViewHolder viewHolder, int actionState) {
        if (actionState != ItemTouchHelper.ACTION_STATE_IDLE) {
            ItemTouchHelperViewHolder itemViewHolder = (ItemTouchHelperViewHolder) viewHolder;
            itemViewHolder.onItemSelected();
        }

        super.onSelectedChanged(viewHolder, actionState);
    }

    @Override
    public void clearView(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
        super.clearView(recyclerView, viewHolder);
        ItemTouchHelperViewHolder itemViewHolder = (ItemTouchHelperViewHolder) viewHolder;
        itemViewHolder.onItemClear();
    }

}
