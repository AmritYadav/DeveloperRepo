package com.bitpyx.mytask.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.MotionEventCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.bitpyx.mytask.R;
import com.bitpyx.mytask.allTask.presenters.IAllTaskPresenter;
import com.bitpyx.mytask.data.model.DropDownContent;
import com.bitpyx.mytask.data.model.Task;
import com.bitpyx.mytask.utility.Common;
import com.bitpyx.mytask.utility.ItemTouchHelperAdapter;
import com.bitpyx.mytask.utility.ItemTouchHelperViewHolder;

import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class TaskSearchViewAdapter extends RecyclerView.Adapter<TaskSearchViewAdapter.ViewHolder>
        implements ItemTouchHelperAdapter {

    public interface OnStartDragListener {
        void onStartDrag(RecyclerView.ViewHolder viewHolder);
    }

    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;

    private List<Task> mTaskList;
    private List<Task> itemsPendingRemoval;
    private boolean toggleMultiSelect=false;
    private List<DropDownContent> mCategoryList;
    private Context mContext;
    private boolean searchViewOpen;
    private OnStartDragListener mOnStartDragListener;
    private IAllTaskPresenter mAllTaskPresenter;

    private int colorAccent, colorPriorityNormal, colorPriorityMedium, colorPriorityHigh;

    public TaskSearchViewAdapter(Context context, List<Task> taskList
            , List<DropDownContent> categoryList, boolean searchViewOpen
            , OnStartDragListener onStartDragListener, IAllTaskPresenter allTaskPresenter) {

        mContext = context;
        mTaskList = taskList;
        mCategoryList = categoryList;
        this.searchViewOpen = searchViewOpen;

        mOnStartDragListener = onStartDragListener;
        mAllTaskPresenter = allTaskPresenter;

        itemsPendingRemoval = new ArrayList<>();

        colorAccent = ContextCompat
                .getColor(mContext, R.color.colorAccent);
        colorPriorityNormal = ContextCompat
                .getColor(mContext, R.color.priorityNormal);
        colorPriorityMedium = ContextCompat
                .getColor(mContext, R.color.priorityMedium);
        colorPriorityHigh = ContextCompat
                .getColor(mContext, R.color.priorityHigh);

    }

    @Override
    public TaskSearchViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        switch (viewType) {
            case TYPE_HEADER:
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.row_task_today_header, parent, false);
                return new TaskSearchViewAdapter.ViewHolder(view, viewType);
            case TYPE_ITEM:
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.layout_row_task_search, parent, false);
                return new TaskSearchViewAdapter.ViewHolder(view, viewType);
        }
        return null;
    }

    @Override
    public int getItemViewType(int position) {
        return mTaskList.get(position).getViewType();
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {

        Task task = mTaskList.get(position);

        switch (task.getViewType()) {
            case TYPE_HEADER:
                holder.textViewHeaderTitle.setText(task.getTitle());
                break;
            case TYPE_ITEM:

                holder.mTextViewTaskTitle.setText(task.getTitle());

                String categoryName = Common.getCategoryName(mCategoryList, task.getCategoryId());

                String dueDate = Common.getFormattedDate(task.getDate());
                holder.textViewTaskDueDate.setText(dueDate);

                LocalDate localDateDue = LocalDateTime.parse(task.getDate()
                        , Common.defaultDateTimeFormatter).toLocalDate();
                LocalDate now = LocalDate.now();

                if (localDateDue.isBefore(now)) {
                    holder.textViewTaskDueDate.setTextColor(colorPriorityHigh);
                } else if (localDateDue.equals(now)) {
                    holder.textViewTaskDueDate.setTextColor(colorAccent);
                }

                if (!task.getRepeat().equals("One Time")) {
                    holder.imageViewTaskRepeating.setVisibility(View.VISIBLE);
                } else {
                    holder.imageViewTaskRepeating.setVisibility(View.GONE);
                }

                String priority = task.getPriority();
                switch (priority) {
                    case "Normal":
                        holder.mImageViewTaskPriority
                                .getDrawable()
                                .setColorFilter(colorPriorityNormal, PorterDuff.Mode.SRC_IN);
                        break;
                    case "Medium":
                        holder.mImageViewTaskPriority
                                .getDrawable()
                                .setColorFilter(colorPriorityMedium, PorterDuff.Mode.SRC_IN);
                        break;
                    case "High":
                        holder.mImageViewTaskPriority
                                .getDrawable()
                                .setColorFilter(colorPriorityHigh, PorterDuff.Mode.SRC_IN);
                        break;
                }


                if (searchViewOpen) {
                    holder.mTextViewTaskCategory.setText(categoryName);
                    holder.mImageButtonTaskDrag.setVisibility(View.GONE);
                } else {
                    holder.mImageButtonTaskDrag.setVisibility(View.VISIBLE);
                    holder.mTextViewTaskCategory.setVisibility(View.GONE);
                }

                if(toggleMultiSelect) {
                    holder.mImageButtonTaskDrag.setVisibility(View.GONE);
                }

                holder.mImageButtonTaskDrag.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        if (MotionEventCompat.getActionMasked(event) == MotionEvent.ACTION_DOWN) {
                            mOnStartDragListener.onStartDrag(holder);
                        }
                        return false;
                    }
                });

                if (task.isSelected()) {
                    holder.itemView.setBackgroundColor(ContextCompat.getColor(mContext
                            , R.color.listSelectedColor));
                } else {
                    holder.itemView.setBackgroundColor(0);
                }

                break;
        }
    }

    @Override
    public int getItemCount() {
        return mTaskList.size();
    }

    public void pendingRemoval(int position) {

        final Task task = mTaskList.get(position);
        if (!itemsPendingRemoval.contains(task)) {
            itemsPendingRemoval.add(task);
            notifyItemChanged(position);
            remove(mTaskList.indexOf(task));
        }
    }

    private void remove(int position) {
        Task task = mTaskList.get(position);
        if (itemsPendingRemoval.contains(task)) {
            itemsPendingRemoval.remove(task);
        }
        if (mTaskList.contains(task)) {
            mTaskList.remove(position);
            notifyItemRemoved(position);
        }
    }

    public boolean isPendingRemoval(int position) {
        Task task = mTaskList.get(position);
        return itemsPendingRemoval.contains(task);
    }

    @Override
    public void onItemMove(int fromPosition, int toPosition) {
        Collections.swap(mTaskList, fromPosition, toPosition);
        notifyItemMoved(fromPosition, toPosition);
        Task task = mTaskList.get(toPosition);
        for (int i = toPosition; i >= 0; i--) {
            Task task1 = mTaskList.get(i);
            if (task1.getViewType() == TYPE_HEADER) {
                task.setCategoryId(task1.getCategoryId());
                mAllTaskPresenter.changeTaskCategory(task);
                break;
            }
        }
    }

    @Override
    public void onItemDismiss(int position) {

    }

    class ViewHolder extends RecyclerView.ViewHolder implements ItemTouchHelperViewHolder {
        private TextView mTextViewTaskTitle, mTextViewTaskCategory, textViewHeaderTitle, textViewTaskDueDate;
        private ImageView mImageViewTaskPriority, imageViewTaskRepeating;
        private ImageButton mImageButtonTaskDrag;

        ViewHolder(View view, int viewType) {
            super(view);

            switch (viewType) {
                case TYPE_HEADER:
                    textViewHeaderTitle = (TextView) view.findViewById(R.id.text_header_title);
                    break;
                case TYPE_ITEM:
                    mImageViewTaskPriority = (ImageView) view.findViewById(R.id.iv_task_priority);
                    imageViewTaskRepeating = (ImageView) view.findViewById(R.id.iv_task_repeat);
                    mTextViewTaskTitle = (TextView) view.findViewById(R.id.tv_task_title);
                    textViewTaskDueDate = (TextView) view.findViewById(R.id.tv_task_due_date);
                    mTextViewTaskCategory = (TextView) view.findViewById(R.id.tv_task_category);
                    mImageButtonTaskDrag = (ImageButton) view.findViewById(R.id.ib_task_drag);
                    break;
            }
        }

        @Override
        public void onItemSelected() {
            itemView.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorAccent));
            mTextViewTaskTitle.setTextColor(ContextCompat.getColor(mContext, R.color.white));
            mImageButtonTaskDrag
                    .getDrawable()
                    .setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_IN);
        }

        @Override
        public void onItemClear() {
            itemView.setBackgroundColor(0);
            mTextViewTaskTitle.setTextColor(Color.BLACK);
            mImageButtonTaskDrag
                    .getDrawable()
                    .setColorFilter(Color.BLACK, PorterDuff.Mode.SRC_IN);
        }
    }

    public void refreshAdapter(List<Task> taskList, boolean toggleMultiSelect) {
        this.toggleMultiSelect=toggleMultiSelect;
        mTaskList = taskList;
        notifyDataSetChanged();
    }

}
