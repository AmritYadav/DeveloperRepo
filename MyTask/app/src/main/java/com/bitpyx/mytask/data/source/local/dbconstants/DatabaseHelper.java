package com.bitpyx.mytask.data.source.local.dbconstants;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.bitpyx.mytask.R;
import com.bitpyx.mytask.data.model.Assignee;
import com.bitpyx.mytask.data.model.DropDownContent;
import com.bitpyx.mytask.data.model.Task;
import com.bitpyx.mytask.data.source.local.dbconstants.DatabaseContract.TaskAssigneeTable;
import com.bitpyx.mytask.utility.Common;

import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static com.bitpyx.mytask.data.source.local.dbconstants.DatabaseContract.DATABASE_NAME;
import static com.bitpyx.mytask.data.source.local.dbconstants.DatabaseContract.DATABASE_VERSION;
import static com.bitpyx.mytask.data.source.local.dbconstants.DatabaseContract.DB_LOCATION;
import static com.bitpyx.mytask.data.source.local.dbconstants.DatabaseContract.TaskCategoryTable;
import static com.bitpyx.mytask.data.source.local.dbconstants.DatabaseContract.TaskTable;

/**
 * @author Amrit Yadav
 * @since 5/29/2017
 */
public class DatabaseHelper extends SQLiteOpenHelper {

    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;
    private Context mContext;

    public DatabaseHelper(Context context) {
        super(context
                , DB_LOCATION + File.separator
                        + DATABASE_NAME, null
                , DATABASE_VERSION);
        mContext = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(TaskTable.CREATE_TABLE);
        db.execSQL(TaskCategoryTable.CREATE_TABLE);
        db.execSQL(TaskAssigneeTable.CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public Long addTaskCategory(String taskCategory) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put(TaskCategoryTable.COLUMN_NAME_TYPE, taskCategory);

        return db.insert(TaskCategoryTable.TABLE_NAME, null, contentValues);
    }

    public long addNewTask(Task task) {

        SQLiteDatabase db = this.getWritableDatabase();

        Assignee assignee = task.getAssignee();

        ContentValues contentValues = new ContentValues();
        contentValues.put(TaskTable.COLUMN_NAME_TITLE, task.getTitle());
        contentValues.put(TaskTable.COLUMN_NAME_DESCRIPTION
                , task.getDescription());
        contentValues.put(TaskTable.COLUMN_NAME_CATEGORY_ID
                , task.getCategoryId());
        contentValues.put(TaskTable.COLUMN_NAME_DUE_DATE
                , task.getDate());
        contentValues.put(TaskTable.COLUMN_NAME_REMINDER
                , task.getReminder());
        contentValues.put(TaskTable.COLUMN_NAME_PRIORITY, task.getPriority());
        contentValues.put(TaskTable.COLUMN_NAME_REPEAT, task.getRepeat());
        contentValues.put(TaskTable.COLUMN_NAME_REPEAT_FREQUENCY, task.getRepeatFrequency());
        contentValues.put(TaskTable.COLUMN_NAME_STATUS, task.getStatus());
        contentValues.put(TaskTable.COLUMN_NAME_CREATED_ON
                , task.getTaskCreationDate());

        if (assignee != null) {
            int assigneeId = checkAssigneeDetails(assignee.getContactId());
            if (assigneeId > 0) {
                contentValues.put(TaskTable.COLUMN_NAME_ASSIGNEE_TABLE_ID
                        , assigneeId);
            } else {
                if (assignee.getAssigneeName() != null && !assignee.getAssigneeName().isEmpty()) {
                    long assigneeTableRowId = addAssignee(assignee);
                    contentValues.put(TaskTable.COLUMN_NAME_ASSIGNEE_TABLE_ID
                            , assigneeTableRowId);
                }
            }
        } else {
            contentValues.put(TaskTable.COLUMN_NAME_ASSIGNEE_TABLE_ID
                    , 0);
        }
        return db.insert(TaskTable.TABLE_NAME, null, contentValues);

    }

    private long addAssignee(Assignee assignee) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put(TaskAssigneeTable.COLUMN_NAME_PEOPLE_APP_CONTACT_ID
                , assignee.getContactId());
        contentValues.put(TaskAssigneeTable.COLUMN_NAME_ASSINEE_NAME
                , assignee.getAssigneeName());

        return db.insert(TaskAssigneeTable.TABLE_NAME, null, contentValues);
    }

    public long updateCategory(String category, int categoryId) {

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put(TaskCategoryTable.COLUMN_NAME_TYPE, category);

        return db.update(TaskCategoryTable.TABLE_NAME
                , contentValues, TaskCategoryTable.COLUMN_NAME_ID + "=?"
                , new String[]{String.valueOf(categoryId)});
    }

    public long updateTask(Task task) {

        SQLiteDatabase db = this.getWritableDatabase();

        Assignee assignee = task.getAssignee();

        ContentValues contentValues = new ContentValues();
        contentValues.put(TaskTable.COLUMN_NAME_TITLE, task.getTitle());
        contentValues.put(TaskTable.COLUMN_NAME_DESCRIPTION
                , task.getDescription());
        contentValues.put(TaskTable.COLUMN_NAME_CATEGORY_ID, task.getCategoryId());
        contentValues.put(TaskTable.COLUMN_NAME_DUE_DATE, task.getDate());
        contentValues.put(TaskTable.COLUMN_NAME_REMINDER, task.getReminder());
        contentValues.put(TaskTable.COLUMN_NAME_PRIORITY, task.getPriority());
        contentValues.put(TaskTable.COLUMN_NAME_REPEAT, task.getRepeat());
        contentValues.put(TaskTable.COLUMN_NAME_REPEAT_FREQUENCY, task.getRepeatFrequency());
        contentValues.put(TaskTable.COLUMN_NAME_STATUS, task.getStatus());
        contentValues.put(TaskTable.COLUMN_NAME_CREATED_ON
                , task.getTaskCreationDate());

        int assigneeId = checkAssigneeDetails(assignee.getContactId());
        if (assigneeId > 0) {
            contentValues.put(TaskTable.COLUMN_NAME_ASSIGNEE_TABLE_ID
                    , assigneeId);
        } else {
            if (assignee.getAssigneeName() != null && !assignee.getAssigneeName().isEmpty()) {
                long assigneeTableRowId = addAssignee(assignee);
                contentValues.put(TaskTable.COLUMN_NAME_ASSIGNEE_TABLE_ID
                        , assigneeTableRowId);
            }
        }

        return db.update(TaskTable.TABLE_NAME, contentValues
                , TaskTable.COLUMN_NAME_ID + "=?"
                , new String[]{String.valueOf(task.getTaskId())});

    }

    private int checkAssigneeDetails(int contactId) {
        SQLiteDatabase db = this.getWritableDatabase();

        String query = "SELECT * FROM " + TaskAssigneeTable.TABLE_NAME
                + " WHERE " + TaskAssigneeTable.COLUMN_NAME_PEOPLE_APP_CONTACT_ID
                + "=?";
        Cursor cursor = db.rawQuery(query, new String[]{String.valueOf(contactId)});
        if (cursor.moveToNext())
            return cursor.getInt(cursor.getColumnIndex(
                    TaskAssigneeTable.COLUMN_NAME_ID));
        cursor.close();
        return 0;
    }

    public boolean isDefaultTaskCategoryAvailable() {
        SQLiteDatabase db = this.getReadableDatabase();

        String query = "SELECT * FROM " + TaskCategoryTable.TABLE_NAME;

        return db.rawQuery(query, null).getCount() > 0;
    }

    public List<DropDownContent> getCategoryList() {

        SQLiteDatabase db = this.getReadableDatabase();

        String query = "SELECT * FROM " + TaskCategoryTable.TABLE_NAME;

        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToNext()) {
            List<DropDownContent> categoryList = new ArrayList<>();
            do {
                int categoryId = cursor.getInt(cursor
                        .getColumnIndex(TaskCategoryTable.COLUMN_NAME_ID));
                String categoryName = cursor.getString(cursor
                        .getColumnIndex(TaskCategoryTable.COLUMN_NAME_TYPE));

                int taskCountInCategory = getPendingTaskCountForCategory(categoryId);

                DropDownContent dropDownContent = new DropDownContent();
                dropDownContent.setName(categoryName);
                dropDownContent.setCategoryId(categoryId);
                dropDownContent.setTaskCount(taskCountInCategory);
                dropDownContent.setType("C");

                categoryList.add(dropDownContent);
            } while (cursor.moveToNext());
            return categoryList;
        }
        cursor.close();
        return Collections.emptyList();
    }

    private Assignee getAssigneeDetails(int assigneeTableRowId) {
        Assignee assignee = new Assignee();
        SQLiteDatabase db = this.getReadableDatabase();

        String query = "SELECT * FROM " + TaskAssigneeTable.TABLE_NAME
                + " WHERE " + TaskAssigneeTable.COLUMN_NAME_ID + "=?";

        Cursor cursor = db.rawQuery(query, new String[]{String.valueOf(assigneeTableRowId)});
        if (cursor.moveToFirst()) {
            assignee.setRowId(assigneeTableRowId);
            assignee.setContactId(cursor.getInt(cursor.getColumnIndex(
                    TaskAssigneeTable.COLUMN_NAME_PEOPLE_APP_CONTACT_ID)));
            assignee.setAssigneeName(cursor.getString(cursor.getColumnIndex(
                    TaskAssigneeTable.COLUMN_NAME_ASSINEE_NAME)));
            return assignee;
        } else {
            assignee.setRowId(assigneeTableRowId);
        }
        cursor.close();
        return assignee;
    }

    private int getPendingTaskCountForCategory(int categoryId) {
        int taskCount = 0;
        SQLiteDatabase db = this.getReadableDatabase();

        String query = "SELECT * FROM " + TaskTable.TABLE_NAME
                + " WHERE " + TaskTable.COLUMN_NAME_CATEGORY_ID + "=?"
                + " AND " + TaskTable.COLUMN_NAME_STATUS + " !=?";

        Cursor cursor = db.rawQuery(query, new String[]{String.valueOf(categoryId), "Complete"});
        if (cursor.moveToNext()) {
            do {
                String dueDate = cursor.getString(cursor.getColumnIndex(
                        TaskTable.COLUMN_NAME_DUE_DATE));

                LocalDate localDateDue = LocalDateTime
                        .parse(dueDate, Common.defaultDateTimeFormatter).toLocalDate();
                LocalDate localDateNow = LocalDate.now();
                if (localDateDue.isBefore(localDateNow) || localDateDue.equals(localDateNow)) {
                    taskCount++;
                }
            } while (cursor.moveToNext());
        }
        return taskCount;
    }

    public int getTodayTaskCount() {
        int taskCount = 0;
        SQLiteDatabase db = this.getReadableDatabase();

        String query = "SELECT * FROM " + TaskTable.TABLE_NAME
                + " WHERE " + TaskTable.COLUMN_NAME_STATUS + " != ?";

        Cursor cursor = db.rawQuery(query, new String[]{"Complete"});
        if (cursor.moveToNext()) {
            do {
                String dueDate = cursor.getString(cursor.getColumnIndex(
                        TaskTable.COLUMN_NAME_DUE_DATE));

                LocalDate localDateDue = LocalDateTime
                        .parse(dueDate, Common.defaultDateTimeFormatter).toLocalDate();
                LocalDate localDateNow = LocalDate.now();
                if (localDateDue.isBefore(localDateNow) || localDateDue.equals(localDateNow)) {
                    taskCount++;
                }
            } while (cursor.moveToNext());
        }
        return taskCount;
    }

    public int getTaskCount(int categoryId) {
        SQLiteDatabase db = this.getReadableDatabase();

        String query = "SELECT * FROM " + TaskTable.TABLE_NAME
                + " WHERE " + TaskTable.COLUMN_NAME_CATEGORY_ID + " = ?";
        return db.rawQuery(query, new String[]{String.valueOf(categoryId)}).getCount();
    }

    public void deleteTask(int taskId) {

        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TaskTable.TABLE_NAME
                , TaskTable.COLUMN_NAME_ID + "=?"
                , new String[]{String.valueOf(taskId)});
    }

    public List<Task> getOverdueTaskList() {

        List<Task> taskList;

        SQLiteDatabase db = this.getReadableDatabase();

        String query = "SELECT * FROM " + TaskTable.TABLE_NAME
                + " WHERE " + TaskTable.COLUMN_NAME_STATUS + " != ?"
                + " ORDER BY " + TaskTable.COLUMN_NAME_PRIORITY + " ASC";

        Cursor cursor = db.rawQuery(query, new String[]{"Complete"});

        if (cursor.moveToNext()) {

            taskList = new ArrayList<>();

            Task taskHeader = new Task();
            taskHeader.setTitle("Overdue");
            taskHeader.setTaskHeaderTitleColor(R.color.priorityHigh);
            taskHeader.setViewType(TYPE_HEADER);
            taskList.add(taskHeader);

            do {

                String dueDate = cursor.getString(cursor.getColumnIndex(
                        TaskTable.COLUMN_NAME_DUE_DATE));

                LocalDate localDateDue = LocalDateTime
                        .parse(dueDate, Common.defaultDateTimeFormatter).toLocalDate();
                LocalDate localDateNow = LocalDate.now();

                if (localDateDue.isBefore(localDateNow)) {
                    Task task = new Task();
                    task.setTaskId(cursor.getInt(cursor.getColumnIndex(
                            TaskTable.COLUMN_NAME_ID)));
                    task.setTitle(cursor.getString(cursor.getColumnIndex(
                            TaskTable.COLUMN_NAME_TITLE)));
                    task.setDescription(cursor.getString(cursor.getColumnIndex(
                            TaskTable.COLUMN_NAME_DESCRIPTION)));
                    task.setCategoryId(cursor.getInt(cursor.getColumnIndex(
                            TaskTable.COLUMN_NAME_CATEGORY_ID)));
                    task.setDate(dueDate);
                    task.setReminder(cursor.getString(cursor.getColumnIndex(
                            TaskTable.COLUMN_NAME_REMINDER)));
                    task.setPriority(cursor.getString(cursor.getColumnIndex(
                            TaskTable.COLUMN_NAME_PRIORITY)));
                    task.setRepeat(cursor.getString(cursor.getColumnIndex(
                            TaskTable.COLUMN_NAME_REPEAT)));
                    task.setRepeatFrequency(cursor.getInt(cursor.getColumnIndex(
                            TaskTable.COLUMN_NAME_REPEAT_FREQUENCY)));
                    //Fetch Assignee details
                    Assignee assignee = getAssigneeDetails(cursor.getInt(cursor.getColumnIndex(
                            TaskTable.COLUMN_NAME_ASSIGNEE_TABLE_ID)));
                    task.setAssignee(assignee);

                    task.setStatus(cursor.getString(cursor.getColumnIndex(
                            TaskTable.COLUMN_NAME_STATUS)));
                    task.setTaskCreationDate(cursor.getString(cursor.getColumnIndex(
                            TaskTable.COLUMN_NAME_CREATED_ON)));
                    task.setViewType(TYPE_ITEM);
                    taskList.add(task);
                }

            } while (cursor.moveToNext());

            if (taskList.size() > 1)
                return taskList;
        }
        cursor.close();

        return Collections.emptyList();
    }

    public List<Task> getTodayTaskList() {

        List<Task> taskList;

        SQLiteDatabase db = this.getReadableDatabase();

        String query = "SELECT * FROM " + TaskTable.TABLE_NAME
                + " WHERE " + TaskTable.COLUMN_NAME_STATUS + " != ?"
                + " ORDER BY " + TaskTable.COLUMN_NAME_PRIORITY + " ASC";

        Cursor cursor = db.rawQuery(query, new String[]{"Complete"});

        if (cursor.moveToNext()) {

            taskList = new ArrayList<>();

            Task taskHeader = new Task();
            taskHeader.setTitle("Today");
            taskHeader.setViewType(TYPE_HEADER);
            taskHeader.setTaskHeaderTitleColor(R.color.colorAccent);
            taskList.add(taskHeader);

            do {

                String dueDate = cursor.getString(cursor.getColumnIndex(
                        TaskTable.COLUMN_NAME_DUE_DATE));

                LocalDate localDateDue = LocalDateTime
                        .parse(dueDate, Common.defaultDateTimeFormatter).toLocalDate();
                LocalDate localDateNow = LocalDate.now();

                if (localDateDue.equals(localDateNow)) {
                    Task task = new Task();
                    task.setTaskId(cursor.getInt(cursor.getColumnIndex(
                            TaskTable.COLUMN_NAME_ID)));
                    task.setTitle(cursor.getString(cursor.getColumnIndex(
                            TaskTable.COLUMN_NAME_TITLE)));
                    task.setDescription(cursor.getString(cursor.getColumnIndex(
                            TaskTable.COLUMN_NAME_DESCRIPTION)));
                    task.setCategoryId(cursor.getInt(cursor.getColumnIndex(
                            TaskTable.COLUMN_NAME_CATEGORY_ID)));
                    task.setDate(dueDate);
                    task.setReminder(cursor.getString(cursor.getColumnIndex(
                            TaskTable.COLUMN_NAME_REMINDER)));
                    task.setPriority(cursor.getString(cursor.getColumnIndex(
                            TaskTable.COLUMN_NAME_PRIORITY)));
                    task.setRepeat(cursor.getString(cursor.getColumnIndex(
                            TaskTable.COLUMN_NAME_REPEAT)));
                    task.setRepeatFrequency(cursor.getInt(cursor.getColumnIndex(
                            TaskTable.COLUMN_NAME_REPEAT_FREQUENCY)));
                    //Fetch Assignee details
                    Assignee assignee = getAssigneeDetails(cursor.getInt(cursor.getColumnIndex(
                            TaskTable.COLUMN_NAME_ASSIGNEE_TABLE_ID)));
                    task.setAssignee(assignee);

                    task.setStatus(cursor.getString(cursor.getColumnIndex(
                            TaskTable.COLUMN_NAME_STATUS)));
                    task.setTaskCreationDate(cursor.getString(cursor.getColumnIndex(
                            TaskTable.COLUMN_NAME_CREATED_ON)));
                    task.setViewType(TYPE_ITEM);
                    taskList.add(task);
                }

            } while (cursor.moveToNext());

            if (taskList.size() > 1)
                return taskList;
        }
        cursor.close();
        return Collections.emptyList();
    }

    public List<Task> getTaskCompleteList() {
        List<Task> taskList;

        SQLiteDatabase db = this.getReadableDatabase();

        String query = "SELECT * FROM " + TaskTable.TABLE_NAME
                + " WHERE " + TaskTable.COLUMN_NAME_STATUS + " = ?"
                + " ORDER BY " + TaskTable.COLUMN_NAME_CREATED_ON + " DESC";

        Cursor cursor = db.rawQuery(query, new String[]{"Complete"});

        if (cursor.moveToNext()) {

            taskList = new ArrayList<>();

            do {

                Task task = new Task();
                task.setTaskId(cursor.getInt(cursor.getColumnIndex(
                        TaskTable.COLUMN_NAME_ID)));
                task.setTitle(cursor.getString(cursor.getColumnIndex(
                        TaskTable.COLUMN_NAME_TITLE)));
                task.setDescription(cursor.getString(cursor.getColumnIndex(
                        TaskTable.COLUMN_NAME_DESCRIPTION)));
                task.setCategoryId(cursor.getInt(cursor.getColumnIndex(
                        TaskTable.COLUMN_NAME_CATEGORY_ID)));
                task.setDate(cursor.getString(cursor.getColumnIndex(
                        TaskTable.COLUMN_NAME_DUE_DATE)));
                task.setReminder(cursor.getString(cursor.getColumnIndex(
                        TaskTable.COLUMN_NAME_REMINDER)));
                task.setPriority(cursor.getString(cursor.getColumnIndex(
                        TaskTable.COLUMN_NAME_PRIORITY)));
                task.setRepeat(cursor.getString(cursor.getColumnIndex(
                        TaskTable.COLUMN_NAME_REPEAT)));
                task.setRepeatFrequency(cursor.getInt(cursor.getColumnIndex(
                        TaskTable.COLUMN_NAME_REPEAT_FREQUENCY)));
                //Fetch Assignee details
                Assignee assignee = getAssigneeDetails(cursor.getInt(cursor.getColumnIndex(
                        TaskTable.COLUMN_NAME_ASSIGNEE_TABLE_ID)));
                task.setAssignee(assignee);

                task.setStatus(cursor.getString(cursor.getColumnIndex(
                        TaskTable.COLUMN_NAME_STATUS)));
                task.setTaskCreationDate(cursor.getString(cursor.getColumnIndex(
                        TaskTable.COLUMN_NAME_CREATED_ON)));
                task.setViewType(TYPE_ITEM);
                taskList.add(task);
            } while (cursor.moveToNext());
            return taskList;
        }
        cursor.close();
        return Collections.emptyList();
    }

    public List<Task> getAllCategorizedTask() {
        List<Task> taskList = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        String query = "SELECT * FROM " + TaskTable.TABLE_NAME
                + " WHERE " + TaskTable.COLUMN_NAME_STATUS + " != ?"
                + " AND " + TaskTable.COLUMN_NAME_CATEGORY_ID + "=?"
                + " ORDER BY " + TaskTable.COLUMN_NAME_PRIORITY + " ASC";

        List<DropDownContent> categoryList = getCategoryList();

        for (DropDownContent dropDownContent : categoryList) {

            Task taskHeader = new Task();
            taskHeader.setTitle(dropDownContent.getName());
            taskHeader.setViewType(TYPE_HEADER);
            taskHeader.setCategoryId(dropDownContent.getCategoryId());
            taskList.add(taskHeader);

            Cursor cursor = db.rawQuery(query, new String[]{"Complete"
                    , String.valueOf(dropDownContent.getCategoryId())});
            if (cursor.moveToNext()) {
                do {
                    Task task = new Task();
                    task.setTaskId(cursor.getInt(cursor.getColumnIndex(
                            TaskTable.COLUMN_NAME_ID)));
                    task.setTitle(cursor.getString(cursor.getColumnIndex(
                            TaskTable.COLUMN_NAME_TITLE)));
                    task.setDescription(cursor.getString(cursor.getColumnIndex(
                            TaskTable.COLUMN_NAME_DESCRIPTION)));
                    task.setCategoryId(cursor.getInt(cursor.getColumnIndex(
                            TaskTable.COLUMN_NAME_CATEGORY_ID)));
                    task.setDate(cursor.getString(cursor.getColumnIndex(
                            TaskTable.COLUMN_NAME_DUE_DATE)));
                    task.setReminder(cursor.getString(cursor.getColumnIndex(
                            TaskTable.COLUMN_NAME_REMINDER)));
                    task.setPriority(cursor.getString(cursor.getColumnIndex(
                            TaskTable.COLUMN_NAME_PRIORITY)));
                    task.setRepeat(cursor.getString(cursor.getColumnIndex(
                            TaskTable.COLUMN_NAME_REPEAT)));
                    task.setRepeatFrequency(cursor.getInt(cursor.getColumnIndex(
                            TaskTable.COLUMN_NAME_REPEAT_FREQUENCY)));

                    //Fetch Assignee details
                    Assignee assignee = getAssigneeDetails(cursor.getInt(cursor.getColumnIndex(
                            TaskTable.COLUMN_NAME_ASSIGNEE_TABLE_ID)));
                    task.setAssignee(assignee);

                    task.setStatus(cursor.getString(cursor.getColumnIndex(
                            TaskTable.COLUMN_NAME_STATUS)));
                    task.setTaskCreationDate(cursor.getString(cursor.getColumnIndex(
                            TaskTable.COLUMN_NAME_CREATED_ON)));
                    task.setViewType(TYPE_ITEM);
                    taskList.add(task);
                } while (cursor.moveToNext());
            }
            cursor.close();
        }
        return taskList;
    }

    public String getCategoryName(int categoryId) {

        SQLiteDatabase db = this.getReadableDatabase();

        String query = "SELECT " + TaskCategoryTable.COLUMN_NAME_TYPE + " FROM "
                + TaskCategoryTable.TABLE_NAME + " WHERE "
                + TaskCategoryTable.COLUMN_NAME_ID + "=?";

        Cursor cursor = db.rawQuery(query, new String[]{String.valueOf(categoryId)});
        cursor.moveToNext();
        String categoryType = cursor.getString(0);
        cursor.close();
        return categoryType;
    }

    public List<Task> getFilteredTaskList(String filterQuery) {

        filterQuery = "%" + filterQuery + "%";
        SQLiteDatabase db = this.getReadableDatabase();

        String query = "SELECT * FROM " + TaskTable.TABLE_NAME
                + " WHERE " + TaskTable.COLUMN_NAME_STATUS + " !=? "
                + " AND " + TaskTable.COLUMN_NAME_TITLE + " LIKE ?"
                + " OR " + TaskTable.COLUMN_NAME_DESCRIPTION + " LIKE ?";

        Cursor cursor = db.rawQuery(query, new String[]{"Complete", filterQuery, filterQuery});
        if (cursor.moveToNext()) {
            List<Task> taskList = new ArrayList<>();
            do {
                Task task = new Task();
                task.setTaskId(cursor.getInt(cursor
                        .getColumnIndex(TaskTable.COLUMN_NAME_ID)));
                task.setTitle(cursor.getString(cursor
                        .getColumnIndex(TaskTable.COLUMN_NAME_TITLE)));
                task.setDescription(cursor.getString(cursor
                        .getColumnIndex(TaskTable.COLUMN_NAME_DESCRIPTION)));
                task.setCategoryId(cursor.getInt(cursor
                        .getColumnIndex(TaskTable.COLUMN_NAME_CATEGORY_ID)));
                task.setDate(cursor.getString(cursor
                        .getColumnIndex(TaskTable.COLUMN_NAME_DUE_DATE)));
                task.setReminder(cursor.getString(cursor
                        .getColumnIndex(TaskTable.COLUMN_NAME_REMINDER)));
                task.setPriority(cursor.getString(cursor
                        .getColumnIndex(TaskTable.COLUMN_NAME_PRIORITY)));
                task.setRepeat(cursor.getString(cursor
                        .getColumnIndex(TaskTable.COLUMN_NAME_REPEAT)));
                task.setRepeatFrequency(cursor.getInt(cursor
                        .getColumnIndex(TaskTable.COLUMN_NAME_REPEAT_FREQUENCY)));
                //Fetch Assignee details
                Assignee assignee = getAssigneeDetails(cursor.getInt(cursor.getColumnIndex(
                        TaskTable.COLUMN_NAME_ASSIGNEE_TABLE_ID)));
                task.setAssignee(assignee);
                task.setStatus(cursor.getString(cursor
                        .getColumnIndex(TaskTable.COLUMN_NAME_STATUS)));
                task.setTaskCreationDate(cursor.getString(cursor
                        .getColumnIndex(TaskTable.COLUMN_NAME_CREATED_ON)));
                task.setViewType(TYPE_ITEM);
                taskList.add(task);
            } while (cursor.moveToNext());
            return taskList;
        }
        cursor.close();
        return Collections.emptyList();
    }

    public List<Task> getTaskListByCategory(int categoryId) {
        SQLiteDatabase db = this.getReadableDatabase();
        String query = "SELECT * " +
                " FROM " + TaskTable.TABLE_NAME +
                " WHERE " + TaskTable.COLUMN_NAME_CATEGORY_ID + "=?" +
                " AND " + TaskTable.COLUMN_NAME_STATUS + " !=?" +
                " ORDER BY " + TaskTable.COLUMN_NAME_PRIORITY + " ASC";

        Cursor cursor = db.rawQuery(query, new String[]{String.valueOf(categoryId), "Complete"});
        List<Task> taskList = new ArrayList<>();
        if (cursor.moveToNext()) {
            do {

                String dueDate = cursor.getString(cursor.getColumnIndex(
                        TaskTable.COLUMN_NAME_DUE_DATE));

                LocalDate localDateDue = LocalDateTime
                        .parse(dueDate, Common.defaultDateTimeFormatter).toLocalDate();
                LocalDate localDateNow = LocalDate.now();

                if (localDateDue.isBefore(localDateNow) || localDateDue.equals(localDateNow)) {
                    Task task = new Task();
                    task.setTaskId(cursor.getInt(cursor.getColumnIndex(
                            TaskTable.COLUMN_NAME_ID)));
                    task.setTitle(cursor.getString(cursor.getColumnIndex(
                            TaskTable.COLUMN_NAME_TITLE)));
                    task.setDescription(cursor.getString(cursor.getColumnIndex(
                            TaskTable.COLUMN_NAME_DESCRIPTION)));
                    task.setCategoryId(cursor.getInt(cursor.getColumnIndex(
                            TaskTable.COLUMN_NAME_CATEGORY_ID)));
                    task.setDate(cursor.getString(cursor.getColumnIndex(
                            TaskTable.COLUMN_NAME_DUE_DATE)));
                    task.setReminder(cursor.getString(cursor.getColumnIndex(
                            TaskTable.COLUMN_NAME_REMINDER)));
                    task.setPriority(cursor.getString(cursor.getColumnIndex(
                            TaskTable.COLUMN_NAME_PRIORITY)));
                    task.setRepeat(cursor.getString(cursor.getColumnIndex(
                            TaskTable.COLUMN_NAME_REPEAT)));
                    task.setRepeatFrequency(cursor.getInt(cursor.getColumnIndex(
                            TaskTable.COLUMN_NAME_REPEAT_FREQUENCY)));
                    //Fetch Assignee details
                    Assignee assignee = getAssigneeDetails(cursor.getInt(cursor.getColumnIndex(
                            TaskTable.COLUMN_NAME_ASSIGNEE_TABLE_ID)));
                    task.setAssignee(assignee);
                    task.setStatus(cursor.getString(cursor.getColumnIndex(
                            TaskTable.COLUMN_NAME_STATUS)));
                    task.setTaskCreationDate(cursor.getString(cursor.getColumnIndex(
                            TaskTable.COLUMN_NAME_CREATED_ON)));
                    task.setViewType(TYPE_ITEM);
                    taskList.add(task);
                }
            } while (cursor.moveToNext());
            return taskList;
        }
        cursor.close();
        return Collections.emptyList();
    }

    public boolean isCategoryExists(String categoryName) {
        SQLiteDatabase db = this.getReadableDatabase();

        String query = "SELECT * FROM " + TaskCategoryTable.TABLE_NAME
                + " WHERE " + TaskCategoryTable.COLUMN_NAME_TYPE + " LIKE ?";

        Cursor cursor = db.rawQuery(query, new String[]{categoryName});

        return cursor != null && cursor.getCount() > 0;
    }

    public void deleteCategory(DropDownContent categoryObj) {
        SQLiteDatabase db = this.getWritableDatabase();

        String categoryId = String.valueOf(categoryObj.getCategoryId());

        db.delete(TaskCategoryTable.TABLE_NAME
                , TaskCategoryTable.COLUMN_NAME_ID + "=?"
                , new String[]{categoryId});

        db.delete(TaskTable.TABLE_NAME
                , TaskTable.COLUMN_NAME_CATEGORY_ID + "=?"
                , new String[]{categoryId});

    }

    public List<Task> getTaskListByFilter(int filterId) {
        List<Task> taskList = new ArrayList<>();

        SQLiteDatabase db = this.getReadableDatabase();
        String filter = Common.getPriorityById(filterId);
        String query = "SELECT * " +
                " FROM " + TaskTable.TABLE_NAME +
                " WHERE " + TaskTable.COLUMN_NAME_PRIORITY + "=? " +
                " AND " + TaskTable.COLUMN_NAME_STATUS + " !=?";

        Cursor cursor = db.rawQuery(query, new String[]{filter, "Complete"});

        if (cursor.moveToNext()) {
            do {

                String dueDate = cursor.getString(cursor.getColumnIndex(
                        TaskTable.COLUMN_NAME_DUE_DATE));

                LocalDate localDateDue = LocalDateTime
                        .parse(dueDate, Common.defaultDateTimeFormatter).toLocalDate();
                LocalDate localDateNow = LocalDate.now();

                if (localDateDue.isBefore(localDateNow) || localDateDue.equals(localDateNow)) {

                    Task task = new Task();
                    task.setTaskId(cursor.getInt(cursor.getColumnIndex(
                            TaskTable.COLUMN_NAME_ID)));
                    task.setTitle(cursor.getString(cursor.getColumnIndex(
                            TaskTable.COLUMN_NAME_TITLE)));
                    task.setDescription(cursor.getString(cursor.getColumnIndex(
                            TaskTable.COLUMN_NAME_DESCRIPTION)));
                    task.setCategoryId(cursor.getInt(cursor.getColumnIndex(
                            TaskTable.COLUMN_NAME_CATEGORY_ID)));
                    task.setDate(cursor.getString(cursor.getColumnIndex(
                            TaskTable.COLUMN_NAME_DUE_DATE)));
                    task.setReminder(cursor.getString(cursor.getColumnIndex(
                            TaskTable.COLUMN_NAME_REMINDER)));
                    task.setPriority(cursor.getString(cursor.getColumnIndex(
                            TaskTable.COLUMN_NAME_PRIORITY)));
                    task.setRepeat(cursor.getString(cursor.getColumnIndex(
                            TaskTable.COLUMN_NAME_REPEAT)));
                    task.setRepeatFrequency(cursor.getInt(cursor.getColumnIndex(
                            TaskTable.COLUMN_NAME_REPEAT_FREQUENCY)));
                    //Fetch Assignee details
                    Assignee assignee = getAssigneeDetails(cursor.getInt(cursor.getColumnIndex(
                            TaskTable.COLUMN_NAME_ASSIGNEE_TABLE_ID)));
                    task.setAssignee(assignee);
                    task.setStatus(cursor.getString(cursor.getColumnIndex(
                            TaskTable.COLUMN_NAME_STATUS)));
                    task.setTaskCreationDate(cursor.getString(cursor.getColumnIndex(
                            TaskTable.COLUMN_NAME_CREATED_ON)));
                    task.setViewType(TYPE_ITEM);
                    taskList.add(task);
                }
            } while (cursor.moveToNext());
            return taskList;
        }
        cursor.close();
        return Collections.emptyList();
    }

    public List<Assignee> getAssigneeList() {
        SQLiteDatabase db = this.getReadableDatabase();
        String query = "SELECT DISTINCT a." + TaskAssigneeTable.COLUMN_NAME_ID + ", a."
                + TaskAssigneeTable.COLUMN_NAME_PEOPLE_APP_CONTACT_ID + ", a."
                + TaskAssigneeTable.COLUMN_NAME_ASSINEE_NAME
                + " FROM " + TaskAssigneeTable.TABLE_NAME + " a"
                + " JOIN " + TaskTable.TABLE_NAME + " b"
                + " ON a." + TaskAssigneeTable.COLUMN_NAME_ID
                + " = b." + TaskTable.COLUMN_NAME_ASSIGNEE_TABLE_ID
                + " WHERE b." + TaskTable.COLUMN_NAME_STATUS + "!=?"
                + " ORDER BY a." + TaskAssigneeTable.COLUMN_NAME_PEOPLE_APP_CONTACT_ID + " DESC";

        Cursor cursor = db.rawQuery(query, new String[]{"Complete"});
        if (cursor.moveToNext()) {
            List<Assignee> assigneeList = new ArrayList<>();
            do {
                if (cursor.getInt(1) > 0) {
                    Assignee assignee = new Assignee();
                    assignee.setRowId(cursor
                            .getInt(cursor.getColumnIndex(TaskAssigneeTable.COLUMN_NAME_ID)));
                    assignee.setContactId(cursor.getInt(cursor
                            .getColumnIndex(TaskAssigneeTable.COLUMN_NAME_PEOPLE_APP_CONTACT_ID)));
                    assignee.setAssigneeName(cursor.getString(cursor
                            .getColumnIndex(TaskAssigneeTable.COLUMN_NAME_ASSINEE_NAME)));
                    assigneeList.add(assignee);
                }
            } while (cursor.moveToNext());
            if (assigneeList.size() > 0) {
                Assignee assignee = new Assignee();
                assignee.setRowId(0);
                assignee.setContactId(0);
                assignee.setAssigneeName("All");
                assigneeList.add(assignee);
            }
            return assigneeList;
        }
        cursor.close();
        return Collections.emptyList();
    }

    public List<Task> getOverdueTaskList(int contactId) {

        List<Task> taskList;

        SQLiteDatabase db = this.getReadableDatabase();

        String query = "SELECT * FROM " + TaskTable.TABLE_NAME + " a"
                + " JOIN " + TaskAssigneeTable.TABLE_NAME + " b"
                + " ON a." + TaskTable.COLUMN_NAME_ASSIGNEE_TABLE_ID
                + " = b." + TaskAssigneeTable.COLUMN_NAME_ID
                + " WHERE " + TaskTable.COLUMN_NAME_STATUS + " != ?"
                + " AND " + TaskAssigneeTable.COLUMN_NAME_PEOPLE_APP_CONTACT_ID + "=?"
                + " ORDER BY " + TaskTable.COLUMN_NAME_PRIORITY + " ASC";

        Cursor cursor = db.rawQuery(query, new String[]{"Complete", String.valueOf(contactId)});

        if (cursor.moveToNext()) {

            taskList = new ArrayList<>();

            Task taskHeader = new Task();
            taskHeader.setTitle("Overdue");
            taskHeader.setTaskHeaderTitleColor(R.color.priorityHigh);
            taskHeader.setViewType(TYPE_HEADER);
            taskList.add(taskHeader);

            do {
                String dueDate = cursor.getString(cursor.getColumnIndex(
                        TaskTable.COLUMN_NAME_DUE_DATE));

                LocalDate localDateDue = LocalDateTime
                        .parse(dueDate, Common.defaultDateTimeFormatter).toLocalDate();
                LocalDate localDateNow = LocalDate.now();

                if (localDateDue.isBefore(localDateNow)) {
                    Task task = new Task();
                    task.setTaskId(cursor.getInt(0));
                    task.setTitle(cursor.getString(cursor.getColumnIndex(
                            TaskTable.COLUMN_NAME_TITLE)));
                    task.setDescription(cursor.getString(cursor.getColumnIndex(
                            TaskTable.COLUMN_NAME_DESCRIPTION)));
                    task.setCategoryId(cursor.getInt(cursor.getColumnIndex(
                            TaskTable.COLUMN_NAME_CATEGORY_ID)));
                    task.setDate(cursor.getString(cursor.getColumnIndex(
                            TaskTable.COLUMN_NAME_DUE_DATE)));
                    task.setReminder(cursor.getString(cursor.getColumnIndex(
                            TaskTable.COLUMN_NAME_REMINDER)));
                    task.setPriority(cursor.getString(cursor.getColumnIndex(
                            TaskTable.COLUMN_NAME_PRIORITY)));
                    task.setRepeat(cursor.getString(cursor.getColumnIndex(
                            TaskTable.COLUMN_NAME_REPEAT)));
                    task.setRepeatFrequency(cursor.getInt(cursor.getColumnIndex(
                            TaskTable.COLUMN_NAME_REPEAT_FREQUENCY)));
                    //Fetch Assignee details
                    Assignee assignee = getAssigneeDetails(cursor.getInt(cursor.getColumnIndex(
                            TaskTable.COLUMN_NAME_ASSIGNEE_TABLE_ID)));
                    task.setAssignee(assignee);

                    task.setStatus(cursor.getString(cursor.getColumnIndex(
                            TaskTable.COLUMN_NAME_STATUS)));
                    task.setTaskCreationDate(cursor.getString(cursor.getColumnIndex(
                            TaskTable.COLUMN_NAME_CREATED_ON)));
                    task.setViewType(TYPE_ITEM);
                    taskList.add(task);
                }
            } while (cursor.moveToNext());

            if (taskList.size() > 1)
                return taskList;
        }
        cursor.close();
        return Collections.emptyList();
    }

    public List<Task> getTodayTaskList(int contactId) {

        List<Task> taskList;

        SQLiteDatabase db = this.getReadableDatabase();

        String query = "SELECT * FROM " + TaskTable.TABLE_NAME + " a"
                + " JOIN " + TaskAssigneeTable.TABLE_NAME + " b"
                + " ON a." + TaskTable.COLUMN_NAME_ASSIGNEE_TABLE_ID
                + " = b." + TaskAssigneeTable.COLUMN_NAME_ID
                + " WHERE " + TaskTable.COLUMN_NAME_STATUS + " != ?"
                + " AND " + TaskAssigneeTable.COLUMN_NAME_PEOPLE_APP_CONTACT_ID + "=?"
                + " ORDER BY " + TaskTable.COLUMN_NAME_PRIORITY + " ASC";

        Cursor cursor = db.rawQuery(query, new String[]{"Complete", String.valueOf(contactId)});

        if (cursor.moveToNext()) {

            taskList = new ArrayList<>();

            Task taskHeader = new Task();
            taskHeader.setTitle("Today");
            taskHeader.setTaskHeaderTitleColor(R.color.colorAccent);
            taskHeader.setViewType(TYPE_HEADER);
            taskList.add(taskHeader);

            do {
                String dueDate = cursor.getString(cursor.getColumnIndex(
                        TaskTable.COLUMN_NAME_DUE_DATE));

                LocalDate localDateDue = LocalDateTime
                        .parse(dueDate, Common.defaultDateTimeFormatter).toLocalDate();
                LocalDate localDateNow = LocalDate.now();

                if (localDateDue.equals(localDateNow)) {
                    Task task = new Task();
                    task.setTaskId(cursor.getInt(0));
                    task.setTitle(cursor.getString(cursor.getColumnIndex(
                            TaskTable.COLUMN_NAME_TITLE)));
                    task.setDescription(cursor.getString(cursor.getColumnIndex(
                            TaskTable.COLUMN_NAME_DESCRIPTION)));
                    task.setCategoryId(cursor.getInt(cursor.getColumnIndex(
                            TaskTable.COLUMN_NAME_CATEGORY_ID)));
                    task.setDate(cursor.getString(cursor.getColumnIndex(
                            TaskTable.COLUMN_NAME_DUE_DATE)));
                    task.setReminder(cursor.getString(cursor.getColumnIndex(
                            TaskTable.COLUMN_NAME_REMINDER)));
                    task.setPriority(cursor.getString(cursor.getColumnIndex(
                            TaskTable.COLUMN_NAME_PRIORITY)));
                    task.setRepeat(cursor.getString(cursor.getColumnIndex(
                            TaskTable.COLUMN_NAME_REPEAT)));
                    task.setRepeatFrequency(cursor.getInt(cursor.getColumnIndex(
                            TaskTable.COLUMN_NAME_REPEAT_FREQUENCY)));
                    //Fetch Assignee details
                    Assignee assignee = getAssigneeDetails(cursor.getInt(cursor.getColumnIndex(
                            TaskTable.COLUMN_NAME_ASSIGNEE_TABLE_ID)));
                    task.setAssignee(assignee);

                    task.setStatus(cursor.getString(cursor.getColumnIndex(
                            TaskTable.COLUMN_NAME_STATUS)));
                    task.setTaskCreationDate(cursor.getString(cursor.getColumnIndex(
                            TaskTable.COLUMN_NAME_CREATED_ON)));
                    task.setViewType(TYPE_ITEM);
                    taskList.add(task);
                }
            } while (cursor.moveToNext());

            if (taskList.size() > 1)
                return taskList;
        }
        cursor.close();
        return Collections.emptyList();
    }

}
