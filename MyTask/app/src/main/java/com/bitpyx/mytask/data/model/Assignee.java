package com.bitpyx.mytask.data.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * @author Aamrit Yadav
 * @since 7/15/2017.
 */
public class Assignee implements Parcelable {
    private int rowId, contactId;
    private String assigneeName;

    //Empty Constructor
    public Assignee(){
    }

    public static final Creator<Assignee> CREATOR = new Creator<Assignee>() {
        @Override
        public Assignee createFromParcel(Parcel in) {
            return new Assignee(in);
        }

        @Override
        public Assignee[] newArray(int size) {
            return new Assignee[size];
        }
    };

    public int getRowId() {
        return rowId;
    }

    public void setRowId(int rowId) {
        this.rowId = rowId;
    }

    public int getContactId() {
        return contactId;
    }

    public void setContactId(int contactId) {
        this.contactId = contactId;
    }

    public String getAssigneeName() {
        return assigneeName;
    }

    public void setAssigneeName(String assigneeName) {
        this.assigneeName = assigneeName;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(rowId);
        dest.writeInt(contactId);
        dest.writeString(assigneeName);
    }

    private Assignee(Parcel in) {
        rowId = in.readInt();
        contactId = in.readInt();
        assigneeName = in.readString();
    }

    @Override
    public String toString() {
        return assigneeName ;
    }
}
