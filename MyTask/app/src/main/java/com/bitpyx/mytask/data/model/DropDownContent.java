package com.bitpyx.mytask.data.model;

/**
 * @author Amrit Yadav
 * @since 6/10/2017
 */
public class DropDownContent {

    private String colorCode, name, type;
    private int categoryId, taskCount, drawableResourceId;

    public String getColorCode() {
        return colorCode;
    }

    public void setColorCode(String colorCode) {
        this.colorCode = colorCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getTaskCount() {
        return taskCount;
    }

    public void setTaskCount(int taskCount) {
        this.taskCount = taskCount;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public int getDrawableResourceId() {
        return drawableResourceId;
    }

    public void setDrawableResourceId(int drawableResourceId) {
        this.drawableResourceId = drawableResourceId;
    }

    @Override
    public String toString() {
        return "DropDownContent{" +
                "colorCode='" + colorCode + '\'' +
                ", name='" + name + '\'' +
                ", type='" + type + '\'' +
                ", taskCount='" + taskCount + '\'' +
                ", categoryId=" + categoryId +
                ", drawableResourceId=" + drawableResourceId +
                '}';
    }
}
