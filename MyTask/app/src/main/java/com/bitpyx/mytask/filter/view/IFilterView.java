package com.bitpyx.mytask.filter.view;

import com.bitpyx.mytask.data.model.DropDownContent;
import com.bitpyx.mytask.data.model.Task;
import com.bitpyx.mytask.utility.IMvpView;

import java.util.List;

/**
 * @author Amrit Yadav
 * @since 7/13/2017.
 */
public interface IFilterView extends IMvpView {
    void refreshTaskList(List<Task> taskList);

    void categoryList(List<DropDownContent> dropDownContents);

}
