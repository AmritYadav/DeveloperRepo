package com.bitpyx.mytask.search;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bitpyx.mytask.R;
import com.bitpyx.mytask.adapter.TaskSearchViewAdapter;
import com.bitpyx.mytask.addTask.AddEditTaskActivity;
import com.bitpyx.mytask.data.model.DropDownContent;
import com.bitpyx.mytask.data.model.Task;
import com.bitpyx.mytask.main.MainActivity;
import com.bitpyx.mytask.search.presenters.GlobalTaskSearchPresenter;
import com.bitpyx.mytask.search.presenters.IGlobalTaskSearchPresenter;
import com.bitpyx.mytask.search.views.IGlobalTaskSearchView;
import com.bitpyx.mytask.utility.RecyclerTouchListener;

import java.util.Collections;
import java.util.List;

/**
 * A fragment representing a list of Items.
 * <p/>
 */
public class SearchFragment extends Fragment implements IGlobalTaskSearchView {

    private static final String ARG_COLUMN_QUERY = "query";
    private static final String ARG_FRAG_TAG = "argFragmentTag";
    private static final String ARG_CATEGORY_FRAG_ID = "argCategoryFragId";
    private static final String ARG_CATEGORY_FRAG_TAG = "argCategoryFragTag";
    private String mSearchQuery = "";
    private String mFragmentTag, mCategoryFragTag;
    private int mCategoryFragId;
    private List<DropDownContent> mCategoryList;
    private List<Task> mTaskList;
    protected RecyclerView mRecyclerView;
    private TaskSearchViewAdapter mTaskSearchViewAdapter;
    private IGlobalTaskSearchPresenter<IGlobalTaskSearchView> mGlobalTaskSearchPresenter;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public SearchFragment() {
    }

    @SuppressWarnings("unused")
    public static SearchFragment newInstance(Bundle bundle) {
        SearchFragment fragment = new SearchFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mGlobalTaskSearchPresenter = new GlobalTaskSearchPresenter();
        mGlobalTaskSearchPresenter.attachView(this);
        mGlobalTaskSearchPresenter.getCategoryList();
        Bundle arguments = getArguments();
        if (arguments != null) {
            mSearchQuery = arguments.getString(ARG_COLUMN_QUERY);
            mFragmentTag=arguments.getString(ARG_FRAG_TAG);
            mCategoryFragTag=arguments.getString(ARG_CATEGORY_FRAG_TAG);
            mCategoryFragId=arguments.getInt(ARG_CATEGORY_FRAG_ID, 0);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_task_list, container, false);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.recycler_view_task_search);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerView.addOnItemTouchListener(new RecyclerTouchListener(getActivity()
                , mRecyclerView, new MainActivity.ClickListener() {
            @Override
            public void onClick(View view, int position) {

                Task task = mTaskList.get(position);

                Intent intent = new Intent(getActivity(), AddEditTaskActivity.class);
                intent.putExtra(ARG_FRAG_TAG, mFragmentTag);
                intent.putExtra(ARG_CATEGORY_FRAG_TAG, mCategoryFragTag);
                intent.putExtra(ARG_CATEGORY_FRAG_ID, mCategoryFragId);
                intent.putExtra("Task", task);
                intent.putExtra("Type", "edit");
                startActivity(intent);
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        DividerItemDecoration mDividerItemDecoration =
                new DividerItemDecoration(mRecyclerView.getContext(),
                        DividerItemDecoration.VERTICAL);
        mRecyclerView.addItemDecoration(mDividerItemDecoration);

        if (!mSearchQuery.trim().isEmpty()) {
            mGlobalTaskSearchPresenter.searchTask(mSearchQuery);
            mTaskSearchViewAdapter =
                    new TaskSearchViewAdapter(getActivity(), mTaskList, mCategoryList, true
                            , null, null);
            mRecyclerView.setAdapter(mTaskSearchViewAdapter);
        }

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void filteredTaskList(List<Task> taskList) {
        mTaskList = taskList;
    }

    @Override
    public void categoryList(List<DropDownContent> dropDownContents) {
        mCategoryList = dropDownContents;
    }

    public void filterSearchResult(String query) {
        mSearchQuery = query;
        if (!mSearchQuery.trim().isEmpty()) {
            mGlobalTaskSearchPresenter.searchTask(mSearchQuery);
            mTaskSearchViewAdapter =
                    new TaskSearchViewAdapter(getActivity(), mTaskList, mCategoryList, true
                            , null, null);
        } else {
            mTaskSearchViewAdapter =
                    new TaskSearchViewAdapter(getActivity(), Collections.<Task>emptyList()
                            , mCategoryList, true
                            , null, null);
        }
        mRecyclerView.setAdapter(mTaskSearchViewAdapter);
    }

}
