package com.bitpyx.mytask.addTask.inrteractors;

import com.bitpyx.mytask.data.model.DropDownContent;
import com.bitpyx.mytask.data.model.Task;

import java.util.List;

/**
 * @author Amrit Yadav
 * @since 6/17/2017
 */
public interface IAddEditInteractor {

    long insertNewCategory(String categoryName);

    boolean categoryExists(String categoryName);

    List<DropDownContent> fetchCategoryList();

    List<DropDownContent> fetchPriorityList();

    List<DropDownContent> fetchRepeatedList();

    String fetchCategoryName(int categoryId);

    long saveNewTask(Task task);

    long updateTask(Task task);

}
