package com.bitpyx.mytask.category.manage.presenters;

import com.bitpyx.mytask.category.manage.interactors.IMCategoryInteractor;
import com.bitpyx.mytask.category.manage.interactors.MCategoryInteractor;
import com.bitpyx.mytask.category.manage.view.IMCategoryView;
import com.bitpyx.mytask.data.model.DropDownContent;

import java.util.List;

/**
 * @author Amrit Yadav
 * @since 7/9/2017.
 */
public class MCategoryPresenter implements IMCategoryPresenter<IMCategoryView> {
    private IMCategoryView mCategoryView;
    private IMCategoryInteractor mCategoryInteractor;
    private IMCategoryView.OnCategoryAdded mOnCategoryAdded;
    @Override
    public void attachView(IMCategoryView view) {
        mCategoryView=view;
        mCategoryInteractor=new MCategoryInteractor(mCategoryView.getContext());
    }

    @Override
    public void fetchTaskCategories() {
        List<DropDownContent> mCategoryList = mCategoryInteractor.fetchCategoryList();
        mCategoryView.showCategoryList(mCategoryList);
    }

    @Override
    public void saveNewCategory(String categoryName, boolean editCategory
            , DropDownContent categoryObj, IMCategoryView.OnCategoryAdded onCategoryAdded) {

        mOnCategoryAdded = onCategoryAdded;
        boolean categoryExists = mCategoryInteractor.categoryExists(categoryName);
        if(!categoryName.trim().isEmpty()) {
            if (categoryExists) mOnCategoryAdded.onFailure("Category Already Exists");
            else {

                long rowId = editCategory ? mCategoryInteractor.updateCategory(categoryName
                        , categoryObj.getCategoryId())
                        : mCategoryInteractor.insertNewCategory(categoryName);

                boolean status = rowId > 0;

                if(status){
                    mOnCategoryAdded.onSuccess();
                    String message = editCategory ? "Category updated" : "New category Added";
                    mCategoryView.showToast(message);
                } else mOnCategoryAdded.onFailure("Failed to add new category");
            }
        } else {
            mOnCategoryAdded.onFailure("Category name cannot be empty");
        }

    }

    @Override
    public void removeCategory(DropDownContent categoryObj
            , IMCategoryView.OnCategoryAdded onCategoryAdded) {
        mOnCategoryAdded = onCategoryAdded;
        mCategoryInteractor.deleteCategory(categoryObj);
        mOnCategoryAdded.onSuccess();
        mCategoryView.showToast("Category remove");
    }

    @Override
    public void getTaskCountForTheCategory(int categoryId) {
        int taskCount = mCategoryInteractor.fetchTaskCountCountForCategory(categoryId);
        boolean hasTask = taskCount > 0;
        mCategoryView.checkTaskCountForTheCategory(hasTask);
    }

    @Override
    public void detachView() {

    }
}
