package com.bitpyx.mytask.search.interactors;

import com.bitpyx.mytask.data.model.DropDownContent;
import com.bitpyx.mytask.data.model.Task;

import java.util.List;

/**
 * @author Amrit Yadav
 * @since 6/24/2017
 */
public interface IGlobalTaskSearchInteractor {

    List<Task> filterTask(String filterQuery);
    List<DropDownContent> fetchCategoryList();

}
