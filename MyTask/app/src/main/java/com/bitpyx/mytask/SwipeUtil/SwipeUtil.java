package com.bitpyx.mytask.SwipeUtil;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.support.v7.content.res.AppCompatResources;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;

import com.bitpyx.mytask.R;

public abstract class SwipeUtil extends ItemTouchHelper.SimpleCallback {

    private Context context;
    private Paint p = new Paint();

    protected SwipeUtil(int dragDirs, int swipeDirs, Context context) {
        super(dragDirs, swipeDirs);
        this.context = context;
    }

    @Override
    public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
        return false;
    }

    @Override
    public abstract void onSwiped(RecyclerView.ViewHolder viewHolder, int direction);

    @Override
    public int getSwipeDirs(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {

        return super.getSwipeDirs(recyclerView, viewHolder);
    }


    @Override
    public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder,
                            float dX, float dY, int actionState, boolean isCurrentlyActive) {

        View itemView = viewHolder.itemView;

        p.setColor(Color.parseColor("#D32F2F"));
        RectF background = new RectF((float) itemView.getRight() + dX, (float) itemView.getTop(),(float) itemView.getRight(), (float) itemView.getBottom());
        c.drawRect(background,p);

        int itemHeight = itemView.getBottom() - itemView.getTop();

        Drawable deleteIcon = AppCompatResources.getDrawable(context, R.drawable.ic_delete);
        int intrinsicWidth = deleteIcon != null ? deleteIcon.getIntrinsicWidth() : 0;
        int intrinsicHeight = deleteIcon != null ? deleteIcon.getIntrinsicWidth() : 0;

        int xMarkMargin =(int) context.getResources().getDimension(R.dimen.ic_clear_margin);
        int xMarkLeft = itemView.getRight() - xMarkMargin - intrinsicWidth;
        int xMarkRight = itemView.getRight() - xMarkMargin;
        int xMarkTop = itemView.getTop() + (itemHeight - intrinsicHeight) / 2;
        int xMarkBottom = xMarkTop + intrinsicHeight;


        //Setting Swipe Icon
        if (deleteIcon != null) {
            deleteIcon.setBounds(xMarkLeft, xMarkTop , xMarkRight, xMarkBottom);
            deleteIcon.draw(c);
        }
        super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
    }

}