package com.bitpyx.mytask.allTask.interactors;

import android.content.Context;

import com.bitpyx.mytask.data.model.DropDownContent;
import com.bitpyx.mytask.data.model.Task;
import com.bitpyx.mytask.data.source.local.dbconstants.DatabaseHelper;

import org.joda.time.LocalDateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.List;

/**
 * @author Amrit Yadav
 * @since 25-06-2017
 */
public class AllTaskInteractor implements IAllTaskInteractor {
    private DatabaseHelper mDatabaseHelper;
    private DateTimeFormatter simpleDateFormat = DateTimeFormat.forPattern("yyyy-MM-dd hh:mm a");

    public AllTaskInteractor(Context context) {
        mDatabaseHelper = new DatabaseHelper(context);
    }

    @Override
    public List<Task> fetchAllTaskList() {
        return mDatabaseHelper.getAllCategorizedTask();
    }

    @Override
    public List<DropDownContent> fetchCategoryList() {
        return mDatabaseHelper.getCategoryList();
    }

    @Override
    public void deleteTask(int taskId) {
        mDatabaseHelper.deleteTask(taskId);
    }

    @Override
    public void saveUndoTask(List<Task> undoTaskList) {
        for (Task task : undoTaskList) {
            mDatabaseHelper.addNewTask(task);
        }
    }

    @Override
    public void updateTasksStatus(List<Task> mTaskList, List<Integer> mListItemsToUpdate) {
        try {
            int listSize = mListItemsToUpdate.size();
            for (int i = 0; i < listSize; i++) {
                Task task = mTaskList.get(mListItemsToUpdate.get(i));
                LocalDateTime localDateTime = LocalDateTime.parse(task.getDate(), simpleDateFormat);

                switch (task.getRepeat()) {
                    case "One Time":
                        task.setStatus("Complete");
                        break;
                    case "Weekly":
                        task.setDate(simpleDateFormat
                                .print(localDateTime.plusWeeks(task.getRepeatFrequency())));
                        break;
                    case "Monthly":
                        task.setDate(simpleDateFormat
                                .print(localDateTime.plusMonths(task.getRepeatFrequency())));
                        break;
                    case "Yearly":
                        task.setDate(simpleDateFormat
                                .print(localDateTime.plusYears(task.getRepeatFrequency())));
                        break;
                }

                mDatabaseHelper.updateTask(task);

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void updateTaskCategory(Task task) {
        mDatabaseHelper.updateTask(task);
    }
}
