package com.bitpyx.mytask.search.interactors;

import android.content.Context;

import com.bitpyx.mytask.data.model.DropDownContent;
import com.bitpyx.mytask.data.model.Task;
import com.bitpyx.mytask.data.source.local.dbconstants.DatabaseHelper;

import java.util.List;

/**
 * @author Amrit Yadav
 * @since 6/24/2017
 */
public class GlobalTaskSearchInteractor implements IGlobalTaskSearchInteractor{
    private DatabaseHelper mDatabaseHelper;
    public GlobalTaskSearchInteractor(Context context) {
        mDatabaseHelper = new DatabaseHelper(context);
    }

    @Override
    public List<Task> filterTask(String filterQuery) {
        return mDatabaseHelper.getFilteredTaskList(filterQuery);
    }

    @Override
    public List<DropDownContent> fetchCategoryList() {
        return mDatabaseHelper.getCategoryList();
    }
}
