package com.bitpyx.mytask.main.interactors;

import com.bitpyx.mytask.data.model.Assignee;
import com.bitpyx.mytask.data.model.DropDownContent;

import java.util.List;

/**
 * @author Amrit Yadav
 * @since 21-05-2017
 */
public interface IMainInteractor {

    void addDefaultTaskCategory(String newTaskCategory);

    boolean checkDefaultTaskCategory();

    List<DropDownContent> fetchCategoryList();

    int fetchTodayTaskCount();

    int fetchTaskCompleteCount();

    List<Assignee> fetchAssigneeList();

    void addAppCreatedCategoryList(List<String> categoryList);

}
