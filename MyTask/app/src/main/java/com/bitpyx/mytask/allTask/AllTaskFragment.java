package com.bitpyx.mytask.allTask;

import android.animation.Animator;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.LinearInterpolator;

import com.bitpyx.mytask.R;
import com.bitpyx.mytask.SwipeUtil.SwipeUtil;
import com.bitpyx.mytask.adapter.TaskSearchViewAdapter;
import com.bitpyx.mytask.addTask.AddEditTaskActivity;
import com.bitpyx.mytask.allTask.presenters.AllTaskPresenter;
import com.bitpyx.mytask.allTask.presenters.IAllTaskPresenter;
import com.bitpyx.mytask.allTask.views.IAllTaskView;
import com.bitpyx.mytask.data.model.DropDownContent;
import com.bitpyx.mytask.data.model.Task;
import com.bitpyx.mytask.main.MainActivity;
import com.bitpyx.mytask.utility.RecyclerItemTouchHelperCallback;
import com.bitpyx.mytask.utility.RecyclerTouchListener;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * Use the {@link AllTaskFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AllTaskFragment extends Fragment implements IAllTaskView
        , View.OnClickListener, TaskSearchViewAdapter.OnStartDragListener {

    private static final String ARG_CONTACT_ID = "contact_id";
    private static final String EXTRA_MAIN_FRAGMENT = "mainFragment";
    private int statusBarColor;
    private ActionMode mActionMode;
    private RecyclerView mRecyclerView;
    private TaskSearchViewAdapter mTaskSearchViewAdapter;
    private Snackbar snackbar;
    private List<Task> mTaskList;
    private List<Task> mRemovedTaskList;
    private List<DropDownContent> mCategoryList;
    private List<Integer> mListItemsToUpdate = new ArrayList<>();
    private boolean mIsMultiSelect = false;
    private ItemTouchHelper mItemTouchHelper;
    private IAllTaskPresenter<IAllTaskView> mAllTaskPresenter;
    private FloatingActionButton mFabAddEditTask;

    private ActionMode.Callback mActionModeCallback = new ActionMode.Callback() {

        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                //hold current color of status bar
                statusBarColor = getActivity().getWindow().getStatusBarColor();
                getActivity().getWindow()
                        .setStatusBarColor(ContextCompat.getColor(getActivity()
                                , R.color.actionModePrimaryColorDark));
            }

            CoordinatorLayout.LayoutParams layoutParams =
                    (CoordinatorLayout.LayoutParams) mFabAddEditTask.getLayoutParams();
            int fab_bottomMargin = layoutParams.bottomMargin;

            mFabAddEditTask.animate().translationY(mFabAddEditTask.getHeight() + fab_bottomMargin)
                    .setInterpolator(new LinearInterpolator()).setListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animation) {

                }

                @Override
                public void onAnimationEnd(Animator animation) {
                    if (mIsMultiSelect)
                        mFabAddEditTask.setVisibility(View.GONE);
                }

                @Override
                public void onAnimationCancel(Animator animation) {

                }

                @Override
                public void onAnimationRepeat(Animator animation) {

                }
            }).start();

            // Inflate a menu resource providing context menu items
            MenuInflater inflater = mode.getMenuInflater();
            inflater.inflate(R.menu.menu_multi_select, menu);
            MenuItem item = menu.findItem(R.id.action_send);
            item.setVisible(false);
            MenuItem itemRestoreTask = menu.findItem(R.id.action_restore);
            itemRestoreTask.setVisible(false);
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false; // Return false if nothing is done
        }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            mAllTaskPresenter.onActionItemClicked(mTaskList, mListItemsToUpdate, mRecyclerView);
            mActionMode = null;
            mIsMultiSelect = false;
            return false;
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                //return to "old" color of status bar
                getActivity().getWindow().setStatusBarColor(statusBarColor);
            }
            mFabAddEditTask.setVisibility(View.VISIBLE);
            mFabAddEditTask.animate().translationY(0).setInterpolator(new LinearInterpolator()).start();
            mActionMode = null;
            mIsMultiSelect = false;
            mAllTaskPresenter.onDestroyActionMode(mTaskList, mListItemsToUpdate, mRecyclerView);
        }
    };

    public AllTaskFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param contactId Parameter 1.
     * @return A new instance of fragment AllTaskFragment.
     */
    public static AllTaskFragment newInstance(int contactId) {
        AllTaskFragment fragment = new AllTaskFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_CONTACT_ID, contactId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            getArguments().getInt(ARG_CONTACT_ID);
        }
        mRemovedTaskList = new ArrayList<>();
        mAllTaskPresenter = new AllTaskPresenter();
        mAllTaskPresenter.attachView(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_today, container, false);

        CoordinatorLayout mCorCoordinatorLayout = (CoordinatorLayout) container.getParent();

        mFabAddEditTask = (FloatingActionButton) mCorCoordinatorLayout
                .findViewById(R.id.fab_add_edit_task);

        snackbar = Snackbar.make(mCorCoordinatorLayout, "", 2000).setAction("UNDO", this)
                .addCallback(new Snackbar.Callback() {
                    @Override
                    public void onDismissed(Snackbar snackbar, int event) {
                        mRemovedTaskList.clear();
                    }

                    @Override
                    public void onShown(Snackbar snackbar) {

                    }
                });

        mRecyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerView.addOnItemTouchListener(new RecyclerTouchListener(getActivity()
                , mRecyclerView, new MainActivity.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                Task task = mTaskList.get(position);
                if (mIsMultiSelect) {
                    if (task.isSelected()) {
                        mAllTaskPresenter.unSelectListItem(view, task);
                        mListItemsToUpdate.removeAll(Arrays.asList(position));
                    } else {
                        mAllTaskPresenter.selectListItem(view, task);
                        mListItemsToUpdate.add(position);
                    }

                    mActionMode.setTitle("" + mListItemsToUpdate.size());

                    if (mListItemsToUpdate.isEmpty() && mActionMode != null) {
                        mIsMultiSelect = false;
                        mActionMode.finish();
                        mActionMode = null;
                    }
                } else {
                    Intent intent = new Intent(getActivity(), AddEditTaskActivity.class);
                    intent.putExtra(EXTRA_MAIN_FRAGMENT, "All");
                    intent.putExtra("Task", task);
                    intent.putExtra("Type", "edit");
                    startActivity(intent);
                }
            }

            @Override
            public void onLongClick(View view, int position) {
                int viewId = view.getId();
                switch (viewId) {
                    case R.id.ib_task_drag:
                        mTaskSearchViewAdapter = (TaskSearchViewAdapter) mRecyclerView.getAdapter();
                        ItemTouchHelper.Callback callback =
                                new RecyclerItemTouchHelperCallback(mTaskSearchViewAdapter, true);
                        mItemTouchHelper = new ItemTouchHelper(callback);
                        mItemTouchHelper.attachToRecyclerView(mRecyclerView);
                        break;
                    default:
                        mTaskSearchViewAdapter = (TaskSearchViewAdapter) mRecyclerView.getAdapter();
                        ItemTouchHelper.Callback callback1 =
                                new RecyclerItemTouchHelperCallback(mTaskSearchViewAdapter, false);
                        mItemTouchHelper = new ItemTouchHelper(callback1);
                        mItemTouchHelper.attachToRecyclerView(mRecyclerView);
                        mIsMultiSelect = true;
                        if (!mListItemsToUpdate.contains(position)) {
                            mTaskSearchViewAdapter.refreshAdapter(mTaskList, true);
                            Task task = mTaskList.get(position);
                            mAllTaskPresenter.selectListItem(view, task);
                            mListItemsToUpdate.add(position);
                        }
                        if (mActionMode == null) {
                            mActionMode = getActivity().startActionMode(mActionModeCallback);
                            mActionMode.setTitle("" + mListItemsToUpdate.size());
                        }
                }
            }
        }));

        DividerItemDecoration mDividerItemDecoration =
                new DividerItemDecoration(mRecyclerView.getContext(),
                        DividerItemDecoration.VERTICAL);
        mRecyclerView.addItemDecoration(mDividerItemDecoration);

        setSwipeForRecyclerView();

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onResume() {
        super.onResume();
        mAllTaskPresenter.getCategoryList();
        mAllTaskPresenter.fetchAllTaskList();
    }

    @Override
    public void setAdapter(List<Task> taskList) {

        if (mActionMode != null)
            mActionMode.finish();

        mTaskList = taskList;
        mTaskSearchViewAdapter = new TaskSearchViewAdapter(getActivity()
                , mTaskList, mCategoryList, false, this, mAllTaskPresenter);
        mRecyclerView.setAdapter(mTaskSearchViewAdapter);
        ItemTouchHelper.Callback callback =
                new RecyclerItemTouchHelperCallback(mTaskSearchViewAdapter, false);
        mItemTouchHelper = new ItemTouchHelper(callback);
        mItemTouchHelper.attachToRecyclerView(mRecyclerView);
    }

    @Override
    public void categoryList(List<DropDownContent> dropDownContents) {
        mCategoryList = dropDownContents;
    }

    private void setSwipeForRecyclerView() {

        SwipeUtil swipeHelper = new SwipeUtil(0, ItemTouchHelper.LEFT, getActivity()) {

            @Override
            public int getMovementFlags(RecyclerView recyclerView
                    , RecyclerView.ViewHolder viewHolder) {
                return viewHolder.getItemViewType() == 0
                        ? 0 : super.getMovementFlags(recyclerView, viewHolder);
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                int swipedPosition = viewHolder.getAdapterPosition();
                Task task = mTaskList.get(swipedPosition);
                mAllTaskPresenter.removeTask(task.getTaskId());
                mRemovedTaskList.add(task);
                mTaskSearchViewAdapter = (TaskSearchViewAdapter) mRecyclerView.getAdapter();
                mTaskSearchViewAdapter.pendingRemoval(swipedPosition);
                String message = "DELETE (" + mRemovedTaskList.size() + ")";
                snackbar.setText(message);
                snackbar.show();
            }

            @Override
            public int getSwipeDirs(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
                int position = viewHolder.getAdapterPosition();
                mTaskSearchViewAdapter = (TaskSearchViewAdapter) mRecyclerView.getAdapter();
                if (position >= 0 && mTaskSearchViewAdapter.isPendingRemoval(position)) {
                    return 0;
                }
                return super.getSwipeDirs(recyclerView, viewHolder);
            }
        };

        ItemTouchHelper mItemTouchHelper = new ItemTouchHelper(swipeHelper);
        mItemTouchHelper.attachToRecyclerView(mRecyclerView);
    }

    @Override
    public void onClick(View v) {
        mAllTaskPresenter.addUndoTask(mRemovedTaskList);
        mAllTaskPresenter.fetchAllTaskList();
        mRemovedTaskList.clear();
        mTaskSearchViewAdapter = new TaskSearchViewAdapter(getActivity()
                , mTaskList, mCategoryList, false, this, mAllTaskPresenter);
        mRecyclerView.setAdapter(mTaskSearchViewAdapter);
        mTaskSearchViewAdapter.notifyDataSetChanged();
    }

    @Override
    public void onStartDrag(RecyclerView.ViewHolder viewHolder) {
        mItemTouchHelper.startDrag(viewHolder);
    }
}
