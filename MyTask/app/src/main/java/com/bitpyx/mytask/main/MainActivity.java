package com.bitpyx.mytask.main;

import android.app.ActivityOptions;
import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ExpandableListView;
import android.widget.ListView;
import android.widget.Toast;

import com.bitpyx.mytask.R;
import com.bitpyx.mytask.adapter.NavExpandableListAdapter;
import com.bitpyx.mytask.addTask.AddEditTaskActivity;
import com.bitpyx.mytask.allTask.AllTaskFragment;
import com.bitpyx.mytask.category.CategoryFragment;
import com.bitpyx.mytask.category.manage.ManageCategoryFragment;
import com.bitpyx.mytask.complete.TaskCompleteFragment;
import com.bitpyx.mytask.data.model.Assignee;
import com.bitpyx.mytask.data.model.DropDownContent;
import com.bitpyx.mytask.data.model.NavSubTitle;
import com.bitpyx.mytask.data.model.NavTitle;
import com.bitpyx.mytask.data.model.Task;
import com.bitpyx.mytask.filter.TaskFilterFragment;
import com.bitpyx.mytask.main.presenters.IMainPresenter;
import com.bitpyx.mytask.main.presenters.MainPresenterImpl;
import com.bitpyx.mytask.main.view.IMainView;
import com.bitpyx.mytask.receiver.AlarmReceiver;
import com.bitpyx.mytask.search.SearchFragment;
import com.bitpyx.mytask.today.TodayTaskFragment;
import com.bitpyx.mytask.utility.Common;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import jxl.write.WritableSheet;

import static com.bitpyx.mytask.utility.Common.TASK_PRIORITY_HIGH;
import static com.bitpyx.mytask.utility.Common.TASK_PRIORITY_MEDIUM;
import static com.bitpyx.mytask.utility.Common.TASK_PRIORITY_NORMAL;
import static com.bitpyx.mytask.utility.Common.createTaskExcelHeader;

public class MainActivity extends AppCompatActivity implements IMainView
        , View.OnClickListener, ExpandableListView.OnGroupClickListener
        , ExpandableListView.OnGroupCollapseListener, ExpandableListView.OnGroupExpandListener
        , ExpandableListView.OnChildClickListener {

    private static final String FRAGMENT_MAIN_FRAGMENT = "mainFragment";
    private static final String FRAGMENT_CATEGORY_FRAGMENT_ID = "categoryFragmentId";
    private static final String FRAGMENT_CATEGORY_FRAGMENT_TITLE = "categoryFragmentTitle";
    private static final String ARG_FRAG_TAG = "argFragmentTag";
    private static final String ARG_COLUMN_QUERY = "query";
    private static final String ARG_CATEGORY_FRAG_ID = "argCategoryFragId";
    private static final String ARG_CATEGORY_FRAG_TAG = "argCategoryFragTag";

    private List<NavTitle> mNavTitleList;
    private List<NavSubTitle> mNavSubTitleCategoryList;
    private List<NavSubTitle> mNavSubTitleFilterList;
    private List<Assignee> mAssigneeList;
    HashMap<String, List<NavSubTitle>> mNavMenuMap;
    NavExpandableListAdapter navExpandableListAdapter;
    private boolean isGroupExpanded = false;
    private boolean isActivityResumed = false;
    private boolean isMenuItemCollapsed = false;
    private String navigateBackToPrevFragment, navigateBackToPrevCategoryTag;
    private String mLastSelectedFragment, mLastSelectedCategoryFragmentTitle, mLastSearchedQuery = "";
    private int mLastSelectedCategoryFragmentId, navigateBackToPrevCategoryId;
    private int navTodayTaskCount, navTaskCompleteCount;
    private int navGroupExpandPosition;
    private int prevSelectedContactId;
    private DrawerLayout drawerLayout;
    private FragmentManager mFragmentManager;
    private FloatingActionButton mAddEditTask;
    private ExpandableListView mExpandableListView;
    private Fragment mSelectedFragment;
    private AlertDialog mAlertDialog;
    private MenuItem mSearchItem, mExportToExcelItem, mFilterAssignee;
    private IMainPresenter<IMainView> mMainPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_task_drawer);

        Intent intent = getIntent();
        mLastSelectedFragment = intent.getStringExtra(FRAGMENT_MAIN_FRAGMENT);
        if (mLastSelectedFragment == null)
            mLastSelectedFragment = "Today";
        mLastSelectedCategoryFragmentId = intent.getIntExtra(FRAGMENT_CATEGORY_FRAGMENT_ID, 0);
        mLastSelectedCategoryFragmentTitle = intent.getStringExtra(FRAGMENT_CATEGORY_FRAGMENT_TITLE);

        mNavSubTitleCategoryList = new ArrayList<>();
        mNavTitleList = new ArrayList<>();
        mNavMenuMap = new HashMap<>();

        mMainPresenter = new MainPresenterImpl();
        initViews();

    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);

        mSearchItem = menu.findItem(R.id.action_search);
        mExportToExcelItem = menu.findItem(R.id.action_export_to_excel);
        mFilterAssignee = menu.findItem(R.id.action_filter_assignee);

        SearchView searchView = (SearchView) MenuItemCompat.getActionView(mSearchItem);
        searchView.setOnQueryTextFocusChangeListener(new View.OnFocusChangeListener() {

            @Override
            public void onFocusChange(View v, boolean hasFocus) {

            }
        });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String searchQuery) {
                mLastSearchedQuery = searchQuery;
                SearchFragment searchFragment = (SearchFragment) mFragmentManager
                        .findFragmentByTag("Search");
                if (searchFragment != null && searchFragment.isVisible()) {
                    searchFragment.filterSearchResult(searchQuery);
                }
                return true;
            }
        });

        MenuItemCompat.setOnActionExpandListener(mSearchItem, new MenuItemCompat
                .OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                mExportToExcelItem.setVisible(true);
                mFilterAssignee.setVisible(true);
                mExportToExcelItem.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
                mSearchItem.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS
                        | MenuItem.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW);
                isMenuItemCollapsed = true;
                showPreviousFragment();
                new Handler(Looper.myLooper()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mAddEditTask.setVisibility(View.VISIBLE);
                    }
                }, 100);
                return true;  // Return true to collapse action view
            }

            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                mExportToExcelItem.setVisible(false);
                mFilterAssignee.setVisible(false);
                navigateBackToPrevFragment = mLastSelectedFragment;
                navigateBackToPrevCategoryId = mLastSelectedCategoryFragmentId;
                navigateBackToPrevCategoryTag = mLastSelectedCategoryFragmentTitle;
                isMenuItemCollapsed = false;
                mAddEditTask.setVisibility(View.GONE);
                Bundle bundle = new Bundle();
                bundle.putString(ARG_COLUMN_QUERY, mLastSearchedQuery);
                bundle.putString(ARG_FRAG_TAG, navigateBackToPrevFragment);
                bundle.putString(ARG_CATEGORY_FRAG_TAG, navigateBackToPrevCategoryTag);
                bundle.putInt(ARG_CATEGORY_FRAG_ID, navigateBackToPrevCategoryId);
                FragmentTransaction transaction = mFragmentManager.beginTransaction();
                transaction.replace(R.id.frame_layout, SearchFragment.newInstance(bundle), "Search");
                transaction.commit();
                mLastSelectedFragment = "Search";
                return true;  // Return true to expand action view
            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        switch (id) {
            case R.id.action_search:
                return true;
            case R.id.action_export_to_excel:
                prepareSelectedFragmentDataForExcelExport();
                break;
            case R.id.action_filter_assignee:
                prepareAssigneeFilter();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void initViews() {

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mFragmentManager = getSupportFragmentManager();

        // get the listView
        mExpandableListView = (ExpandableListView) findViewById(R.id.nav_content);
        mExpandableListView.setOnGroupClickListener(this);
        mExpandableListView.setOnGroupCollapseListener(this);
        mExpandableListView.setOnGroupExpandListener(this);
        mExpandableListView.setOnChildClickListener(this);

        // preparing list data
        prepareListData();

        navExpandableListAdapter = new NavExpandableListAdapter(MainActivity.this
                , mNavTitleList, mNavMenuMap);
        mExpandableListView.setAdapter(navExpandableListAdapter);

        mAddEditTask = (FloatingActionButton) findViewById(R.id.fab_add_edit_task);
        mAddEditTask.setOnClickListener(MainActivity.this);

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer);
        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout
                , toolbar, R.string.drawer_open, R.string.drawer_close) {

            @Override
            public void onDrawerClosed(View v) {
                super.onDrawerClosed(v);
            }

            @Override
            public void onDrawerOpened(View v) {
                super.onDrawerOpened(v);
                mMainPresenter.getCategoryList();
                mMainPresenter.getTodayTaskCount();
                mMainPresenter.getTaskCompleteCount();
                NavTitle navTitleToday = (NavTitle) mExpandableListView.getItemAtPosition(1);
                navTitleToday.setTaskCount(navTodayTaskCount);
                NavTitle navTitleTaskComplete = (NavTitle) mExpandableListView.getItemAtPosition(2);
                navTitleTaskComplete.setTaskCount(navTaskCompleteCount);
                navExpandableListAdapter.notifyDataSetChanged();
                if (isGroupExpanded)
                    mExpandableListView.expandGroup(navGroupExpandPosition);
                else
                    mExpandableListView.collapseGroup(3);
            }
        };
        drawerLayout.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();

    }

    /*
    * Preparing the list data
    */
    private void prepareListData() {

        NavTitle navTitle = new NavTitle();
        navTitle.setTitleText("All");
        navTitle.setDrawableId(R.drawable.ic_all);
        navTitle.setTaskCount(0);
        navTitle.setmNavSubTitleList(Collections.<NavSubTitle>emptyList());

        NavTitle navTitle1 = new NavTitle();
        navTitle1.setTitleText("Today");
        navTitle1.setDrawableId(R.drawable.ic_today);
        navTitle1.setTaskCount(0);
        navTitle1.setmNavSubTitleList(Collections.<NavSubTitle>emptyList());

        NavTitle navTitle2 = new NavTitle();
        navTitle2.setTitleText("Complete");
        navTitle2.setDrawableId(R.drawable.ic_complete);
        navTitle2.setTaskCount(0);
        navTitle2.setmNavSubTitleList(Collections.<NavSubTitle>emptyList());

        NavTitle navTitle3 = new NavTitle();
        navTitle3.setTitleText("Category");
        navTitle3.setDrawableId(R.drawable.ic_category);
        navTitle3.setTaskCount(0);
        navTitle3.setmNavSubTitleList(mNavSubTitleCategoryList);

        NavTitle navTitle4 = new NavTitle();
        navTitle4.setTitleText("Manage Category");
        navTitle4.setDrawableId(R.drawable.ic_settings);
        navTitle4.setTaskCount(0);
        navTitle4.setmNavSubTitleList(Collections.<NavSubTitle>emptyList());

        NavTitle navTitle5 = new NavTitle();
        navTitle5.setTitleText("Filter");
        navTitle5.setDrawableId(R.drawable.ic_filter);
        navTitle5.setTaskCount(0);
        navTitle5.setmNavSubTitleList(mNavSubTitleCategoryList);

        mNavTitleList.add(navTitle);
        mNavTitleList.add(navTitle1);
        mNavTitleList.add(navTitle2);
        mNavTitleList.add(navTitle3);
        mNavTitleList.add(navTitle4);
        mNavTitleList.add(navTitle5);

        mNavMenuMap.put(mNavTitleList.get(0).getTitleText(), Collections.<NavSubTitle>emptyList());
        mNavMenuMap.put(mNavTitleList.get(1).getTitleText(), Collections.<NavSubTitle>emptyList());
        mNavMenuMap.put(mNavTitleList.get(2).getTitleText(), Collections.<NavSubTitle>emptyList());
        mNavMenuMap.put(mNavTitleList.get(3).getTitleText(), mNavSubTitleCategoryList);
        mNavMenuMap.put(mNavTitleList.get(4).getTitleText(), Collections.<NavSubTitle>emptyList());

        //Prepares list for filter Navigation Sub Menu
        prepareNavFilterTaskList();
        mNavMenuMap.put(mNavTitleList.get(5).getTitleText(), mNavSubTitleFilterList);
    }

    public void prepareNavFilterTaskList() {
        mNavSubTitleFilterList = new ArrayList<>();

        NavSubTitle navSubTitleNormal = new NavSubTitle();
        navSubTitleNormal.setSubTitleText("Normal");
        navSubTitleNormal.setSubMenuId(TASK_PRIORITY_NORMAL);
        navSubTitleNormal.setDrawableId(R.drawable.ic_category_item);
        mNavSubTitleFilterList.add(navSubTitleNormal);
        NavSubTitle navSubTitleMedium = new NavSubTitle();
        navSubTitleMedium.setSubTitleText("Medium");
        navSubTitleMedium.setSubMenuId(TASK_PRIORITY_MEDIUM);
        navSubTitleMedium.setDrawableId(R.drawable.ic_category_item);
        mNavSubTitleFilterList.add(navSubTitleMedium);
        NavSubTitle navSubTitleHigh = new NavSubTitle();
        navSubTitleHigh.setSubTitleText("High");
        navSubTitleHigh.setSubMenuId(TASK_PRIORITY_HIGH);
        navSubTitleHigh.setDrawableId(R.drawable.ic_category_item);
        mNavSubTitleFilterList.add(navSubTitleHigh);
    }

    public void prepareAssigneeFilter() {
        AlertDialog.Builder builderSingle = new AlertDialog.Builder(MainActivity.this);
        builderSingle.setTitle("View task assigned to");
        ListView listViewAssignee = (ListView) LayoutInflater.from(this)
                .inflate(R.layout.layout_assignee_list, null);
        ArrayAdapter<Assignee> arrayAdapter = new ArrayAdapter<>(MainActivity.this
                , android.R.layout.simple_list_item_single_choice, mAssigneeList);
        listViewAssignee.setAdapter(arrayAdapter);
        builderSingle.setView(listViewAssignee);
        mAlertDialog = builderSingle.create();
        int size = mAssigneeList.size();
        if (size > 0) {
            int index = checkLastSelectedItemInList(mAssigneeList);
            prevSelectedContactId = index == size - 1 ? 0 : prevSelectedContactId;
            listViewAssignee.setItemChecked(index, true);
        }
        listViewAssignee.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Assignee assignee = mAssigneeList.get(position);
                prevSelectedContactId = assignee.getContactId();
                mAlertDialog.dismiss();
                performAssigneeFilterOnVisibleFragment(prevSelectedContactId);
            }
        });
        mAlertDialog.show();
    }

    public int checkLastSelectedItemInList(List<Assignee> assigneeList) {
        for (Assignee assignee : assigneeList) {
            if (prevSelectedContactId == assignee.getContactId())
                return assigneeList.indexOf(assignee);
        }
        return assigneeList.size() - 1;
    }

    public void performAssigneeFilterOnVisibleFragment(int contactId) {
        FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
        switch (mLastSelectedFragment) {
            case "All":
                fragmentTransaction.replace(R.id.frame_layout
                        , AllTaskFragment.newInstance(contactId), mLastSelectedFragment);
                fragmentTransaction.commit();
                break;
            case "Today":
                fragmentTransaction.replace(R.id.frame_layout
                        , TodayTaskFragment.newInstance(contactId), mLastSelectedFragment);
                fragmentTransaction.commit();
                break;
        }
    }

    public void setDefaultFragment() {
        //Manually displaying the first fragment - one time only
        FragmentTransaction transaction = mFragmentManager.beginTransaction();
        transaction.replace(R.id.frame_layout, new TodayTaskFragment(), "Today");
        transaction.commit();
        mLastSelectedFragment = "Today";
        getSupportActionBar().setTitle("Today");
    }

    //This method will be called when the user will tap on allow or deny
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions
            , @NonNull int[] grantResults) {

        //Checking the request code of our request
        int STORAGE_PERMISSION_CODE = 1;

        if (requestCode == STORAGE_PERMISSION_CODE) {

            //If permission is granted
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if (!Common.isAppDbDirectoryExists()) {
                    List<String> preloadTaskCategoryList = Arrays.asList("Personal", "Society"
                            , "Office", "Any other");
                    mMainPresenter.loadAppCreatedCategoryList(preloadTaskCategoryList);
                    mMainPresenter.getCategoryList();
                }
                startAlarm();
            } else {
                //Displaying another toast if permission is not granted
                Toast.makeText(this, "Oops you just denied the permission", Toast.LENGTH_LONG)
                        .show();
            }
        }
    }

    @Override
    public Context getContext() {
        return MainActivity.this;
    }

    @Override
    public void openDefaultTodayFragment() {
        setDefaultFragment();
    }

    @Override
    public void closeMainActivity() {
        this.finish();
    }

    @Override
    public void categoryList(List<DropDownContent> dropDownContents) {
        prepareListData();
        for (DropDownContent dropDownContent : dropDownContents) {
            NavSubTitle navSubTitle = new NavSubTitle();
            navSubTitle.setSubTitleText(dropDownContent.getName());
            navSubTitle.setDrawableId(R.drawable.ic_category_item);
            navSubTitle.setSubMenuId(dropDownContent.getCategoryId());
            navSubTitle.setTaskCount(dropDownContent.getTaskCount());
            mNavSubTitleCategoryList.add(navSubTitle);
        }
        mNavMenuMap.put(mNavTitleList.get(3).getTitleText(), mNavSubTitleCategoryList);
        navExpandableListAdapter.notifyDataSetChanged();
    }

    @Override
    public void clearNavMenuDataList() {
        mNavTitleList.clear();
        mNavSubTitleCategoryList.clear();
        mNavMenuMap.clear();
    }

    @Override
    public void setTodayTaskCount(int taskCount) {
        //TODO update query to get actual count of task in the today list
        navTodayTaskCount = taskCount;
    }

    @Override
    public void setTaskCompleteCount(int taskCount) {
        navTaskCompleteCount = taskCount;
    }

    @Override
    public void setAssgineeList(List<Assignee> assigneeList) {
        mAssigneeList = assigneeList;
    }

    @Override
    protected void onResume() {
        super.onResume();
        NotificationManager notificationManager
                = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancelAll();

        mMainPresenter.attachView(this);
        if (mMainPresenter.isReadStorageAllowed()) {
            if (Common.isAppDbDirectoryExists()) {
                isActivityResumed = true;
                mMainPresenter.getAssigneeList();
                showPreviousFragment();
                mMainPresenter.getCategoryList();
            } else {
                List<String> preloadTaskCategoryList = Arrays.asList("Personal", "Society"
                        , "Office", "Any other");
                mMainPresenter.loadAppCreatedCategoryList(preloadTaskCategoryList);
                isActivityResumed = true;
                showPreviousFragment();
                mMainPresenter.getCategoryList();
            }
            /*//If permission is already having then showing the toast
            Toast.makeText(this,"You already have the permission",Toast.LENGTH_LONG).show();*/
            //Existing the method with return
        } else {
            //If the app has not the permission then asking for the permission
            mMainPresenter.requestStoragePermission();
        }
    }

    @Override
    public void onClick(View v) {
        if (!mLastSelectedFragment.isEmpty() && !mLastSelectedFragment
                .equalsIgnoreCase("ManageCategory")) {
            Intent intent = new Intent(this, AddEditTaskActivity.class);
            intent.putExtra(FRAGMENT_MAIN_FRAGMENT, mLastSelectedFragment);
            intent.putExtra(FRAGMENT_CATEGORY_FRAGMENT_ID, mLastSelectedCategoryFragmentId);
            intent.putExtra(FRAGMENT_CATEGORY_FRAGMENT_TITLE, mLastSelectedCategoryFragmentTitle);
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(MainActivity.this,
                        Pair.create(v, "fab_transform"));
                startActivity(intent, options.toBundle());
            } else {
                startActivity(intent);
            }
        }
    }

    @Override
    public boolean onChildClick(ExpandableListView parent, View v, int groupPosition
            , int childPosition, long id) {
        mAddEditTask.setOnClickListener(MainActivity.this);
        mSearchItem.setVisible(true);
        mExportToExcelItem.setVisible(true);
        mFilterAssignee.setVisible(false);
        NavSubTitle navSubTitle = mNavMenuMap.get(mNavTitleList.get(groupPosition).getTitleText())
                .get(childPosition);
        String titleText = mNavTitleList.get(groupPosition).getTitleText();
        if (titleText.equalsIgnoreCase("Category")) {
            int categoryId = navSubTitle.getSubMenuId();
            CategoryFragment categoryFragment = (CategoryFragment) mFragmentManager
                    .findFragmentByTag("Category");
            if (categoryFragment != null && categoryFragment.isAdded()
                    && categoryFragment.isVisible()) {
                categoryFragment.setCategory(categoryId);
                mLastSelectedFragment = "Category";
                mLastSelectedCategoryFragmentId = categoryId;
                mLastSelectedCategoryFragmentTitle = navSubTitle.getSubTitleText();
                getSupportActionBar().setTitle(mLastSelectedCategoryFragmentTitle);
            } else {
                FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.frame_layout
                        , CategoryFragment.newInstance(categoryId), "Category");
                fragmentTransaction.commit();
                mLastSelectedFragment = "Category";
                mLastSelectedCategoryFragmentId = categoryId;
                mLastSelectedCategoryFragmentTitle = navSubTitle.getSubTitleText();
                getSupportActionBar().setTitle(mLastSelectedCategoryFragmentTitle);
            }
        } else {
            int filterId = navSubTitle.getSubMenuId();
            TaskFilterFragment taskFilterFragment = (TaskFilterFragment) mFragmentManager
                    .findFragmentByTag("TaskFilter");
            if (taskFilterFragment != null && taskFilterFragment.isAdded()
                    && taskFilterFragment.isVisible()) {
                taskFilterFragment.setFilter(filterId);
                mLastSelectedFragment = "TaskFilter";
                mLastSelectedCategoryFragmentId = filterId;
                mLastSelectedCategoryFragmentTitle = navSubTitle.getSubTitleText();
                getSupportActionBar().setTitle(mLastSelectedCategoryFragmentTitle);
            } else {
                FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.frame_layout
                        , TaskFilterFragment.newInstance(filterId), "TaskFilter");
                fragmentTransaction.commit();
                mLastSelectedFragment = "TaskFilter";
                mLastSelectedCategoryFragmentId = filterId;
                mLastSelectedCategoryFragmentTitle = navSubTitle.getSubTitleText();
                getSupportActionBar().setTitle(mLastSelectedCategoryFragmentTitle);
            }
        }
        drawerLayout.closeDrawers();
        return false;
    }

    @Override
    public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {

        switch (groupPosition) {
            case 0:
                mSearchItem.setVisible(true);
                mExportToExcelItem.setVisible(false);
                mFilterAssignee.setVisible(false);
                drawerLayout.closeDrawers();
                mSelectedFragment = AllTaskFragment.newInstance(0);
                getSupportActionBar().setTitle("All");
                mLastSelectedFragment = "All";
                mLastSelectedCategoryFragmentTitle = "";
                break;
            case 1:
                mSearchItem.setVisible(true);
                mExportToExcelItem.setVisible(true);
                mFilterAssignee.setVisible(true);
                drawerLayout.closeDrawers();
                mSelectedFragment = TodayTaskFragment.newInstance(0);
                getSupportActionBar().setTitle("Today");
                mLastSelectedFragment = "Today";
                mLastSelectedCategoryFragmentTitle = "";
                break;
            case 2:
                mSearchItem.setVisible(false);
                mExportToExcelItem.setVisible(false);
                mFilterAssignee.setVisible(false);
                drawerLayout.closeDrawers();
                mSelectedFragment = TaskCompleteFragment.newInstance("");
                getSupportActionBar().setTitle("Complete");
                mLastSelectedFragment = "Complete";
                mLastSelectedCategoryFragmentTitle = "";
                break;
            case 3:
                if (parent.isGroupExpanded(groupPosition)) {
                    parent.collapseGroup(groupPosition);
                    isGroupExpanded = false;
                } else {
                    parent.expandGroup(groupPosition);
                    parent.collapseGroup(5);
                    isGroupExpanded = true;
                    navGroupExpandPosition = groupPosition;
                }
                break;
            case 4:
                mExportToExcelItem.setVisible(false);
                mSearchItem.setVisible(false);
                mFilterAssignee.setVisible(false);
                drawerLayout.closeDrawers();
                mSelectedFragment = new ManageCategoryFragment();
                getSupportActionBar().setTitle("Manage Category");
                mLastSelectedFragment = "ManageCategory";
                mLastSelectedCategoryFragmentTitle = "";
                break;
            case 5:
                if (parent.isGroupExpanded(groupPosition)) {
                    parent.collapseGroup(groupPosition);
                    isGroupExpanded = false;
                } else {
                    parent.expandGroup(groupPosition);
                    parent.collapseGroup(3);
                    isGroupExpanded = true;
                    navGroupExpandPosition = groupPosition;
                }
                break;
        }

        if (mSelectedFragment != null) {
            mAddEditTask.setOnClickListener(MainActivity.this);
            FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.frame_layout, mSelectedFragment, mLastSelectedFragment);
            fragmentTransaction.commit();
        }

        return true;
    }

    @Override
    public void onGroupCollapse(int groupPosition) {

    }

    @Override
    public void onGroupExpand(int groupPosition) {

    }

    public void showPreviousFragment() {
        FragmentTransaction transaction = mFragmentManager.beginTransaction();
        switch (mLastSelectedFragment) {
            case "All":
                transaction.replace(R.id.frame_layout
                        , AllTaskFragment.newInstance(prevSelectedContactId), "All");
                transaction.commit();
                getSupportActionBar().setTitle("All");
                isActivityResumed = false;
                mLastSelectedCategoryFragmentTitle = "";
                if (mExportToExcelItem != null)
                    mExportToExcelItem.setVisible(false);
                if (mSearchItem != null)
                    mSearchItem.setVisible(true);
                if (mFilterAssignee != null)
                    mFilterAssignee.setVisible(false);
                break;
            case "Today":
                transaction.replace(R.id.frame_layout
                        , TodayTaskFragment.newInstance(prevSelectedContactId), "Today");
                transaction.commit();
                getSupportActionBar().setTitle("Today");
                isActivityResumed = false;
                mLastSelectedCategoryFragmentTitle = "";
                if (mExportToExcelItem != null)
                    mExportToExcelItem.setVisible(true);
                if (mSearchItem != null)
                    mSearchItem.setVisible(true);
                if (mFilterAssignee != null)
                    mFilterAssignee.setVisible(true);
                break;
            case "Complete":
                transaction.replace(R.id.frame_layout
                        , TaskCompleteFragment.newInstance(""));
                transaction.commit();
                getSupportActionBar().setTitle("Complete");
                isActivityResumed = false;
                mLastSelectedCategoryFragmentTitle = "";
                if (mExportToExcelItem != null)
                    mExportToExcelItem.setVisible(false);
                if (mSearchItem != null)
                    mSearchItem.setVisible(false);
                if (mFilterAssignee != null)
                    mFilterAssignee.setVisible(false);
                break;
            case "Category":
                transaction
                        .replace(R.id.frame_layout
                                , CategoryFragment.newInstance(mLastSelectedCategoryFragmentId)
                                , "Category");
                transaction.commit();
                getSupportActionBar().setTitle(mLastSelectedCategoryFragmentTitle);
                isActivityResumed = false;
                if (mExportToExcelItem != null)
                    mExportToExcelItem.setVisible(true);
                if (mSearchItem != null)
                    mSearchItem.setVisible(true);
                if (mFilterAssignee != null)
                    mFilterAssignee.setVisible(false);
                break;
            case "TaskFilter":
                transaction
                        .replace(R.id.frame_layout
                                , TaskFilterFragment.newInstance(mLastSelectedCategoryFragmentId)
                                , "TaskFilter");
                transaction.commit();
                getSupportActionBar().setTitle(mLastSelectedCategoryFragmentTitle);
                isActivityResumed = false;
                if (mExportToExcelItem != null)
                    mExportToExcelItem.setVisible(true);
                if (mSearchItem != null)
                    mSearchItem.setVisible(true);
                if (mFilterAssignee != null)
                    mFilterAssignee.setVisible(false);
                break;
            case "Search":
                if (isActivityResumed && !isMenuItemCollapsed) {
                    Bundle bundle = new Bundle();
                    bundle.putString(ARG_COLUMN_QUERY, mLastSearchedQuery);
                    bundle.putString(ARG_FRAG_TAG, navigateBackToPrevFragment);
                    bundle.putString(ARG_CATEGORY_FRAG_TAG, navigateBackToPrevCategoryTag);
                    bundle.putInt(ARG_CATEGORY_FRAG_ID, navigateBackToPrevCategoryId);
                    transaction
                            .replace(R.id.frame_layout
                                    , SearchFragment.newInstance(bundle)
                                    , "Search");
                    transaction.commit();
                    mLastSelectedCategoryFragmentTitle = "";
                } else {
                    mLastSelectedFragment = navigateBackToPrevFragment;
                    mLastSelectedCategoryFragmentTitle = navigateBackToPrevCategoryTag;
                    mLastSelectedCategoryFragmentId = navigateBackToPrevCategoryId;
                    showPreviousFragment();
                }
                break;
        }
    }

    public void prepareSelectedFragmentDataForExcelExport() {
        List<Task> taskList;
        switch (mLastSelectedFragment) {
            case "Today":
                TodayTaskFragment todayTaskFragment = (TodayTaskFragment) mFragmentManager
                        .findFragmentByTag("Today");
                taskList = todayTaskFragment.getTaskList();
                int taskTodaySize = taskList.size();
                if (taskTodaySize > 0) {
                    generateExcel(taskList, "Today's Task");
                } else {
                    Toast.makeText(this, "No task to export in excel", Toast.LENGTH_SHORT).show();
                }
                taskList.clear();
                break;
            case "Category":
                CategoryFragment categoryFragment = (CategoryFragment) mFragmentManager
                        .findFragmentByTag("Category");
                taskList = categoryFragment.getTaskList();
                int taskCategorySize = taskList.size();
                if (taskCategorySize > 0) {
                    generateExcel(taskList, mLastSelectedCategoryFragmentTitle + " Task");
                } else {
                    Toast.makeText(this, "No task to export in excel", Toast.LENGTH_SHORT).show();
                }
                taskList.clear();
                break;
            case "TaskFilter":
                TaskFilterFragment taskFilterFragment = (TaskFilterFragment) mFragmentManager
                        .findFragmentByTag("TaskFilter");
                taskList = taskFilterFragment.getTaskList();
                int taskFilterSize = taskList.size();
                if (taskFilterSize > 0) {
                    generateExcel(taskList, mLastSelectedCategoryFragmentTitle + " Task");
                } else {
                    Toast.makeText(this, "No task to export in excel", Toast.LENGTH_SHORT).show();
                }
                taskList.clear();
                break;
        }
    }

    public void generateExcel(List<Task> taskList, String sheetTitle) {
        int size = taskList.size();
        WritableSheet writableSheet = createTaskExcelHeader(sheetTitle);
        for (int i = 0; i < size; i++) {
            Task task = taskList.get(i);
            if (task.getViewType() != 0) {
                try {
                    boolean isLastTask = (i == size - 1);
                    String categoryName = getCategoryName(task.getCategoryId());
                    Common.generatedTaskExcel(writableSheet
                            , task, i + 1, categoryName, isLastTask);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        Toast.makeText(this, "Task Exported to /Documents/MyTaskExcelData directory"
                , Toast.LENGTH_LONG).show();
    }

    public String getCategoryName(int categoryId) {
        for (NavSubTitle navSubTitle : mNavSubTitleCategoryList) {
            if (navSubTitle.getSubMenuId() == categoryId)
                return navSubTitle.getSubTitleText();
        }
        return "";
    }

    public void startAlarm() {
        /* Retrieve a PendingIntent that will perform a broadcast */
        Intent alarmIntent = new Intent(MainActivity.this, AlarmReceiver.class);
        PendingIntent pendingIntent = PendingIntent
                .getBroadcast(MainActivity.this, 0, alarmIntent, 0);

        AlarmManager manager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        int interval = 1000 * 60 * 60;
        manager.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis()
                , interval, pendingIntent);
    }

    public interface ClickListener {
        void onClick(View view, int position);

        void onLongClick(View view, int position);
    }

}
