package com.bitpyx.mytask.category.manage;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.bitpyx.mytask.R;
import com.bitpyx.mytask.adapter.ManageCategoryAdapter;
import com.bitpyx.mytask.category.manage.presenters.IMCategoryPresenter;
import com.bitpyx.mytask.category.manage.presenters.MCategoryPresenter;
import com.bitpyx.mytask.category.manage.view.IMCategoryView;
import com.bitpyx.mytask.data.model.DropDownContent;
import com.bitpyx.mytask.main.MainActivity;
import com.bitpyx.mytask.utility.RecyclerTouchListener;

import java.util.List;

public class ManageCategoryFragment extends Fragment implements IMCategoryView
        , IMCategoryView.OnCategoryAdded, View.OnClickListener, DialogInterface.OnClickListener {

    private boolean editCategory = false;

    private EditText mEditTextCategory;
    private RecyclerView mRecyclerView;

    private List<DropDownContent> mCategoryList;
    private IMCategoryPresenter<IMCategoryView> mCategoryPresenter;

    private ManageCategoryAdapter manageCategoryAdapter;
    private DropDownContent selectedCategory;

    public ManageCategoryFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mCategoryPresenter = new MCategoryPresenter();
        mCategoryPresenter.attachView(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_manage_category, container, false);

        CoordinatorLayout coordinatorLayout = (CoordinatorLayout) container.getParent();

        FloatingActionButton actionButton = (FloatingActionButton) coordinatorLayout.findViewById(R.id.fab_add_edit_task);
        actionButton.setOnClickListener(this);

        mRecyclerView = (RecyclerView) view.findViewById(R.id.category_recycler_view);

        // LinearLayoutManager is used here, this will layout the elements in a similar fashion
        // to the way ListView would layout elements. The RecyclerView.LayoutManager defines how
        // elements are laid out.
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);

        DividerItemDecoration mDividerItemDecoration =
                new DividerItemDecoration(mRecyclerView.getContext(),
                        DividerItemDecoration.VERTICAL);
        mRecyclerView.addItemDecoration(mDividerItemDecoration);

        mRecyclerView.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), mRecyclerView, new MainActivity.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                selectedCategory = mCategoryList.get(position);
                ImageButton ibCategoryEdit = (ImageButton) view.findViewById(R.id.ib_manage_category_edit);
                ImageButton ibCategoryRemove = (ImageButton) view.findViewById(R.id.ib_manage_category_remove);
                ibCategoryEdit.setOnClickListener(ManageCategoryFragment.this);
                ibCategoryRemove.setOnClickListener(ManageCategoryFragment.this);
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        mCategoryPresenter.fetchTaskCategories();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void showCategoryList(List<DropDownContent> categoryList) {
        mCategoryList = categoryList;
        manageCategoryAdapter = new ManageCategoryAdapter(mCategoryList);
        mRecyclerView.setAdapter(manageCategoryAdapter);
    }

    @Override
    public void showToast(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void checkTaskCountForTheCategory(boolean hasTask) {
        showCategoryDeleteDialog(hasTask);
    }

    @Override
    public void onClick(View v) {
        int viewId = v.getId();
        switch (viewId) {
            case R.id.fab_add_edit_task:
                editCategory = false;
                showAddCategoryDialog();
                break;
            case R.id.ib_manage_category_edit:
                editCategory = true;
                showAddCategoryDialog();
                break;
            case R.id.ib_manage_category_remove:
                mCategoryPresenter.getTaskCountForTheCategory(selectedCategory.getCategoryId());
                break;
        }
    }

    public void showAddCategoryDialog() {

        LayoutInflater layoutInflaterAndroid = LayoutInflater.from(getActivity());
        View view = layoutInflaterAndroid.inflate(R.layout.layout_input_category_dialog_box, null);

        mEditTextCategory = (EditText) view.findViewById(R.id.editText_category);

        AlertDialog.Builder alertDialogBuilderUserInput = new AlertDialog.Builder(getActivity());
        alertDialogBuilderUserInput.setCancelable(false);

        if (editCategory) {
            mEditTextCategory.setText(selectedCategory.getName());
            alertDialogBuilderUserInput.setPositiveButton("Update", ManageCategoryFragment.this);
        } else {
            alertDialogBuilderUserInput.setPositiveButton("Add", ManageCategoryFragment.this);
        }

        alertDialogBuilderUserInput.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogBox, int id) {
                        dialogBox.cancel();
                    }
                });

        alertDialogBuilderUserInput.setView(view);
        alertDialogBuilderUserInput.show();

    }

    public void showCategoryDeleteDialog(final boolean hasTask) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        if (hasTask) {
            builder.setMessage("Cannot be removed, found tasks created with this category");
        } else {
            builder.setMessage("Are you sure, you want to delete this category?");
            String negativeText = getString(android.R.string.cancel);
            builder.setNegativeButton(negativeText,
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // negative button logic
                        }
                    });
        }

        builder.setCancelable(false);
        String positiveText = getString(android.R.string.ok);
        builder.setPositiveButton(positiveText,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int position) {
                        if (!hasTask) {
                            mCategoryPresenter.removeCategory(selectedCategory
                                    , ManageCategoryFragment.this);
                        }
                    }
                });

        AlertDialog dialog = builder.create();
        // display dialog
        dialog.show();

    }

    @Override
    public void onSuccess() {
        mCategoryPresenter.fetchTaskCategories();
        manageCategoryAdapter.notifyDataSetChanged();
    }

    @Override
    public void onFailure(String message) {
        Toast.makeText(getActivity(), message
                , Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        String categoryName = String.valueOf(mEditTextCategory.getText());
        mCategoryPresenter.saveNewCategory(categoryName.trim(), editCategory
                , selectedCategory, ManageCategoryFragment.this);
    }
}
