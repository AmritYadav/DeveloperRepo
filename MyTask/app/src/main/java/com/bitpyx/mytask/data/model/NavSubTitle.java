package com.bitpyx.mytask.data.model;

/**
 * @author Amrit Yadav
 * @since 6/23/2017
 */
public class NavSubTitle {
    private int drawableId, taskCount, subMenuId;
    private String subTitleText;

    public int getDrawableId() {
        return drawableId;
    }

    public void setDrawableId(int drawableId) {
        this.drawableId = drawableId;
    }

    public int getTaskCount() {
        return taskCount;
    }

    public void setTaskCount(int taskCount) {
        this.taskCount = taskCount;
    }

    public int getSubMenuId() {
        return subMenuId;
    }

    public void setSubMenuId(int subMenuId) {
        this.subMenuId = subMenuId;
    }

    public String getSubTitleText() {
        return subTitleText;
    }

    public void setSubTitleText(String subTitleText) {
        this.subTitleText = subTitleText;
    }

    @Override
    public String toString() {
        return "NavSubTitle{" +
                "drawableId=" + drawableId +
                ", taskCount=" + taskCount +
                ", subMenuId=" + subMenuId +
                ", subTitleText='" + subTitleText + '\'' +
                '}';
    }
}
