package com.bitpyx.mytask.utility;

/**
 * @author Amrit Yadav
 * @since 21-05-2017.
 */
public interface IBasePresenter<V> {

    void attachView(V view);

    void detachView();

}