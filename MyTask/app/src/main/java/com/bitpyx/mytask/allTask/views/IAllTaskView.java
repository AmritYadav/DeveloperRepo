package com.bitpyx.mytask.allTask.views;

import com.bitpyx.mytask.data.model.DropDownContent;
import com.bitpyx.mytask.data.model.Task;
import com.bitpyx.mytask.utility.IMvpView;

import java.util.List;

/**
 * @author Amrit Yadav
 * @since 25-06-2017.
 */
public interface IAllTaskView extends IMvpView{

    void setAdapter(List<Task> taskList);

    void categoryList(List<DropDownContent> dropDownContents);
}
