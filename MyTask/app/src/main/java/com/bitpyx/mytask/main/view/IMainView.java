package com.bitpyx.mytask.main.view;

import com.bitpyx.mytask.data.model.Assignee;
import com.bitpyx.mytask.data.model.DropDownContent;
import com.bitpyx.mytask.utility.IMvpView;

import java.util.List;

/**
 * @author Amrit Yadav
 * @since 21-05-2017
 */
public interface IMainView extends IMvpView {

    void openDefaultTodayFragment();

    void closeMainActivity();

    void categoryList(List<DropDownContent> dropDownContents);

    void clearNavMenuDataList();

    void setTodayTaskCount(int taskCount);

    void setTaskCompleteCount(int taskCount);

    void setAssgineeList(List<Assignee> assigneeList);

}
