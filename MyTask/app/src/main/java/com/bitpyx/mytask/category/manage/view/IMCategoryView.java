package com.bitpyx.mytask.category.manage.view;

import com.bitpyx.mytask.data.model.DropDownContent;
import com.bitpyx.mytask.utility.IMvpView;

import java.util.List;

/**
 * @author Amrit Yadav
 * @since 7/9/2017.
 */
public interface IMCategoryView extends IMvpView{
    void showCategoryList(List<DropDownContent> categoryList);
    void showToast(String message);
    interface OnCategoryAdded{
        void onSuccess();
        void onFailure(String message);
    }
    void checkTaskCountForTheCategory(boolean hasTask);
}
