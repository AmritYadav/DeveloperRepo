package com.bitpyx.mytask.filter.presenters;

import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.bitpyx.mytask.R;
import com.bitpyx.mytask.adapter.TaskAdapter;
import com.bitpyx.mytask.data.model.DropDownContent;
import com.bitpyx.mytask.data.model.Task;
import com.bitpyx.mytask.filter.interactors.ITaskFilterInteractor;
import com.bitpyx.mytask.filter.interactors.TaskFilterInteractor;
import com.bitpyx.mytask.filter.view.IFilterView;

import java.util.List;

/**
 * @author Amrit Yadav
 * @since 7/13/2017.
 */
public class TaskFilterPresenter implements ITaskFilterPresenter<IFilterView>{
    private IFilterView mFilterView;
    private ITaskFilterInteractor mTaskFilterInteractor;
    @Override
    public void attachView(IFilterView view) {
        mFilterView=view;
        mTaskFilterInteractor = new TaskFilterInteractor(mFilterView.getContext());
    }

    @Override
    public void fetchTaskListByFilter(int filterId) {
        List<Task> taskList = mTaskFilterInteractor.getTaskByFilter(filterId);
        mFilterView.refreshTaskList(taskList);
    }

    @Override
    public void getCategoryList() {
        List<DropDownContent> dropDownContents = mTaskFilterInteractor.fetchCategoryList();
        mFilterView.categoryList(dropDownContents);
    }

    @Override
    public void addUndoTask(List<Task> undoTask) {
        mTaskFilterInteractor.saveUndoTask(undoTask);
    }

    @Override
    public void removeTask(int taskId) {
        mTaskFilterInteractor.deleteTask(taskId);
    }

    @Override
    public void updateListOnTaskSwipeRemove(List<Task> taskList) {
        mFilterView.refreshTaskList(taskList);
    }

    @Override
    public void selectListItem(View view, Task task) {
        task.setSelected(true);
        view.setBackgroundColor(ContextCompat.getColor(mFilterView.getContext()
                , R.color.listSelectedColor));
    }

    @Override
    public void unSelectListItem(View view, Task task) {
        task.setSelected(false);
        view.setBackgroundColor(ContextCompat.getColor(mFilterView.getContext()
                , R.color.white));
    }

    @Override
    public void onDestroyActionMode(List<Task> mTaskList, List<Integer> mListItemsToUpdate, RecyclerView mRecyclerView) {
        int listSize = mListItemsToUpdate.size();

        for (int i = 0; i < listSize; i++) {
            int position = mListItemsToUpdate.get(i);
            Task task = mTaskList.get(position);
            task.setSelected(false);
        }
        TaskAdapter adapter = (TaskAdapter) mRecyclerView.getAdapter();
        adapter.refreshAdapter(mTaskList);
        mListItemsToUpdate.clear();
    }

    @Override
    public void onActionItemClicked(List<Task> mTaskList, List<Integer> mListItemsToUpdate, RecyclerView mRecyclerView, int filterId) {
        mTaskFilterInteractor.UpdateTasksStatus(mTaskList, mListItemsToUpdate);
        fetchTaskListByFilter(filterId);
    }

    @Override
    public void detachView() {

    }
}
