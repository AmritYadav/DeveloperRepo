package com.bitpyx.mytask.allTask.presenters;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.bitpyx.mytask.data.model.Task;
import com.bitpyx.mytask.utility.IBasePresenter;

import java.util.List;

/**
 * @author Amrit Yadav
 * @since 25-06-2017.
 */
public interface IAllTaskPresenter<V> extends IBasePresenter<V>{

    void fetchAllTaskList();

    void getCategoryList();

    void removeTask(int taskId);

    void addUndoTask(List<Task> undoTask);

    void selectListItem(View view, Task task);

    void unSelectListItem(View view, Task task);

    void onDestroyActionMode(List<Task> mTaskList, List<Integer> mListItemsToRemove, RecyclerView mRecyclerView);

    void onActionItemClicked(List<Task> mTaskList, List<Integer> mListItemsToRemove, RecyclerView mRecyclerView);

    //Called when any task is dragged from its original category to other category
    void changeTaskCategory(Task task);

}
