package com.bitpyx.mytask.allTask.interactors;

import com.bitpyx.mytask.data.model.DropDownContent;
import com.bitpyx.mytask.data.model.Task;

import java.util.List;

/**
 * @author Amrit Yadav
 * @since 25-06-2017.
 */
public interface IAllTaskInteractor {

    List<Task> fetchAllTaskList();

    List<DropDownContent> fetchCategoryList();

    void deleteTask(int taskId);

    void saveUndoTask(List<Task> undoTaskList);

    void updateTasksStatus(List<Task> mTaskList, List<Integer> mListItemsToUpdate);

    void updateTaskCategory(Task task);

}
