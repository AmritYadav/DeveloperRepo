package com.bitpyx.mytask.data.model;

import java.util.List;

/**
 * Created by Amrit Yadav on 6/23/2017.
 */

public class NavTitle {
    private int drawableId, taskCount;
    private String titleText;
    private List<NavSubTitle> mNavSubTitleList;

    public int getDrawableId() {
        return drawableId;
    }

    public void setDrawableId(int drawableId) {
        this.drawableId = drawableId;
    }

    public int getTaskCount() {
        return taskCount;
    }

    public void setTaskCount(int taskCount) {
        this.taskCount = taskCount;
    }

    public String getTitleText() {
        return titleText;
    }

    public void setTitleText(String titleText) {
        this.titleText = titleText;
    }

    public List<NavSubTitle> getmNavSubTitleList() {
        return mNavSubTitleList;
    }

    public void setmNavSubTitleList(List<NavSubTitle> mNavSubTitleList) {
        this.mNavSubTitleList = mNavSubTitleList;
    }

    @Override
    public String toString() {
        return "NavTitle{" +
                "drawableId=" + drawableId +
                ", taskCount=" + taskCount +
                ", titleText='" + titleText + '\'' +
                ", mNavSubTitleList=" + mNavSubTitleList +
                '}';
    }
}
