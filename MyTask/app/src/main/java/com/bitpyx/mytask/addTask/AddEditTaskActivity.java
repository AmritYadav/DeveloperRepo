package com.bitpyx.mytask.addTask;

import android.app.Activity;
import android.app.Dialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bitpyx.mytask.R;
import com.bitpyx.mytask.adapter.DropDownAdapter;
import com.bitpyx.mytask.addTask.presenters.AddEditPresenter;
import com.bitpyx.mytask.addTask.presenters.IAddEditPresenter;
import com.bitpyx.mytask.addTask.views.IAddEditView;
import com.bitpyx.mytask.data.model.Assignee;
import com.bitpyx.mytask.data.model.DropDownContent;
import com.bitpyx.mytask.data.model.Task;
import com.bitpyx.mytask.main.MainActivity;
import com.kunzisoft.switchdatetime.SwitchDateTimeDialogFragment;

import org.joda.time.LocalDateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class AddEditTaskActivity extends AppCompatActivity implements IAddEditView
        , IAddEditView.onTaskSaved
        , IAddEditView.onTaskUpdated
        , View.OnClickListener
        , TextWatcher
        , AdapterView.OnItemClickListener
        , Spinner.OnItemSelectedListener {

    private static final int PICK_CONTACT = 1;
    private static final String EXTRA_MAIN_FRAGMENT = "mainFragment";
    private static final String EXTRA_CATEGORY_FRAGMENT_ID = "categoryFragmentId";
    private static final String EXTRA_CATEGORY_FRAGMENT_TAG = "categoryFragmentTitle";
    private static final String EXTRA_FRAG_TAG = "argFragmentTag";
    private static final String EXTRA_CATEGORY_FRAG_ID = "argCategoryFragId";
    private static final String EXTRA_CATEGORY_FRAG_TAG = "argCategoryFragTag";

    DateTimeFormatter fmt = DateTimeFormat.forPattern("yyyy-MM-dd hh:mm a");
    private String mLastSelectedFragment, mLastSelectedCategoryFragmentTitle, mMainFragTag, mCategoryFragTag;
    private int mLastSelectedCategoryFragmentId, mCategoryFragId, mRepeatFrequency;
    private Spinner mSpinnerTaskRepeatAfter;
    private LinearLayout mTaskRepeatAfterLayout;
    private TextView mTextViewDescription, mTextViewPriority, mTextViewRepeat, mTextViewCategory, mTextViewDueDate, mTextViewAssignedTo, mTextViewTaskRepeatMode;
    private ImageView mImageViewClearAssignee;
    private EditText mEditTextTaskTitle;
    private FloatingActionButton mFabShareTask;
    private AlertDialog mAlertDialog;
    private IAddEditPresenter<IAddEditView> mAddEditPresenter;
    private Task mTask;
    private String textDueDate, mTextTaskMode;
    private String[] repeatFrequencyArray;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_task);

        mAddEditPresenter = new AddEditPresenter();
        mAddEditPresenter.attachView(this);

        initViews();

        Intent intent = getIntent();
        if (intent.hasExtra("Task")) {
            mTask = intent.getParcelableExtra("Task");
            mTextTaskMode = intent.getStringExtra("Type");
            setTaskFields(mTask);
        }

        mLastSelectedFragment = intent.getStringExtra(EXTRA_MAIN_FRAGMENT);
        mLastSelectedCategoryFragmentId = intent.getIntExtra(EXTRA_CATEGORY_FRAGMENT_ID, 0);
        mLastSelectedCategoryFragmentTitle = intent.getStringExtra(EXTRA_CATEGORY_FRAGMENT_TAG);
        mMainFragTag = intent.getStringExtra(EXTRA_FRAG_TAG);
        mCategoryFragTag = intent.getStringExtra(EXTRA_CATEGORY_FRAG_TAG);
        mCategoryFragId = intent.getIntExtra(EXTRA_CATEGORY_FRAG_ID, 0);

        if (mLastSelectedCategoryFragmentTitle != null
                && !mLastSelectedCategoryFragmentTitle.isEmpty()) {
            if (mLastSelectedFragment.equalsIgnoreCase("Category")) {
                mTextViewCategory.setText(mLastSelectedCategoryFragmentTitle);
                mTask.setCategoryId(mLastSelectedCategoryFragmentId);
            }
        }

    }

    public void initViews() {

        Toolbar toolbar = (Toolbar) findViewById(R.id.main_toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        repeatFrequencyArray = getResources().getStringArray(R.array.task_repeat_after_array);

        mTaskRepeatAfterLayout = (LinearLayout) findViewById(R.id.ll_task_repeat_after);

        mTextViewTaskRepeatMode = (TextView) mTaskRepeatAfterLayout
                .findViewById(R.id.tv_repeat_mode);
        mSpinnerTaskRepeatAfter = (Spinner) mTaskRepeatAfterLayout
                .findViewById(R.id.sp_task_repeat_after);
        mSpinnerTaskRepeatAfter.setOnItemSelectedListener(this);

        mTextViewDescription = (TextView) findViewById(R.id.tv_description_details);
        mTextViewPriority = (TextView) findViewById(R.id.tv_priority_name);
        mTextViewRepeat = (TextView) findViewById(R.id.tv_repeat_name);
        mTextViewCategory = (TextView) findViewById(R.id.tv_category_name);
        mTextViewDueDate = (TextView) findViewById(R.id.tv_date);
        mTextViewAssignedTo = (TextView) findViewById(R.id.tv_assigned_to);

        mImageViewClearAssignee = (ImageView) findViewById(R.id.iv_clear_assignee);

        mFabShareTask = (FloatingActionButton) findViewById(R.id.fab_share);

        mTextViewRepeat.setText(R.string.text_task_repeat_default_value);
        mTextViewPriority.setText(R.string.text_task_priority_default_value);
        mTextViewCategory.setText(R.string.text_task_category_default_value);

        textDueDate = fmt.print(LocalDateTime.now());

        mTextViewDueDate.setText(textDueDate);

        mEditTextTaskTitle = (EditText) findViewById(R.id.et_task_title);
        mEditTextTaskTitle.addTextChangedListener(this);

        RelativeLayout description = (RelativeLayout) findViewById(R.id.rl_parent_description);
        RelativeLayout dueDate = (RelativeLayout) findViewById(R.id.rl_parent_due_date);
        RelativeLayout priority = (RelativeLayout) findViewById(R.id.rl_parent_priority);
        RelativeLayout repeat = (RelativeLayout) findViewById(R.id.rl_parent_repeated);
        RelativeLayout assignedTo = (RelativeLayout) findViewById(R.id.rl_parent_assigned_to);

        LinearLayout category = (LinearLayout) findViewById(R.id.layout_category);

        ImageButton addCategory = (ImageButton) findViewById(R.id.img_add_category);

        category.setOnClickListener(this);
        addCategory.setOnClickListener(this);
        description.setOnClickListener(this);
        dueDate.setOnClickListener(this);
        priority.setOnClickListener(this);
        repeat.setOnClickListener(this);
        assignedTo.setOnClickListener(this);

        mImageViewClearAssignee.setOnClickListener(this);

        mFabShareTask.setOnClickListener(this);

        mTask = new Task();
        mTask.setPriority("High");
        mTask.setRepeat("One Time");
        mTask.setRepeatFrequency(0);
        mTask.setCategoryId(1);
        mTask.setDate(textDueDate);
        mTask.setStatus("Pending");

        mTextViewDescription.setHint(getResources().getString(R.string.tv_task_description_hint));
        mAddEditPresenter.checkPriorityColorCode(mTask.getPriority());
        mTaskRepeatAfterLayout.setVisibility(View.GONE);

        mRepeatFrequency = mTask.getRepeatFrequency();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_new_task, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int itemId = item.getItemId();

        switch (itemId) {
            case android.R.id.home:
                supportFinishAfterTransition();
                onBackPressed();
                return true;
            case R.id.item_new_task_done:
                if (mTextTaskMode != null && !mTextTaskMode.isEmpty()) {
                    mAddEditPresenter.updateTask(mTask, this);
                } else {
                    LocalDateTime localDateTime = LocalDateTime.now();
                    mTask.setTaskCreationDate(fmt.print(localDateTime));
                    mAddEditPresenter.saveTask(mTask, this);
                }
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void showDialog() {

        LayoutInflater layoutInflaterAndroid = LayoutInflater.from(this);
        View mView = layoutInflaterAndroid.inflate(R.layout.layout_input_dialog_box, null);
        AlertDialog.Builder alertDialogBuilderUserInput = new AlertDialog.Builder(this);
        alertDialogBuilderUserInput.setView(mView);

        final EditText userInputDialogEditText = (EditText) mView.findViewById(R.id.editText);
        if (mTask.getDescription() != null)
            if (!mTask.getDescription().trim().isEmpty()) {
                userInputDialogEditText.setText(mTask.getDescription());
            } else {
                userInputDialogEditText.setHint(getResources()
                        .getString(R.string.tv_task_description_hint));
            }

        alertDialogBuilderUserInput
                .setCancelable(false)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogBox, int id) {

                        String description = String.valueOf(userInputDialogEditText.getText());

                        if (!description.trim().isEmpty()) {
                            mTextViewDescription.setText(userInputDialogEditText.getText());
                            mTask.setDescription(String.valueOf(userInputDialogEditText.getText()));
                        } else {
                            mTextViewDescription.setText(
                                    getResources().getString(R.string.tv_task_description_hint));
                            mTask.setDescription("");
                        }
                    }
                })

                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialogBox, int id) {
                                dialogBox.cancel();
                            }
                        });

        AlertDialog alertDialogAndroid = alertDialogBuilderUserInput.create();
        alertDialogAndroid.getWindow().setSoftInputMode(WindowManager
                .LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        alertDialogAndroid.show();

    }

    public void showDateTImePicker() throws ParseException {

        SwitchDateTimeDialogFragment dateTimeFragment = SwitchDateTimeDialogFragment.newInstance(
                "Select Date",
                "OK",
                "Cancel"
        );

        // Assign values
        dateTimeFragment.startAtCalendarView();
        dateTimeFragment.set24HoursMode(false);

        if (mTask.getDate() != null) {
            Date date = new SimpleDateFormat("yyyy-MM-dd hh:mm a", Locale.US).parse(mTask.getDate());
            dateTimeFragment.setDefaultDateTime(date);
        }

        // Set listener
        dateTimeFragment.setOnButtonClickListener(
                new SwitchDateTimeDialogFragment.OnButtonClickListener() {
                    @Override
                    public void onPositiveButtonClick(Date date) {
                        textDueDate = fmt.print(new LocalDateTime(date));
                        mTask.setDate(textDueDate);
                        mTextViewDueDate.setText(textDueDate);
                    }

                    @Override
                    public void onNegativeButtonClick(Date date) {
                    }
                });

// Show
        dateTimeFragment.show(getSupportFragmentManager(), "dialog_time");

    }

    public void showDropDownMenu(String title, List<DropDownContent> dropDownContents) {

        LayoutInflater layoutInflaterAndroid = LayoutInflater.from(this);
        View mView = layoutInflaterAndroid.inflate(R.layout.layout_dropdown_menu, null);

        final AlertDialog.Builder alertDialogBuilderUserInput = new AlertDialog.Builder(this);

        TextView dialogTitle = (TextView) mView.findViewById(R.id.text_dropdown_title);
        dialogTitle.setText(title);

        ListView mList = (ListView) mView.findViewById(R.id.list);
        DropDownAdapter dropDownAdapter = new DropDownAdapter(this, dropDownContents);
        mList.setAdapter(dropDownAdapter);

        alertDialogBuilderUserInput.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogBox, int id) {
                        dialogBox.cancel();
                    }
                });

        mList.setOnItemClickListener(this);

        alertDialogBuilderUserInput.setView(mView);
        mAlertDialog = alertDialogBuilderUserInput.show();

    }

    public void showAddCategoryDialog() {

        LayoutInflater layoutInflaterAndroid = LayoutInflater.from(this);

        View view = layoutInflaterAndroid.inflate(R.layout.layout_input_category_dialog_box, null);

        final EditText editTextCategory = (EditText) view.findViewById(R.id.editText_category);

        AlertDialog.Builder alertDialogBuilderUserInput = new AlertDialog.Builder(this);
        alertDialogBuilderUserInput.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String categoryName = String.valueOf(editTextCategory.getText());
                if (categoryName.trim().isEmpty())
                    Toast.makeText(AddEditTaskActivity.this, "Category Name should not be empty"
                            , Toast.LENGTH_SHORT).show();
                else
                    mAddEditPresenter.saveNewCategory(categoryName);
            }
        });

        alertDialogBuilderUserInput.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogBox, int id) {
                        dialogBox.cancel();
                    }
                });

        alertDialogBuilderUserInput.setView(view);
        alertDialogBuilderUserInput.show();

    }

    public void openShareBottomSheet() {
        View view = getLayoutInflater().inflate(R.layout.content_bottom_sheet, null);

        TextView textViewSms = (TextView) view.findViewById(R.id.action_share_via_sms);
        TextView textViewWhatsApp = (TextView) view.findViewById(R.id.action_share_via_whatsapp);
        TextView textViewMail = (TextView) view.findViewById(R.id.action_share_via_mail);

        textViewSms.setOnClickListener(this);
        textViewWhatsApp.setOnClickListener(this);
        textViewMail.setOnClickListener(this);

        Dialog mBottomSheetDialog = new Dialog(this, R.style.MaterialDialogSheet);
        mBottomSheetDialog.setContentView(view);
        mBottomSheetDialog.setCancelable(true);
        mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        mBottomSheetDialog.getWindow().setGravity(Gravity.BOTTOM);
        mBottomSheetDialog.show();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        DropDownContent dropDownContent = (DropDownContent) parent.getItemAtPosition(position);

        String type = dropDownContent.getType();

        switch (type) {
            case "P":
                String priority = dropDownContent.getName();

                if (priority.equals("Low")) {
                    mTask.setPriority("Normal");
                    mTextViewPriority.setText(mTask.getPriority());
                } else {
                    mTextViewPriority.setText(priority);
                    mTask.setPriority(priority);
                }
                mAddEditPresenter.checkPriorityColorCode(mTask.getPriority());
                break;
            case "R":
                String repeatMode = dropDownContent.getName();
                mTextViewRepeat.setText(repeatMode);
                mTask.setRepeat(repeatMode);
                if (!repeatMode.equalsIgnoreCase("One Time")) {
                    mTaskRepeatAfterLayout.setVisibility(View.VISIBLE);
                    int length = repeatMode.length();
                    String repeatText = repeatMode.substring(0, length - 2) + "(s)";
                    mTextViewTaskRepeatMode.setText(repeatText);
                } else {
                    mTaskRepeatAfterLayout.setVisibility(View.GONE);
                    mRepeatFrequency = 0;
                }
                mTask.setRepeatFrequency(mRepeatFrequency);
                break;
            case "C":
                mTextViewCategory.setText(dropDownContent.getName());
                mTask.setCategoryId(dropDownContent.getCategoryId());
                break;

        }
        mAlertDialog.dismiss();
    }

    @Override
    public Context getContext() {
        return AddEditTaskActivity.this;
    }

    @Override
    public void showCategoryList(List<DropDownContent> categoryList) {
        showDropDownMenu("Category", categoryList);
    }

    @Override
    public void showPriorityList(List<DropDownContent> priorityList) {
        showDropDownMenu("Priority", priorityList);
    }

    @Override
    public void showRepeatedList(List<DropDownContent> repeatedList) {
        showDropDownMenu("Repeat", repeatedList);
    }

    @Override
    public void showToast(String message, int toastDuration) {
        Toast.makeText(this, message, toastDuration).show();
    }

    @Override
    public void categoryName(String categoryName) {
        mTextViewCategory.setText(categoryName);
    }

    @Override
    public void setPriorityTextColor(int priorityTextColor) {
        mTextViewPriority.setTextColor(priorityTextColor);
    }

    @Override
    public void onClick(View v) {
        int viewId = v.getId();
        switch (viewId) {
            case R.id.layout_category:
                mAddEditPresenter.fetchTaskCategories();
                break;
            case R.id.img_add_category:
                showAddCategoryDialog();
                break;
            case R.id.rl_parent_description:
                showDialog();
                break;
            case R.id.rl_parent_due_date:
                try {
                    showDateTImePicker();
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                break;
            case R.id.rl_parent_priority:
                mAddEditPresenter.fetchTaskPriorities();
                break;
            case R.id.rl_parent_repeated:
                mAddEditPresenter.fetchTaskRepeated();
                break;
            case R.id.rl_parent_assigned_to:
                Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
                //Alternate way to do show contacts same as above
//                intent.setType(ContactsContract.Contacts.CONTENT_TYPE);
                startActivityForResult(intent, PICK_CONTACT);
                break;
            case R.id.iv_clear_assignee:
                mTextViewAssignedTo.setText("");
                mTextViewAssignedTo.setTextSize(14);
                Assignee assignee = new Assignee();
                assignee.setRowId(0);
                assignee.setContactId(0);
                assignee.setAssigneeName("");
                mTask.setAssignee(assignee);
                mImageViewClearAssignee.setVisibility(View.GONE);
                mFabShareTask.setVisibility(View.GONE);
                break;
            case R.id.fab_share:
                openShareBottomSheet();
                break;
            case R.id.action_share_via_sms:
                Assignee assignee1 = mTask.getAssignee();
                String contactNumber1 = getContactNumber(assignee1.getContactId());
                String message1 = prepareMessageToShare(0);
                Intent smsIntent = new Intent(Intent.ACTION_SENDTO);
                smsIntent.putExtra("sms_body", message1);
                smsIntent.addCategory(Intent.CATEGORY_DEFAULT);
                smsIntent.setType("vnd.android-dir/mms-sms");
                smsIntent.setData(Uri.parse("sms:" + contactNumber1));
                startActivity(smsIntent);
                break;
            case R.id.action_share_via_whatsapp:
                Assignee assignee2 = mTask.getAssignee();
                String contactNumber2 = getContactNumber(assignee2.getContactId());
                String message2 = prepareMessageToShare(0);
                Intent sendIntent = new Intent("android.intent.action.MAIN");
                sendIntent.putExtra("jid", contactNumber2 + "@s.whatsapp.net");
                sendIntent.putExtra(Intent.EXTRA_TEXT, message2);
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.setPackage("com.whatsapp");
                sendIntent.setType("text/plain");
                startActivity(sendIntent);
                break;
            case R.id.action_share_via_mail:
                Assignee assignee3 = mTask.getAssignee();
                String assigneeEmailId = getAssigneeEmailId(assignee3.getContactId());
                String message3 = prepareMessageToShare(1);
                Intent emailIntent = new Intent(Intent.ACTION_SEND, Uri.parse("mailto:"));
                emailIntent.setType("message/rfc822");
                String[] recipients = new String[]{assigneeEmailId};
                emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, recipients);
                emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Assigned Task");
                emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, message3);
                startActivity(Intent.createChooser(emailIntent, "Send E-mail via"));
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Assignee assignee;
        switch (requestCode) {
            case (PICK_CONTACT):
                if (resultCode == Activity.RESULT_OK) {
                    mImageViewClearAssignee.setVisibility(View.VISIBLE);
                    assignee = new Assignee();
                    Uri contactData = data.getData();
                    ContentResolver cr = getContentResolver();
                    String[] projection = {ContactsContract.Contacts._ID
                            , ContactsContract.Data.DISPLAY_NAME};
                    Cursor phone = cr.query(contactData, projection, null, null, null);
                    if (phone != null && phone.moveToNext()) {
                        int contactNumberId = phone.getInt(phone
                                .getColumnIndex(ContactsContract.Contacts._ID));
                        String contactNumberName = phone.getString(phone
                                .getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                        assignee.setContactId(contactNumberId);
                        assignee.setAssigneeName(contactNumberName);
                        mTextViewAssignedTo.setText(contactNumberName);
                        mTextViewAssignedTo.setTextSize(18);
                        mTask.setAssignee(assignee);
                        phone.close();
                    }
                }
                break;
        }
    }

    @Override
    public void onSuccess(String successMessage) {
        this.showToast(successMessage, Toast.LENGTH_SHORT);
        //supportFinishAfterTransition();
        //onBackPressed();
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra(EXTRA_MAIN_FRAGMENT, mLastSelectedFragment);
        intent.putExtra(EXTRA_CATEGORY_FRAGMENT_ID, mLastSelectedCategoryFragmentId);
        intent.putExtra(EXTRA_CATEGORY_FRAGMENT_TAG, mLastSelectedCategoryFragmentTitle);
        startActivity(intent);
        finish();
    }

    @Override
    public void onFail(String errorMessage) {
        mTask.setTaskCreationDate("");
        this.showToast(errorMessage, Toast.LENGTH_SHORT);
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        mTask.setTitle(String.valueOf(s));
    }

    public void setTaskFields(Task mTask) {

        mAddEditPresenter.checkPriorityColorCode(mTask.getPriority());

        mEditTextTaskTitle.setText(mTask.getTitle());
        mTextViewPriority.setText(mTask.getPriority());
        mTextViewDueDate.setText(mTask.getDate());
        mTextViewRepeat.setText(mTask.getRepeat());
        mTextViewDescription.setText(mTask.getDescription());

        mAddEditPresenter.getCategoryTypeById(mTask.getCategoryId());
        int position = 0;
        String repeatMode = mTask.getRepeat();
        if (!repeatMode.equalsIgnoreCase("One Time")) {
            mTaskRepeatAfterLayout.setVisibility(View.VISIBLE);
            mRepeatFrequency = mTask.getRepeatFrequency();
            position = getRepeatFreqPosition(String.valueOf(mRepeatFrequency));
        } else {
            mTaskRepeatAfterLayout.setVisibility(View.GONE);
            mRepeatFrequency = 0;
        }
        mSpinnerTaskRepeatAfter.setSelection(position);
        mTask.setRepeatFrequency(mRepeatFrequency);

        Assignee assignee = mTask.getAssignee();
        if (assignee.getRowId() > 0 && assignee.getContactId() > 0) {
            mTextViewAssignedTo.setText(assignee.getAssigneeName());
            mImageViewClearAssignee.setVisibility(View.VISIBLE);
            mFabShareTask.setVisibility(View.VISIBLE);
        } else {
            mImageViewClearAssignee.setVisibility(View.GONE);
            mFabShareTask.setVisibility(View.GONE);
        }
    }

    @Override
    public void onUpdateSuccess(String successMessage) {
        this.showToast(successMessage, Toast.LENGTH_SHORT);
        if (mMainFragTag != null) {
            mLastSelectedFragment = mMainFragTag;
            mLastSelectedCategoryFragmentId = mCategoryFragId;
            mLastSelectedCategoryFragmentTitle = mCategoryFragTag;
        }
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra(EXTRA_MAIN_FRAGMENT, mLastSelectedFragment);
        intent.putExtra(EXTRA_CATEGORY_FRAGMENT_ID, mLastSelectedCategoryFragmentId);
        intent.putExtra(EXTRA_CATEGORY_FRAGMENT_TAG, mLastSelectedCategoryFragmentTitle);
        startActivity(intent);
        finish();
    }

    @Override
    public void onUpdateFail(String errorMessage) {
        this.showToast(errorMessage, Toast.LENGTH_SHORT);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    public String getContactNumber(int rawContactId) {
        Cursor c = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                new String[]{ContactsContract.CommonDataKinds.Phone.CONTACT_ID
                        , ContactsContract.CommonDataKinds.Phone.NUMBER}
                , ContactsContract.CommonDataKinds.Phone.CONTACT_ID + "=?",
                new String[]{String.valueOf(rawContactId)}, null);
        if (c != null) {
            c.moveToFirst();
            return c.getString(1).replaceAll("[()\\s-]+", "").substring(1, 13);
        }
        return "";
    }

    public String getAssigneeEmailId(int contactId) {
        Cursor cursor = getContentResolver().query(
                ContactsContract.CommonDataKinds.Email.CONTENT_URI, null,
                ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = ?",
                new String[]{String.valueOf(contactId)}, null);
        if (cursor != null && cursor.moveToNext()) {
            return cursor.getString(cursor
                    .getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));
        }
        return "";
    }

    private String prepareMessageToShare(int shareCode) {
        Assignee assignee = mTask.getAssignee();
        String message = "";
        switch (shareCode) {
            case 0:
                message = "You have been assigned below task :\n\n";
                break;
            case 1:
                message = "Hi " + assignee.getAssigneeName() + ",\n\n"
                        + "You have been assigned below task :\n\n";
                break;
        }
        message = message + "Title : " + mTask.getTitle() + "\n";
        return message;
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        mRepeatFrequency = Integer.valueOf(String.valueOf(parent.getSelectedItem()));
        mTask.setRepeatFrequency(mRepeatFrequency);
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
    }

    public int getRepeatFreqPosition(String repeatFrequency) {
        int length = repeatFrequencyArray.length;
        int position = 0;
        for (int i = 0; i < length; i++) {
            if (repeatFrequencyArray[i].equalsIgnoreCase(repeatFrequency)) {
                position = i;
                break;
            }
        }
        return position;
    }

}