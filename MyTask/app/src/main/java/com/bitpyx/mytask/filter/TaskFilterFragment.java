package com.bitpyx.mytask.filter;

import android.animation.Animator;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.LinearInterpolator;
import android.widget.RelativeLayout;

import com.bitpyx.mytask.R;
import com.bitpyx.mytask.SwipeUtil.SwipeUtil;
import com.bitpyx.mytask.adapter.TaskAdapter;
import com.bitpyx.mytask.addTask.AddEditTaskActivity;
import com.bitpyx.mytask.data.model.DropDownContent;
import com.bitpyx.mytask.data.model.Task;
import com.bitpyx.mytask.filter.presenters.ITaskFilterPresenter;
import com.bitpyx.mytask.filter.presenters.TaskFilterPresenter;
import com.bitpyx.mytask.filter.view.IFilterView;
import com.bitpyx.mytask.main.MainActivity;
import com.bitpyx.mytask.utility.RecyclerTouchListener;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * @author Amrit Yadav
 * @since 12/07/2017
 */
public class TaskFilterFragment extends Fragment implements IFilterView
        , ActionMode.Callback, View.OnClickListener {
    private static final String ARG_COLUMN_FILTER_TYPE = "filterType";
    private static final String EXTRA_MAIN_FRAGMENT = "mainFragment";
    private static final String EXTRA_CATEGORY_FRAGMENT_ID = "categoryFragmentId";
    private static final String EXTRA_CATEGORY_FRAGMENT_TAG = "categoryFragmentTitle";
    private int statusBarColor;
    private RecyclerView mRecyclerView;
    private int filterId;
    private boolean mIsMultiSelect = false;
    private List<Task> mTaskList;
    private List<Task> mRemovedTaskList;
    private List<DropDownContent> mCategoryList;
    private List<Integer> mListItemsToUpdate = new ArrayList<>();
    private Snackbar mSnackbar;
    private ActionMode mActionMode;
    private ITaskFilterPresenter<IFilterView> mTaskFilterPresenter;
    private FloatingActionButton mFabAddEditTask;
    private TaskAdapter mTaskAdapter;
    private RelativeLayout mRelativeLayoutEmptyView;

    public TaskFilterFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param filterType type of filter Normal, Medium, High
     * @return A new instance of fragment TaskFilterFragment.
     */
    public static TaskFilterFragment newInstance(int filterType) {
        TaskFilterFragment fragment = new TaskFilterFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_FILTER_TYPE, filterType);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onResume() {
        super.onResume();
        mTaskFilterPresenter.getCategoryList();
        mTaskFilterPresenter.fetchTaskListByFilter(filterId);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mTaskList = Collections.emptyList();
        if (getArguments() != null) {
            filterId = getArguments().getInt(ARG_COLUMN_FILTER_TYPE);
        }
        mTaskFilterPresenter = new TaskFilterPresenter();
        mTaskFilterPresenter.attachView(this);
        mRemovedTaskList = new ArrayList<>();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_task_filter, container, false);

        CoordinatorLayout mCorCoordinatorLayout = (CoordinatorLayout) container.getParent();

        mFabAddEditTask = (FloatingActionButton) mCorCoordinatorLayout
                .findViewById(R.id.fab_add_edit_task);

        mSnackbar = Snackbar.make(mCorCoordinatorLayout, "", 2000).setAction("UNDO", this)
                .addCallback(new Snackbar.Callback() {
                    @Override
                    public void onDismissed(Snackbar snackbar, int event) {
                        mRemovedTaskList.clear();
                    }

                    @Override
                    public void onShown(Snackbar snackbar) {

                    }
                });

        mRelativeLayoutEmptyView = (RelativeLayout) view.findViewById(R.id.layout_today_empty_view);

        mRecyclerView = (RecyclerView) view.findViewById(R.id.rv_task_filter);
        // LinearLayoutManager is used here, this will layout the elements in a similar fashion
        // to the way ListView would layout elements. The RecyclerView.LayoutManager defines how
        // elements are laid out.
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);

        mRecyclerView.addOnItemTouchListener(new RecyclerTouchListener(getActivity()
                , mRecyclerView, new MainActivity.ClickListener() {
            @Override
            public void onClick(View view, int position) {

                Task task = mTaskList.get(position);

                if (mIsMultiSelect) {
                    if (task.isSelected()) {
                        mTaskFilterPresenter.unSelectListItem(view, task);
                        mListItemsToUpdate.removeAll(Arrays.asList(position));
                    } else {
                        mTaskFilterPresenter.selectListItem(view, task);
                        mListItemsToUpdate.add(position);
                    }

                    mActionMode.setTitle("" + mListItemsToUpdate.size());

                    if (mListItemsToUpdate.isEmpty() && mActionMode != null) {
                        mIsMultiSelect = false;
                        mActionMode.finish();
                        mActionMode = null;
                    }

                } else {
                    Intent intent = new Intent(getActivity(), AddEditTaskActivity.class);
                    intent.putExtra(EXTRA_MAIN_FRAGMENT, "TaskFilter");
                    intent.putExtra(EXTRA_CATEGORY_FRAGMENT_ID, filterId);
                    intent.putExtra(EXTRA_CATEGORY_FRAGMENT_TAG, task.getPriority());
                    intent.putExtra("Task", task);
                    intent.putExtra("Type", "edit");
                    startActivity(intent);
                }

            }

            @Override
            public void onLongClick(View view, int position) {
                mIsMultiSelect = true;

                if (!mListItemsToUpdate.contains(position)) {
                    Task task = mTaskList.get(position);
                    mTaskFilterPresenter.selectListItem(view, task);
                    mListItemsToUpdate.add(position);
                }

                if (mActionMode == null) {
                    mActionMode = getActivity().startActionMode(TaskFilterFragment.this);
                }

                mActionMode.setTitle("" + mListItemsToUpdate.size());
            }
        }));

        DividerItemDecoration mDividerItemDecoration =
                new DividerItemDecoration(mRecyclerView.getContext(),
                        DividerItemDecoration.VERTICAL);
        mRecyclerView.addItemDecoration(mDividerItemDecoration);

        setSwipeForRecyclerView();

        return view;
    }

    private void setSwipeForRecyclerView() {

        SwipeUtil swipeHelper = new SwipeUtil(0, ItemTouchHelper.LEFT, getActivity()) {

            @Override
            public int getMovementFlags(RecyclerView recyclerView
                    , RecyclerView.ViewHolder viewHolder) {
                return viewHolder.getItemViewType() == 0
                        ? 0 : super.getMovementFlags(recyclerView, viewHolder);
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                int swipedPosition = viewHolder.getAdapterPosition();
                Task task = mTaskList.get(swipedPosition);
                mTaskFilterPresenter.removeTask(task.getTaskId());
                mRemovedTaskList.add(task);
                mTaskAdapter = (TaskAdapter) mRecyclerView.getAdapter();
                mTaskAdapter.pendingRemoval(swipedPosition);
                mTaskFilterPresenter.updateListOnTaskSwipeRemove(mTaskList);
                String message = "DELETE (" + mRemovedTaskList.size() + ")";
                mSnackbar.setText(message);
                mSnackbar.show();
            }

            @Override
            public int getSwipeDirs(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
                int position = viewHolder.getAdapterPosition();
                mTaskAdapter = (TaskAdapter) mRecyclerView.getAdapter();
                if (mTaskAdapter.isPendingRemoval(position)) {
                    return 0;
                }
                return super.getSwipeDirs(recyclerView, viewHolder);
            }
        };

        ItemTouchHelper mItemTouchHelper = new ItemTouchHelper(swipeHelper);
        mItemTouchHelper.attachToRecyclerView(mRecyclerView);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void refreshTaskList(List<Task> taskList) {
        if (mActionMode != null)
            mActionMode.finish();
        mTaskList = taskList;
        if (mTaskList.size() > 0) {
            mRecyclerView.setVisibility(View.VISIBLE);
            mRelativeLayoutEmptyView.setVisibility(View.GONE);
            mTaskAdapter = new TaskAdapter(getActivity(), mTaskList, mCategoryList);
            mRecyclerView.setAdapter(mTaskAdapter);
            mTaskAdapter.notifyDataSetChanged();
        } else {
            mRecyclerView.setVisibility(View.GONE);
            mRelativeLayoutEmptyView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void categoryList(List<DropDownContent> dropDownContents) {
        mCategoryList = dropDownContents;
    }

    @Override
    public boolean onCreateActionMode(ActionMode mode, Menu menu) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            //hold current color of status bar
            statusBarColor = getActivity().getWindow().getStatusBarColor();
            getActivity().getWindow()
                    .setStatusBarColor(ContextCompat.getColor(getActivity()
                            , R.color.actionModePrimaryColorDark));
        }

        CoordinatorLayout.LayoutParams layoutParams =
                (CoordinatorLayout.LayoutParams) mFabAddEditTask.getLayoutParams();
        int fab_bottomMargin = layoutParams.bottomMargin;

        mFabAddEditTask.animate().translationY(mFabAddEditTask.getHeight() + fab_bottomMargin)
                .setInterpolator(new LinearInterpolator()).setListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                if(mIsMultiSelect)
                    mFabAddEditTask.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        }).start();

        // Inflate a menu resource providing context menu items
        MenuInflater inflater = mode.getMenuInflater();
        inflater.inflate(R.menu.menu_multi_select, menu);
        MenuItem item = menu.findItem(R.id.action_send);
        item.setVisible(false);
        MenuItem itemRestoreTask = menu.findItem(R.id.action_restore);
        itemRestoreTask.setVisible(false);
        return true;
    }

    @Override
    public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
        return false;
    }

    @Override
    public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
        mTaskFilterPresenter.onActionItemClicked(mTaskList, mListItemsToUpdate, mRecyclerView, filterId);
        mActionMode = null;
        mIsMultiSelect = false;
        return false;
    }

    @Override
    public void onDestroyActionMode(ActionMode mode) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            //return to "old" color of status bar
            getActivity().getWindow().setStatusBarColor(statusBarColor);
        }
        mFabAddEditTask.setVisibility(View.VISIBLE);
        mFabAddEditTask.animate().translationY(0).setInterpolator(new LinearInterpolator()).start();
        mActionMode = null;
        mIsMultiSelect = false;
        mTaskFilterPresenter.onDestroyActionMode(mTaskList, mListItemsToUpdate, mRecyclerView);
    }

    @Override
    public void onClick(View v) {
        mTaskFilterPresenter.addUndoTask(mRemovedTaskList);
        mRemovedTaskList.clear();
        mTaskFilterPresenter.fetchTaskListByFilter(filterId);
    }

    public void setFilter(int filterId) {
        this.filterId=filterId;
        mTaskFilterPresenter.fetchTaskListByFilter(filterId);
    }

    public List<Task> getTaskList() {
        return mTaskList;
    }

}