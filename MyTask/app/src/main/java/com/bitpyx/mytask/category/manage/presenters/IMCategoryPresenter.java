package com.bitpyx.mytask.category.manage.presenters;

import com.bitpyx.mytask.category.manage.view.IMCategoryView;
import com.bitpyx.mytask.data.model.DropDownContent;
import com.bitpyx.mytask.utility.IBasePresenter;

/**
 * @author Amrit Yadav
 * @since 7/9/2017.
 */
public interface IMCategoryPresenter<V> extends IBasePresenter<V>{
    void fetchTaskCategories();
    void saveNewCategory(String categoryName, boolean editCategory
            ,DropDownContent categoryObj, IMCategoryView.OnCategoryAdded onCategoryAdded);
    void removeCategory(DropDownContent categoryObj
            , IMCategoryView.OnCategoryAdded onCategoryAdded);
    void getTaskCountForTheCategory(int categoryId);
}
