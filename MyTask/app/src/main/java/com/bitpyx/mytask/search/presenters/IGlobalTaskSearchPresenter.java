package com.bitpyx.mytask.search.presenters;

import com.bitpyx.mytask.utility.IBasePresenter;

/**
 * Created by amrit on 6/24/2017.
 */

public interface IGlobalTaskSearchPresenter<V> extends IBasePresenter<V> {
    void searchTask(String searchQuery);

    void getCategoryList();
}
