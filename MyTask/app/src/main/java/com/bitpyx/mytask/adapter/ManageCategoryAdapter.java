package com.bitpyx.mytask.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bitpyx.mytask.R;
import com.bitpyx.mytask.data.model.DropDownContent;

import java.util.List;

/**
 * @author Amrit Yadav
 * @since 7/9/2017.
 */
public class ManageCategoryAdapter extends RecyclerView.Adapter<ManageCategoryAdapter.VHItem>{
    private List<DropDownContent> mCategoryList;

    public ManageCategoryAdapter(List<DropDownContent> categoryList) {
        mCategoryList=categoryList;
    }

    @Override
    public VHItem onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_manage_category, parent, false);

        return new VHItem(view);
    }

    @Override
    public void onBindViewHolder(VHItem holder, int position) {
        DropDownContent categoryObj = mCategoryList.get(position);
        holder.mTextViewCategoryTitle.setText(categoryObj.getName());
    }

    @Override
    public int getItemCount() {
        return mCategoryList.size();
    }

    @Override
    public int getItemViewType(int position) {
        //1 indicates all the rows are of Item type
        return 1;
    }

    class VHItem extends RecyclerView.ViewHolder{
        private TextView mTextViewCategoryTitle;

        VHItem(View view) {
            super(view);
            mTextViewCategoryTitle=(TextView) view.findViewById(R.id.tv_manage_category_title);
        }
    }

}
