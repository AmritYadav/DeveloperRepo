package com.bitpyx.mytask.complete.interactors;

import com.bitpyx.mytask.data.model.DropDownContent;
import com.bitpyx.mytask.data.model.Task;

import java.util.List;

/**
 * @author Amrit Yadav
 * @since 7/29/2017.
 */
public interface ITaskCompleteInteractor {
    List<Task> fetchTaskCompleteList();

    List<DropDownContent> fetchCategoryList();

    void deleteTask(int taskId);

    void saveUndoTask(List<Task> undoTaskList);

    void updateTasksStatus(List<Task> mTaskList, List<Integer> mListItemsToUpdate);
}
