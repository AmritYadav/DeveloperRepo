package com.bitpyx.mytask.today.presenters;

import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.bitpyx.mytask.R;
import com.bitpyx.mytask.adapter.TaskAdapter;
import com.bitpyx.mytask.data.model.DropDownContent;
import com.bitpyx.mytask.data.model.Task;
import com.bitpyx.mytask.today.interactors.ITodayTaskInteractor;
import com.bitpyx.mytask.today.interactors.TodayTaskInteractor;
import com.bitpyx.mytask.today.views.ITodayView;

import java.util.List;

/**
 * @author Amrit Yadav
 * @since 6/17/2017
 */
public class TodayTaskPresenter implements ITodayTaskPresenter<ITodayView> {

    private ITodayView mTodayView;
    private ITodayTaskInteractor mTodayTaskInteractor;

    @Override
    public void attachView(ITodayView mTodayTask) {
        this.mTodayView = mTodayTask;
        mTodayTaskInteractor = new TodayTaskInteractor(mTodayTask.getContext());
    }

    @Override
    public void fetchTodayTask() {
        List<Task> taskList = mTodayTaskInteractor.fetchTodayTask();
        mTodayView.setAdapter(taskList);
    }

    @Override
    public void fetchTodayTask(int contactId) {
        List<Task> taskList = mTodayTaskInteractor.fetchTodayTask(contactId);
        mTodayView.setAdapter(taskList);
    }

    @Override
    public void getCategoryList() {
        List<DropDownContent> dropDownContents = mTodayTaskInteractor.fetchCategoryList();
        mTodayView.categoryList(dropDownContents);
    }

    @Override
    public void removeTask(int taskId) {
        mTodayTaskInteractor.deleteTask(taskId);
    }

    @Override
    public void updateListHeadersOnSwipeRemove(List<Task> taskList) {
        int headersCount = 0;
        for (Task task : taskList) {
            if(task.getViewType()==0)
                headersCount++;
        }
        if(headersCount==taskList.size()){
            taskList.clear();
            mTodayView.setAdapter(taskList);
        }
    }

    @Override
    public void addUndoTask(List<Task> undoTask) {
        mTodayTaskInteractor.saveUndoTask(undoTask);
    }

    @Override
    public void selectListItem(View view, Task task) {
        task.setSelected(true);
        view.setBackgroundColor(ContextCompat.getColor(mTodayView.getContext()
                , R.color.listSelectedColor));
    }

    @Override
    public void unSelectListItem(View view, Task task) {
        task.setSelected(false);
        view.setBackgroundColor(ContextCompat.getColor(mTodayView.getContext()
                , R.color.white));
    }

    @Override
    public void onDestroyActionMode(List<Task> mTaskList, List<Integer> mListItemsToUpdate, RecyclerView mRecyclerView) {
        int listSize = mListItemsToUpdate.size();
        for (int i = 0; i < listSize; i++) {
            int position = mListItemsToUpdate.get(i);
            Task task = mTaskList.get(position);
            task.setSelected(false);
        }
        TaskAdapter adapter = (TaskAdapter) mRecyclerView.getAdapter();
        adapter.refreshAdapter(mTaskList);
        mListItemsToUpdate.clear();
    }

    @Override
    public void onActionItemClicked(List<Task> mTaskList, List<Integer> mListItemsToUpdate
            , RecyclerView mRecyclerView, int contactId) {
        if(contactId>0){
            fetchTodayTask(contactId);
        } else {
            mTodayTaskInteractor.UpdateTasksStatus(mTaskList, mListItemsToUpdate);
            fetchTodayTask();
        }
    }

    @Override
    public void detachView() {

    }
}
