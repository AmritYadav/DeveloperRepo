package com.bitpyx.mytask.category.manage.interactors;

import com.bitpyx.mytask.data.model.DropDownContent;

import java.util.List;

/**
 * @author Amrit Yadav
 * @since 7/9/2017.
 */
public interface IMCategoryInteractor {
    List<DropDownContent> fetchCategoryList();

    boolean categoryExists(String categoryName);

    int fetchTaskCountCountForCategory(int categoryId);

    long insertNewCategory(String categoryName);

    long updateCategory(String category, int categoryId);

    void deleteCategory(DropDownContent categoryObj);
}
